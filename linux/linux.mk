LINUX26_DIR:=$(BUILD_DIR)/kernel
ifeq ($(BRCM),y)
LINUX26_MAKE_FLAGS = \
	HOSTCC="$(HOSTCC)" \
	HOSTCFLAGS="$(HOSTCFLAGS)" \
	ARCH=$(KERNEL_ARCH) \
	INSTALL_MOD_PATH=$(TARGET_DIR) \
	CROSS_COMPILE=$(TARGET_CROSS) \
	CXX=$(TARGET_CROSS)g++ \
	LDFLAGS="$(TARGET_LDFLAGS)" \
	CFLAGS="$(TARGET_CFLAGS)" \
	LZMA="$(LZMA)" \
	BUILD_DIR="$(BUILD_DIR)" \
	PROFILE="$(PROFILE)"
else
LINUX26_MAKE_FLAGS = \
	HOSTCC="$(HOSTCC)" \
	HOSTCFLAGS="$(HOSTCFLAGS)" \
	ARCH=$(KERNEL_ARCH) \
	INSTALL_MOD_PATH=$(TARGET_DIR) \
	CROSS_COMPILE=$(TARGET_CROSS) \
	CXX=$(TARGET_CROSS)g++ \
	LDFLAGS="$(TARGET_LDFLAGS)" \
	LZMA="$(LZMA)" \
	BUILD_DIR="$(BUILD_DIR)" \
	PROFILE="$(PROFILE)" \
	CUSTOM_CFLAGS="$(subst ",,${BR2_PROFILE_CFLAGS}) $(BUILDCFLAGS)"
endif

ifeq ($(BR2_PACKAGE_GPIO_MODULE),y)
  BUILDCFLAGS += -DSUPPORT_GPIO_MODULE
endif

ifeq ($(BR2_LINUX_KERNEL_UIMAGE),y)
LINUX26_IMAGE_NAME=uImage
LINUX26_DEPENDENCIES+=$(MKIMAGE) host-lzma
else ifeq ($(BR2_LINUX_KERNEL_BZIMAGE),y)
LINUX26_IMAGE_NAME=bzImage
else ifeq ($(BR2_LINUX_KERNEL_ZIMAGE),y)
LINUX26_IMAGE_NAME=zImage
else ifeq ($(BR2_LINUX_KERNEL_VMLINUX_BIN),y)
LINUX26_IMAGE_NAME=vmlinux.bin
else ifeq ($(BR2_LINUX_KERNEL_LZIMAGE),y)
LINUX26_IMAGE_NAME=vmlinux.lzma
else ifeq ($(BR2_LINUX_KERNEL_VMLINUX),y)
LINUX26_DEPENDENCIES+=host-lzma_ralink_modem host-trx_ralink_modem bootrom
LINUX26_IMAGE_NAME=vmlinux
endif

#ifeq (${BCM47XX},y)
#LINUX26_DEPENDENCIES+=bcm47xx_shared
#endif
ifneq ($(BR2_LINUX_KERNEL_CUSTOM_BRANCH), "")
	DOWNLOAD_KERNEL := git clone -b $(subst ",,$(BR2_LINUX_KERNEL_CUSTOM_BRANCH)) git@rd:$(subst ",,$(BR2_LINUX_KERNEL_CUSTOM_REPO)) $(LINUX26_DIR)
	ifeq ($(RALINK),y)
		LINUX26_DEPENDENCIES+=mediatek_wifi
		BUILDCFLAGS += -I$(LINUX26_DIR)/net/nat/hw_nat
	endif

else 
	DOWNLOAD_KERNEL := git clone git@rd:$(subst ",,$(BR2_LINUX_KERNEL_CUSTOM_REPO)) $(LINUX26_DIR)
endif

ifeq (${RALINK_MODEM},y)
LINUX26_IMAGE_PATH=$(LINUX26_DIR)
else
LINUX26_IMAGE_PATH=$(LINUX26_DIR)/arch/$(KERNEL_ARCH)/boot/$(LINUX26_IMAGE_NAME)
endif

# Download
$(LINUX26_DIR)/.stamp_downloaded:
	@$(call MESSAGE,"Downloading kernel")
	#$(call DOWNLOAD,$(LINUX26_SITE),$(LINUX26_SOURCE))
	$(DOWNLOAD_KERNEL)
	touch $@

#Patching
$(LINUX26_DIR)/.stamp_patched:  $(LINUX26_DIR)/.stamp_downloaded
	@$(call MESSAGE,"Patching kernel")
ifeq ($(BR2_LINUX_KERNEL_DLINK_PATCH),y)
	cd $(LINUX26_DIR) &&	patch -p1 -i $(LINUX26_DIR)/patches/*.patch
else
	echo "Not need patching Kernel!"
endif
	touch $@

# Configuration
$(LINUX26_DIR)/.stamp_configured: $(LINUX26_DIR)/.stamp_downloaded $(LINUX26_DIR)/.stamp_patched
	@$(call MESSAGE,"Configuring kernel")
ifeq ($(BR2_rlx),y)
	@ln -sf bsp_${RTL_CHIP} $(LINUX26_DIR)/arch/rlx/bsp
endif
	@if [ -e $(LINUX26_DIR)/config_${PROFILE} ] ; then \
		cp -v $(LINUX26_DIR)/config_${PROFILE} $(LINUX26_DIR)/.config; \
	else \
		cp -v $(PROFILE_DIR)/${BR2_LINUX_KERNEL_CUSTOM_CONFIG_FILE} $(LINUX26_DIR)/.config; \
	fi
	$(Q)touch $@
	

# Compilation. We make sure the kernel gets rebuilt when the
# configuration has changed.

$(LINUX26_DIR)/.stamp_compiled: $(LINUX26_DIR)/.stamp_configured $(LINUX26_DIR)/.config
	@$(call MESSAGE,"Compiling kernel")
	$(TARGET_MAKE_ENV) TARGET_DIR=${TARGET_DIR} $(MAKE) $(LINUX26_MAKE_FLAGS) -C $(@D) $(LINUX26_IMAGE_NAME) V=99


ifeq (${BCM47XX},y)
	$(TARGET_CONFIGURE_OPTS) LINUX_IMAGE_PATH=$(LINUX26_IMAGE_PATH) LZMA=$(LZMA) LZMAPATH=$(HOST_LZMA_DIR)/C/Compress/Lzma BUILD_DIR="$(BUILD_DIR)" LINUXDIR="$(LINUX26_DIR)" $(MAKE1) CONFIG_SQUASHFS=y WLTEST=1 -C $(LINUX26_DIR)/arch/mips/brcm-boards/bcm947xx/compressed
endif
ifeq ($(BR2_rlx),y)
ifeq ($(RTL8676),y)
	${MAKE1} -C $(LINUX26_DIR) vmimg CROSS_COMPILE=$(TARGET_CROSS)
	cp $(LINUX26_DIR)/vmlinux.lzma $(LINUX26_IMAGE_PATH)
else
	CROSS_COMPILE=$(TARGET_CROSS) ${MAKE1} -C $(LINUX26_DIR)/rtkload
endif
endif

ifeq (${BCM47XX},y)
	@if [ $(shell grep -c "CONFIG_MODULES=y" $(LINUX26_DIR)/.config) != 0 ] ; then 	\
		$(TARGET_MAKE_ENV) $(MAKE1) $(LINUX26_MAKE_FLAGS) -C $(@D) modules ;	\
	fi
else 
	@if [ $(shell grep -c "CONFIG_MODULES=y" $(LINUX26_DIR)/.config) != 0 ] ; then 	\
		$(TARGET_MAKE_ENV) $(MAKE) $(LINUX26_MAKE_FLAGS) -C $(@D) modules ;	\
	fi
endif
	
ifeq (${RALINK_MODEM},y)
	$(TARGET_MAKE_ENV) TARGET_DIR=${TARGET_DIR} $(MAKE) $(LINUX26_MAKE_FLAGS) -C $(@D) linux.7z
endif

	$(Q)touch $@

# Installation
$(LINUX26_DIR)/.stamp_installed: $(LINUX26_DIR)/.stamp_compiled
	@$(call MESSAGE,"Installing kernel")
ifneq (${RALINK_MODEM},y)
	cp $(LINUX26_IMAGE_PATH) $(BINARIES_DIR)
else
	cp $(LINUX26_IMAGE_PATH)/linux.7z $(HOST_DIR)/usr/bin
endif
#i need it for cmplzma
ifeq ($(BRCM),y)
	cp $(LINUX26_DIR)/arch/$(KERNEL_ARCH)/boot/vmlinux $(BINARIES_DIR)
endif
	# Install modules and remove symbolic links pointing to build
	# directories, not relevant on the target
	@if [ $(shell grep -c "CONFIG_MODULES=y" $(LINUX26_DIR)/.config) != 0 ] ; then 	\
		$(TARGET_MAKE_ENV) $(MAKE1) $(LINUX26_MAKE_FLAGS) -C $(@D) 		\
			INSTALL_MOD_PATH=$(TARGET_DIR) DEPMOD=$(TOPDIR)/scripts/depmod.pl modules_install ;		\
		rm -f $(TARGET_DIR)/lib/modules/$(LINUX26_VERSION_PROBED)/build ;	\
		rm -f $(TARGET_DIR)/lib/modules/$(LINUX26_VERSION_PROBED)/source ;	\
	fi
	$(Q)touch $@

linux26:  $(LINUX26_DEPENDENCIES) $(LINUX26_DIR)/.stamp_installed 

ifeq ($(BR2_LINUX_KERNEL),y)
TARGETS+=linux26
endif

kernel: linux26 

kernel-clean: 
	@$(call MESSAGE,"Cleaning kernel")
	$(MAKE1) $(LINUX26_MAKE_FLAGS) -C $(LINUX26_DIR) clean
#	rm -rf $(LINUX26_DIR)/.stamp_configured
	rm -rf $(LINUX26_DIR)/.stamp_compiled
	rm -rf $(LINUX26_DIR)/.stamp_installed

kernel-patch-clean:
	@$(call MESSAGE,"Cleaning patches kernel")
	cd $(LINUX26_DIR) &&	patch -p1 -R -i $(LINUX26_DIR)/patches/*.patch
	rm -rf $(LINUX26_DIR)/.stamp_patched

kernel-menuconfig: $(LINUX26_DIR)/.stamp_configured
	$(MAKE1) $(LINUX26_MAKE_FLAGS) -C $(LINUX26_DIR) menuconfig
	rm -f $(LINUX26_DIR)/.stamp_{built,installed,compiled}
