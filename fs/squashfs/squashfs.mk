#############################################################
#
# Build the squashfs root filesystem image
#
#############################################################

ifeq ($(BR2_TARGET_ROOTFS_SQUASHFS4),y)
ROOTFS_SQUASHFS_DEPENDENCIES = host-squashfs

ifeq ($(BR2_TARGET_ROOTFS_SQUASHFS4_LZO),y)
  ROOTFS_SQUASHFS_ARGS += -comp lzo
else
  ifeq ($(BR2_TARGET_ROOTFS_SQUASHFS4_LZMA),y)
    ROOTFS_SQUASHFS_ARGS += -comp lzma
  else
    ifeq ($(BR2_TARGET_ROOTFS_SQUASHFS4_XZ),y)
      ROOTFS_SQUASHFS_ARGS += -comp xz
    else
      ROOTFS_SQUASHFS_ARGS += -comp gzip
    endif
  endif
endif

ADV_OPTIONS := -all-root

ifeq ($(BRCM), y)
  ADV_OPTIONS := -all-root -b 65536
  ROOTFS_SQUASHFS_ARGS :=
endif

else #squashfs v.3

  ifneq ($(RALINK_MODEM), y)
    ROOTFS_SQUASHFS_DEPENDENCIES = host-squashfs3 
  else
    ROOTFS_SQUASHFS_DEPENDENCIES = host-lzma_ralink_modem
  endif
  
  ifeq ($(BIG_ENDIAN),y)
    ROOTFS_SQUASHFS_ARGS=-be
  else
    ROOTFS_SQUASHFS_ARGS=-le
  endif

  # Если девайс DIR_615E, то используется иной размер 
  # блока для уменьшения размера корневой файловой системы.
  ifeq ($(PROFILE),DIR_615E)
    ADV_OPTIONS += -b 262144
  endif
endif

ROOTFS_SQUASHFS_DEPENDENCIES += mklibs

ifneq ($(RALINK_MODEM), y)
define ROOTFS_SQUASHFS_CMD
	$(HOST_DIR)/usr/bin/mksquashfs $(TARGET_DIR) $$@ -noappend $(ADV_OPTIONS)\
		$(ROOTFS_SQUASHFS_ARGS) && \
	chmod 0644 $$@
endef
else
define ROOTFS_SQUASHFS_CMD
	$(HOST_DIR)/usr/bin/mksquashfs-lzma $(TARGET_DIR) $$@ -noappend $(ADV_OPTIONS)\
		$(ROOTFS_SQUASHFS_ARGS) && \
	chmod 0644 $$@
endef
endif

$(eval $(call ROOTFS_TARGET,squashfs))
