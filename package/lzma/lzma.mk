#############################################################
#
# lzma
#
#############################################################
LZMA_VERSION:=4.57
LZMA_SOURCE:=lzma-$(LZMA_VERSION).tar.bz2
LZMA_SITE:=${DLINK_STORAGE}

define HOST_LZMA_BUILD_CMDS
	Sqlzma=$(@D)/sqlzma $(MAKE) -C $(@D)/CPP/7zip/Compress/LZMA_Alone -f sqlzma.mk
	Sqlzma=$(@D)/sqlzma $(MAKE) -C $(@D)/C/Compress/Lzma -f sqlzma.mk
endef
define HOST_LZMA_INSTALL_CMDS
	mkdir -p $(HOST_DIR)/usr/bin
	cp $(@D)/CPP/7zip/Compress/LZMA_Alone/lzma $(HOST_DIR)/usr/bin/lzma
endef
$(eval $(call GENTARGETS,host))

LZMA=$(HOST_DIR)/usr/bin/lzma
