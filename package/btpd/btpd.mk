#############################################################
#
# btpd
#
#############################################################
BTPD_SITE = $(DLINK_GIT_STORAGE)/btpd
BTPD_VERSION = master
BTPD_AUTORECONF = YES
BTPD_CONF_OPT = --sysconfdir=/tmp --with-evloop-method=poll
define BTPD_INSTALL_TARGET_CMDS
	install -pm0755 $(@D)/btpd/btpd $(TARGET_DIR)/usr/sbin/btpd
	${TARGET_STRIP} $(TARGET_DIR)/usr/sbin/btpd
	install -pm0755 $(@D)/cli/btcli $(TARGET_DIR)/usr/sbin/btcli
	${TARGET_STRIP} $(TARGET_DIR)/usr/sbin/btcli
	install -pm0755 $(@D)/cli/btinfo $(TARGET_DIR)/usr/sbin/btinfo
	${TARGET_STRIP} $(TARGET_DIR)/usr/sbin/btinfo
endef

$(eval $(call AUTOTARGETS))
