#############################################################
#
# dropbear
#
#############################################################

#DROPBEAR_VERSION =$(call qstrip,$(BR2_DROPBEAR_VERSION))
#ifeq (${DROPBEAR_VERSION},)
#DROPBEAR_VERSION = $(call qstrip,$(BR2_DEFAULT_BRANCH))
#endif

DROPBEAR_VERSION = master
DROPBEAR_SITE = $(DLINK_GIT_STORAGE)/dropbear

DROPBEAR_CONF_OPT = --disable-zlib --disable-shadow \
	    --disable-lastlog --disable-utmp --disable-utmpx \
	    --disable-wtmp --disable-wtmpx \
	    --disable-libutil --disable-loginfunc --disable-pututline \
	    --disable-pututxline --enable-bundled-libtom

define DROPBEAR_INSTALL_TARGET_CMDS
	install -d $(TARGET_DIR)/usr/bin
	install -d $(TARGET_DIR)/usr/sbin
#	install -pm0755 $(@D)/dropbearmulti $(TARGET_DIR)/usr/sbin/dropbear
#	$(STRIP) $(TARGET_DIR)/usr/sbin/dropbear
	ln -sf dropbear $(TARGET_DIR)/usr/sbin/dropbearkey
	ln -sf dropbear $(TARGET_DIR)/usr/sbin/dropbearconvert
	ln -sf ../sbin/dropbear $(TARGET_DIR)/usr/bin/ssh
	ln -sf ../sbin/dropbear $(TARGET_DIR)/usr/bin/scp
endef

$(eval $(call AUTOTARGETS))
