#############################################################
#
# transmission
#
#############################################################

ifeq ($(RALINK), y)
TRANSMISSION_VERSION = master
else
TRANSMISSION_VERSION = 1_33
endif

TRANSMISSION_SITE = $(DLINK_GIT_STORAGE)/transmission
TRANSMISSION_AUTORECONF = NO
TRANSMISSION_INSTALL_STAGING = YES
TRANSMISSION_INSTALL_TARGET = YES
TRANSMISSION_CFLAGS = "$(TARGET_CFLAGS) -lthread -I$(TARGET_DIR)/usr/include/"


TRANSMISSION_DEPENDENCIES = curl zlib openssl
TRANSMISSION_POST_CONFIGURE_HOOKS += LIBTOOL_PATCH_HOOK
TRANSMISSION_CONF_ENV = EXTRASYSROOTFLAG="--sysroot=$(STAGING_DIR)" LIBCURL_CFLAGS="-I$(TARGET_DIR)/usr/include/ -I$(STAGING_DIR)/include/ --sysroot=$(STAGING_DIR)"
TRANSMISSION_CONF_OPT = --enable-daemon --disable-gtk --disable-wx --with-wx-config="$(STAGING_DIR)/bin/" --enable-largefile --enable-lightweight --prefix="/usr" --disable-static --disable-dependency-tracking
#TRANSMISSION_CONFIGURE_CMDS = ./configure $(TARGET_CONF_OPT) $(TRANSMISSION_CONF_OPT)


TRANSMISSION_MAKE_ENV = $(TARGET_MAKE_ENV) LDFLAGS="$(TARGET_LDFLAGS)" ZLIB_LDFLAGS="$(TARGET_LDFLAGS)" CFLAGS=$(TRANSMISSION_CFLAGS)

define TRANSMISSION_BUILD_CMDS
	echo "# Just for changing last modified date =)" >> $(TRANSMISSION_SRCDIR)/aclocal.m4
	$(TRANSMISSION_MAKE_ENV) $(TRANSMISSION_MAKE) $(TARGET_MAKE_OPT) -I$(TARGET_DIR)usr/include/ -C $(TRANSMISSION_SRCDIR)
endef

$(eval $(call AUTOTARGETS))
