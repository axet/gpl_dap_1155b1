#############################################################
#
# p910nd
#
#############################################################
P910ND_VERSION = 0.94
P910ND_SITE=${DLINK_STORAGE}
P910ND_SOURCE=p910nd-0.95.tar.bz2
P910ND_MAKE_OPTS = \
	CFLAGS="$(TARGET_CFLAGS) -DVERSION=0.9.8 -O2  -Wall " \
	CC="$(TARGET_CC)" \
	LD="$(TARGET_CC)" \
	DESTDIR="$(TARGET_DIR)" \
	EXTRA_LDFLAGS="$(TARGET_LDFLAGS)" \
	CROSS_COMPILE="$(TARGET_CROSS)" \
	STRIP="$(TARGET_STRIP)"

define P910ND_BUILD_CMDS
	$(MAKE) $(P910ND_MAKE_OPTS) -C $(@D) install
endef

define P910ND_INSTALL_TARGET_CMDS
	$(MAKE) $(P910ND_MAKE_OPTS) -C $(@D) install
endef

define P910ND_CLEAN_CMDS
	$(MAKE) -C $(@D) clean
endef

$(eval $(call GENTARGETS))
