#############################################################
#
# pure-ftpd
#
#############################################################
PURE_FTPD_SITE = $(DLINK_GIT_STORAGE)/pure-ftpd
PURE_FTPD_VERSION = master
#CONFIGURE := ./configure --prefix=/usr \
#	--sysconfdir=/tmp \
#	--with-rfc2640 \
#	--with-minimal

#define PURE_FTPD_CONFIGURE_CMDS
#	cd ${@D} && ${CONFIGURE}
#endef
#define PURE_FTPD_BUILD_CMDS
#	$(MAKE) -C $(@D)
#endef
PURE_FTPD_CONF_OPT := --with-minimal --sysconfdir=/tmp
define PURE_FTPD_INSTALL_TARGET_CMDS
	install -pm0755 $(@D)/src/pure-ftpd $(TARGET_DIR)/usr/sbin
	${TARGET_STRIP} $(TARGET_DIR)/usr/sbin/pure-ftpd
endef
$(eval $(call AUTOTARGETS))
