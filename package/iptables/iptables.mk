#############################################################
#
# iptables
#
#############################################################
#IPTABLES_VERSION = 1.4.3-rc1
IPTABLES_VERSION =$(call qstrip,$(BR2_IPTABLES_VERSION))
#IPTABLES_SOURCE = iptables-$(IPTABLES_VERSION).tar.bz2
#IPTABLES_SITE = ${DLINK_STORAGE}

ifeq (${IPTABLES_VERSION},)
IPTABLES_VERSION = $(call qstrip,$(BR2_DEFAULT_BRANCH))
endif

IPTABLES_SITE = $(DLINK_GIT_STORAGE)/iptables

IPTABLES_DEPENDENCIES = host-pkg-config
IPTABLES_INSTALL_STAGING = YES
IPTABLES_CONF_OPT = --libexecdir=/usr/lib --with-kernel=$(LINUX26_DIR)

ifneq ($(BR2_INET_IPV6),y)
IPTABLES_CONF_OPT += --disable-ipv6
endif

ifeq (${BR2_PACKAGE_URLFILTERD},y)
IPTABLES_CONF_OPT += --enable-libipq
endif
IPTABLES_AUTORECONF = YES
#for non rtl_modem
ifneq ($(call qstrip,$(BR2_IPTABLES_VERSION)),rtl_modem)  
define IPTABLES_INSTALL_STAGING_CMDS
# cp -aR $(@D)/include/* $(STAGING_DIR)/usr/include
  cp -aR $(@D)/include/libipq $(STAGING_DIR)/usr/include
  cp -aR $(@D)/include/libiptc $(STAGING_DIR)/usr/include
  cp -aR $(@D)/include/iptables $(STAGING_DIR)/usr/include
  mkdir -p $(STAGING_DIR)/usr/include/linux/netfilter
  cp $(@D)/include/linux/netfilter/*.h $(STAGING_DIR)/usr/include/linux/netfilter
  cp  $(@D)/include/*.h $(STAGING_DIR)/usr/include
  cp -aR $(@D)/libipq/libipq.* $(STAGING_DIR)/usr/lib
endef

ifeq ($(call qstrip,$(BR2_IPTABLES_VERSION)),1.4.13)
define IPTABLES_TARGET_SYMLINK_CREATE
	ln -sf xtables-multi $(TARGET_DIR)/usr/sbin/iptables
	ln -sf xtables-multi $(TARGET_DIR)/usr/sbin/iptables-save
	ln -sf xtables-multi $(TARGET_DIR)/usr/sbin/iptables-restore
endef

define IPTABLES_TARGET_IPV6_SYMLINK_CREATE
	ln -sf xtables-multi $(TARGET_DIR)/usr/sbin/ip6tables
	ln -sf xtables-multi $(TARGET_DIR)/usr/sbin/ip6tables-save
	ln -sf xtables-multi $(TARGET_DIR)/usr/sbin/ip6tables-restore	
endef

else # != 1.4.13

define IPTABLES_TARGET_SYMLINK_CREATE
	ln -sf iptables-multi $(TARGET_DIR)/usr/sbin/iptables
	ln -sf iptables-multi $(TARGET_DIR)/usr/sbin/iptables-save
	ln -sf iptables-multi $(TARGET_DIR)/usr/sbin/iptables-restore
endef

define IPTABLES_TARGET_IPV6_SYMLINK_CREATE
	ln -sf ip6tables-multi $(TARGET_DIR)/usr/sbin/ip6tables
	ln -sf ip6tables-multi $(TARGET_DIR)/usr/sbin/ip6tables-save
	ln -sf ip6tables-multi $(TARGET_DIR)/usr/sbin/ip6tables-restore
endef

endif # iptables

define IPTABLES_TARGET_IPV6_REMOVE
	rm -f $(TARGET_DIR)/usr/lib/libip6tc.*
endef

IPTABLES_POST_INSTALL_TARGET_HOOKS += IPTABLES_TARGET_SYMLINK_CREATE

ifeq ($(BR2_INET_IPV6),y)
IPTABLES_POST_INSTALL_TARGET_HOOKS += IPTABLES_TARGET_IPV6_SYMLINK_CREATE
else
IPTABLES_POST_INSTALL_TARGET_HOOKS += IPTABLES_TARGET_IPV6_REMOVE
endif

define IPTABLES_UNINSTALL_TARGET_CMDS
	rm -f $(TARGET_DIR)/usr/bin/iptables-xml
	rm -f $(TARGET_DIR)/usr/sbin/iptables* $(TARGET_DIR)/usr/sbin/ip6tables*
	rm -rf $(TARGET_DIR)/usr/lib/xtables
endef

$(eval $(call AUTOTARGETS))
else # for rtl_modem branch
define IPTABLES_BUILD_CMDS
	CC="$(TARGET_CC)" LD="$(TARGET_LD)" $(MAKE) V=99 -C ${@D} KERNEL_DIR=${LINUX26_DIR}
endef

define IPTABLES_INSTALL_TARGET_CMDS
	$(INSTALL) -D -m 0755 $(@D)/iptables $(TARGET_DIR)/usr/bin
	$(INSTALL) -D -m 0755 $(@D)/iptables-save $(TARGET_DIR)/usr/bin
	$(INSTALL) -D -m 0755 $(@D)/iptables-restore $(TARGET_DIR)/usr/bin
endef

define IPTABLES_TARGET_IPV6
	$(INSTALL) -D -m 0755 $(@D)/ip6tables $(TARGET_DIR)/usr/bin
	$(INSTALL) -D -m 0755 $(@D)/ip6tables-save $(TARGET_DIR)/usr/bin
	$(INSTALL) -D -m 0755 $(@D)/ip6tables-restore $(TARGET_DIR)/usr/bin
endef

ifeq ($(BR2_INET_IPV6),y)
  IPTABLES_POST_INSTALL_TARGET_HOOKS += IPTABLES_TARGET_IPV6
endif

$(eval $(call GENTARGETS))
endif #end->>> ifneq ($(call qstrip,$(BR2_IPTABLES_VERSION)),rtl_modem) 
