#############################################################
#
# madwimax
#
#############################################################

MADWIMAX_VERSION = 0.1.1
MADWIMAX_SOURCE = madwimax-$(MADWIMAX_VERSION).tar.gz
MADWIMAX_SITE = ${DLINK_STORAGE}
MADWIMAX_DEPENDENCIES = libusb
MADWIMAX_CONF_OPT += --without-man-pages --program-transform-name=madwimax
ifeq (${BR2_PACKAGE_MADWIMAX},y)
TARGET_CFLAGS += -DSUPPORT_WIMAX
endif

$(eval $(call AUTOTARGETS))
