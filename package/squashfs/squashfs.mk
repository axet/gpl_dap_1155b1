SQUASHFS_VERSION=4.2
SQUASHFS_SITE=${DLINK_STORAGE}
HOST_SQUASHFS_DEPENDENCIES = mklibs

ifneq ($(BRCM), y)
SQUASHFS_SOURCE=squashfs$(SQUASHFS_VERSION).tar.gz

# no libattr in BR
HOST_SQUASHFS_DEPENDENCIES = host-zlib host-xz

# no libattr/xz in BR
HOST_SQUASHFS_MAKE_ARGS = \
	XATTR_SUPPORT=0 \
	XZ_SUPPORT=1    \
	GZIP_SUPPORT=1  \
	LZO_SUPPORT=0	\
	LZMA_XZ_SUPPORT=1

define HOST_SQUASHFS_BUILD_CMDS
 $(HOST_MAKE_ENV) $(MAKE) \
   CC="$(HOSTCC)" \
   EXTRA_CFLAGS="$(HOST_CFLAGS)"   \
   EXTRA_LDFLAGS="$(HOST_LDFLAGS)" \
   $(HOST_SQUASHFS_MAKE_ARGS) \
   -C $(@D)/squashfs-tools/
endef

define HOST_SQUASHFS_INSTALL_CMDS
 $(HOST_MAKE_ENV) $(MAKE) $(HOST_SQUASHFS_MAKE_ARGS) \
   -C $(@D)/squashfs-tools/ INSTALL_DIR=$(HOST_DIR)/usr/bin install
endef

else # brcm

SQUASHFS_VERSION=4.0
SQUASHFS_SOURCE=sqlzma-bcm-4.0.tar.bz2

define HOST_SQUASHFS_BUILD_CMDS
	$(HOST_MAKE_ENV) $(MAKE) \
   CC="$(HOSTCC)" \
   EXTRA_CFLAGS="$(HOST_CFLAGS)"   \
   EXTRA_LDFLAGS="$(HOST_LDFLAGS)" \
   $(HOST_SQUASHFS_MAKE_ARGS) \
   -C $(@D)
endef


define HOST_SQUASHFS_INSTALL_CMDS
	install -d $(HOST_DIR)/usr/bin
	install -pm0755 ${@D}/mksquashfs $(HOST_DIR)/usr/bin/mksquashfs
	install -pm0755 ${@D}/lzmacmd $(HOST_DIR)/usr/bin/lzmacmd
endef

endif


$(eval $(call GENTARGETS,host))
