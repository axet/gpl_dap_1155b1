#############################################################
#
# mtd-write
#
#############################################################

MTD_WRITE_DIR=$(BUILD_DIR)/mtd-write
$(MTD_WRITE_DIR)/mtd.c:
	rm -rf $(MTD_WRITE_DIR)
	mkdir $(MTD_WRITE_DIR)
	cp package/mtd-write/mtd.* $(MTD_WRITE_DIR)

$(MTD_WRITE_DIR)/mtd_write: $(MTD_WRITE_DIR)/mtd.c
	$(TARGET_CC) -Os -pipe -fPIC -fomit-frame-pointer -I$(STAGING_DIR)/usr/include -I$(LINUX26_DIR)/include $(MTD_WRITE_DIR)/mtd.c -o $@

$(TARGET_DIR)/usr/bin/mtd_write: $(MTD_WRITE_DIR)/mtd_write
	$(INSTALL) -m 755 $^ $@
mtd-write: $(TARGET_DIR)/usr/bin/mtd_write
