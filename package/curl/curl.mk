#############################################################
#
# curl
#
#############################################################
CURL_VERSION = master
CURL_SITE = $(DLINK_GIT_STORAGE)/curl
CURL_AUTORECONF = NO
CURL_INSTALL_STAGING = YES
CURL_INSTALL_TARGET = YES

CURL_CONF_OPT = \
	--enable-shared \
	--enable-static \
	--disable-thread \
	--enable-cookies \
	--enable-crypto-auth \
	--enable-nonblocking \
	--enable-file \
	--enable-ftp \
	--enable-http \
	--disable-ares \
	--disable-debug \
	--disable-dict \
	--disable-gopher \
	--disable-ldap \
	--disable-manual \
	--disable-sspi \
	--disable-telnet \
	--enable-tftp \
	--disable-verbose \
	--with-random="/dev/urandom" \
	--with-ssl="$(STAGING_DIR)/usr" \
	--without-ca-bundle \
	--without-gnutls \
	--without-krb4 \
	--without-libidn \
	--without-nss \
	--without-libssh2 \
	--with-zlib="$(STAGING_DIR)/usr"


$(eval $(call AUTOTARGETS,package,curl))
