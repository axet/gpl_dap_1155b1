#############################################################
#
# ez-ipupdate
#
#############################################################

EZ_IPUPDATE_VERSION = $(call qstrip,$(BR2_EZ_IPUPDATE_BRANCH))
ifeq (${EZ_IPUPDATE_VERSION},)
EZ_IPUPDATE_VERSION = $(call qstrip,$(BR2_DEFAULT_BRANCH))
endif

EZ_IPUPDATE_SITE = $(DLINK_GIT_STORAGE)/ez-ipupdate


$(eval $(call AUTOTARGETS))
