#############################################################
#
# udhcp
#
#############################################################

UDHCP_VERSION = $(call qstrip,$(BR2_UDHCP_VERSION))
ifeq (${UDHCP_VERSION},)
UDHCP_VERSION = $(call qstrip,$(BR2_DEFAULT_BRANCH))
endif

ifeq ($(strip $(PROFILE)), DIR_620BEE)
TARGET_CFLAGS+=-DBROADCAST_ONLY
endif

ifeq ($(strip $(PROFILE)), DIR_300NRU_MLD)
TARGET_CFLAGS+=-DOPT_58_59
TARGET_CFLAGS+=-DOPT_60
TARGET_CFLAGS+=-DVENDOR_ID='\"dslforum.org\"'
endif

UDHCP_CFLAGS=""

ifeq ($(strip $(PROFILE)), DIR_300ABEE)
UDHCP_CFLAGS+=-DOPT_60
UDHCP_CFLAGS+=-DVENDOR_ID='\"beelinerouter\"'
endif

ifeq ($(strip $(PROFILE)), DIR_300NRUB7_TAT)
UDHCP_CFLAGS+=-DOPT_60
UDHCP_CFLAGS+=-DVENDOR_ID='\"<DLINK_DIR300>\"'
endif


UDHCP_SITE=$(DLINK_GIT_STORAGE)/udhcp
UDHCP_MAKE_OPTS = \
	CFLAGS="$(TARGET_CFLAGS) $(UDHCP_CFLAGS) -DVERSION='\"0.9.8\"'" \
	CC="$(TARGET_CC)" \
	LD="$(TARGET_CC)" \
	PREFIX="$(TARGET_DIR)" \
	EXTRA_LDFLAGS="$(TARGET_LDFLAGS) -L$(TARGET_DIR)/lib/" \
	CROSS_COMPILE="$(TARGET_CROSS)" 

define UDHCP_BUILD_CMDS
	$(MAKE) $(UDHCP_MAKE_OPTS) -C $(@D) all
endef
define UDHCP_INSTALL_TARGET_CMDS
	install -pm0755 $(@D)/udhcpc $(TARGET_DIR)/usr/bin/
endef
define UDHCP_CLEAN_CMDS
	$(MAKE) -C $(@D) clean
endef
$(eval $(call GENTARGETS))
