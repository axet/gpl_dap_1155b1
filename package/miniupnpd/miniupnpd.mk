#############################################################
#
# miniupnpd
#
#############################################################

MINIUPNPD_VERSION = 1.8
MINIUPNPD_SITE = $(DLINK_GIT_STORAGE)/miniupnpd
MINIUPNPD_DEPENDENCIES = iptables libnfnetlink
MINIUPNPD_LDFLAGS = $(TARGET_LDFLAGS) -liptc -lnfnetlink

ifeq ($(call qstrip,$(BR2_IPTABLES_VERSION)),rtl_1.4.4)
  MINIUPNPD_LDFLAGS += -liptc
else ifeq ($(call qstrip,$(BR2_IPTABLES_VERSION)),rtl_modem)
  MINIUPNPD_LDFLAGS += -liptc
else # актуальные версии iptables
  MINIUPNPD_LDFLAGS += -lip4tc
endif

MINIUPNPD_MAKE_ENV = \
		     CC="$(TARGET_CC)" \
		     LD="$(TARGET_LD)"

MINIUPNPD_MAKE_OPTS = \
		      LIBS="" \
		      CFLAGS="$(TARGET_CFLAGS) -DIPTABLES_143 -I${STAGING_DIR}/usr/include" \
		      LDFLAGS="$(MINIUPNPD_LDFLAGS)" \
		      STRIP=${TARGET_STRIP} \
		      IPTABLESPATH=${IPTABLES_DIR}


define MINIUPNPD_BUILD_CMDS
  $(MINIUPNPD_MAKE_ENV) $(MAKE) $(MINIUPNPD_MAKE_OPTS) V=99 -f Makefile.linux -C $(@D) miniupnpd
endef

define MINIUPNPD_INSTALL_TARGET_CMDS
  $(INSTALL) $(@D)/miniupnpd $(TARGET_DIR)/sbin
endef

$(eval $(call GENTARGETS))
