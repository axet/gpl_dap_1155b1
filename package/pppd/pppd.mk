#############################################################
#
# pppd
#
#############################################################
PPPD_VERSION = $(call qstrip,$(BR2_PPPD_VERSION))
ifeq ($(call qstrip,$(BR2_PPPD_VERSION)), )
PPPD_VERSION = $(call qstrip,$(BR2_DEFAULT_BRANCH))
endif

ifeq ($(BR2_rlx),y)
ifneq ($(RTL8676),y)
	PPPD_VERSION = rtl
endif
endif

ifeq ($(PPPD_VERSION),master)
TARGET_CFLAGS+= -DMPPE_OPTIONAL
endif

PPPD_SITE = $(DLINK_GIT_STORAGE)/ppp
PPPD_MAKE_OPT = INSTALL_DIR=$(TARGET_DIR) STRIP=${TARGET_STRIP}


# for nonrlx and 2640RTL
ifneq ($(PPPD_VERSION),rtl)
CONFIGURE := ./configure --target=$(GNU_TARGET_NAME) \
		--host=$(GNU_TARGET_NAME) \
		--build=$(GNU_HOST_NAME) \
		--prefix=/usr \
		--exec-prefix=/usr \
		--sysconfdir=/tmp \
		$(DISABLE_DOCUMENTATION) \
		$(DISABLE_NLS) \
		$(DISABLE_LARGEFILE) \
		$(DISABLE_IPV6) \
		$(QUIET)  

ifeq ($(BR2_PACKAGE_PPPD_PPTP),y)
	PLUGINS += pptp
endif
ifeq ($(BR2_PACKAGE_PPPD_L2TP),y)
	PLUGINS += pppol2tp
endif
ifeq ($(BR2_PACKAGE_PPPD_PPPOE),y)
	PLUGINS += rp-pppoe
endif

ifeq ($(BR2_PACKAGE_PPPD_PPPOA),y)
	PLUGINS += pppoatm
endif

ifeq ($(BR2_PACKAGE_PPPD_MEDIATEK_OPENL2TP),y)
	PLUGINS += mediatek_openl2tp
endif
ifeq ($(BR2_PACKAGE_PPPD_MEDIATEK_PPPOE),y)
	PLUGINS += mediatek_rp-pppoe
endif
ifeq ($(BR2_PACKAGE_PPPD_MEDIATEK_PPTP),y)
	PLUGINS += mediatek_pptp
endif

ifeq ($(BR2_INET_IPV6),y)
	PPPD_MAKE_OPT += HAVE_INET6=y
endif

# define PPPD_BUILD_CMDS
#	$(MAKE1) CC="$(TARGET_CC)" EXTRA_CFLAGS="-I$(@D) -I$(@D)/include $(TARGET_CFLAGS)  -I${STAGING_DIR}/usr/include" \
#		-C $(@D) $(PPPD_MAKE_OPT) install
# endef

define PPPD_CONFIGURE_CMDS
	cd ${@D} && ${CONFIGURE} UNAME_S="Linux" UNAME_M="$(ARCH)" UNAME_R="2.6.21"
endef
define PPPD_BUILD_CMDS
	${MAKE} CC="$(TARGET_CC)" LD="$(TARGET_LD)" EXTRA_CFLAGS="" -C $(@D) SUBDIRS="$(PLUGINS)" $(PPPD_MAKE_OPT)
endef

define PPPD_INSTALL_TARGET_CMDS
	${MAKE} -C ${@D} install INSTROOT=$(TARGET_DIR) DESTDIR=$(TARGET_DIR) STRIP=${TARGET_STRIP} SUBDIRS="$(PLUGINS)"
endef

else #($(PPPD_VERSION),rtl)
###############################################################################################
###              REALTEK								    ###
############################################################################################### 
PPPD_MAKE_OPT += CC=$(TARGET_CC) LD=$(TARGET_LD) STRIP=$(TARGET_STRIP)

define PPPD_BUILD_CMDS
	$(PPPD_MAKE_OPT) ${MAKE} -C $(@D)/ppp-2.4.4/pppd/plugins/rp-pppoe
	$(PPPD_MAKE_OPT) ${MAKE} -C $(@D)/ppp-2.4.4/pppd
	$(PPPD_MAKE_OPT) ${MAKE} -C $(@D)/pptp-1.7.2
	$(PPPD_MAKE_OPT) ${MAKE} -C $(@D)/l2tpd
	$(PPPD_MAKE_OPT) ${MAKE} -C $(@D)/ppp-2.4.4/chat
endef

define PPPD_INSTALL_TARGET_CMDS
	install -d ${TARGET_DIR}/bin
	install -pm0755 ${@D}/l2tpd/l2tpd $(TARGET_DIR)/bin/
	install -pm0755 ${@D}/pptp-1.7.2/pptp $(TARGET_DIR)/bin/
	install -pm0755 ${@D}/ppp-2.4.4/pppd/pppd $(TARGET_DIR)/bin/
	install -pm0755 ${@D}/ppp-2.4.4/chat/chat $(TARGET_DIR)/bin/
endef

endif #($(BR2_rlx),y)

define PPPD_CLEAN_CMDS
	$(PPPD_MAKE_OPT) ${MAKE} -C $(@D) clean
endef

$(eval $(call GENTARGETS))
