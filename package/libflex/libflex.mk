#############################################################
#
# libflex
#
#############################################################
LIBFLEX_VERSION = ralink
#LIBFLEX_SOURCE = libflex-$(LIBFLEX_VERSION).tar.bz2
#LIBFLEX_SITE = ${DLINK_STORAGE}
LIBFLEX_SITE = $(DLINK_GIT_STORAGE)/libflex
LIBFLEX_DEPENDENCIES = host-pkg-config
LIBFLEX_INSTALL_STAGING = YES
LIBFLEX_INSTALL_TARGET = YES
#LIBFLEX_CONF_OPT += enable_timerfd=no
LIBFLEX_MAKE_OPT = \
	INSTALL_DIR="$(TARGET_DIR)" \
	CC="$(TARGET_CC)" \
	MAKE="$(MAKE)" \
	LD="$(TARGET_LD)" \
	STRIP="$(TARGET_STRIP)" \
	STRIPTOOL="$(TARGET_STRIP)" \
	INSTALL="$(INSTALL)" \
	MAJOR_VERSION=0 \
	MINOR_VERSION=9 \
	SUBLEVEL=28 \
	LN="ln" \
	TOPDIR="$(TARGET_DIR)/" \

define LIBFLEX_BUILD_CMDS
	${MAKE} $(LIBFLEX_MAKE_OPT)  -C $(@D) shared
endef
define LIBFLEX_INSTALL_TARGET_CMDS
	cp  $(TOPDIR)/package/libflex/libfl-0.9.28.so $(TARGET_DIR)/lib/libfl-0.9.28.so
	$(TARGET_STRIP) $(TARGET_DIR)/lib/libfl-0.9.28.so
endef

define LIBFLEX_CLEAN_CMDS
	${MAKE} -C $(@D) clean
endef
$(eval $(call GENTARGETS))
