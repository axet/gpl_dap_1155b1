#############################################################
#
# openssl
#
#############################################################

OPENSSL_OCTEON_VERSION = 0.9.8o
OPENSSL_OCTEON_SITE = ${DLINK_STORAGE}
OPENSSL_OCTEON_SOURCE:=openssl-$(OPENSSL_VERSION).tar.bz2
OPENSSL_OCTEON_INSTALL_STAGING = YES
OPENSSL_OCTEON_DEPENDENCIES = zlib
OPENSSL_OCTEON_CFLAGS = $(TARGET_CFLAGS)
define OPENSSL_OCTEON_BUILD_CMDS
	$(TARGET_MAKE_ENV) CC=${TARGET_CC} RANLIB=${TARGET_RANLIB} PERL="/usr/bin/perl" $(MAKE1) -C $(@D) INSTALL_DIR=${TARGET_DIR} EXTRA_CFLAGS="${OPENSSL_OCTEON_CFLAGS}" install
endef
define OPENSSL_OCTEON_INSTALL_STAGING_CMDS
	cp ${@D}/libcrypto.so.0.9.8 ${STAGING_DIR}/usr/lib/
	cp ${@D}/libssl.so.0.9.8 ${STAGING_DIR}/usr/lib/
	mkdir -p ${STAGING_DIR}/usr/include/openssl
	find ${@D} -name "*.h" -exec cp "{}" ${STAGING_DIR}/usr/include/openssl \;
endef
OPENSSL_OCTEON_POST_INSTALL_STAGING_HOOKS += LIBCUEFILE_INSTALL_STAGING_INCLUDES
$(eval $(call GENTARGETS,package,openssl_octeon))
