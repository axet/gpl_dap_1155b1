#############################################################
#
# lzma
#
#############################################################
LZMAG_VERSION:=4.32.7
LZMAG_SOURCE:=lzma-$(LZMAG_VERSION).tar.gz
LZMAG_SITE:=${DLINK_STORAGE}
LZMAG_INSTALL_STAGING = YES
LZMAG_INSTALL_TARGET = YES
LZMAG_CONF_OPT = $(if $(BR2_ENABLE_DEBUG),--enable-debug,--disable-debug)

$(eval $(call AUTOTARGETS))
$(eval $(call AUTOTARGETS,host))

LZMA=$(HOST_DIR)/usr/bin/lzma
