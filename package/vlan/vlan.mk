#############################################################
#
# vconfig
#
#############################################################
VLAN_VERSION = 1
VLAN_SITE = ${DLINK_STORAGE}
VLAN_SOURCE = vlan-$(VLAN_VERSION).tar.bz2

define VLAN_BUILD_CMDS
	$(MAKE) -C $(@D) OPTIMIZE= DEBUG= \
		CC="$(TARGET_CC)" \
		CFLAGS="$(TARGET_CFLAGS) " \
		LDFLAGS="$(TARGET_LDFLAGS)" vconfig
endef

define VLAN_INSTALL_TARGET_CMDS
	$(INSTALL) -m 0755 -D $(@D)/vconfig $(TARGET_DIR)/usr/sbin/vconfig
endef

$(eval $(call GENTARGETS,package,vlan))

