#############################################################
#
# miin_httpd
#
#############################################################

MINI_HTTPD_VERSION := 1.19
MINI_HTTPD_SOURCE = mini_httpd-$(MINI_HTTPD_VERSION).tar.bz2
MINI_HTTPD_DEPENDENCIES = matrixssl
MINI_HTTPD_SITE = ${DLINK_STORAGE}

define MINI_HTTPD_BUILD_CMDS
      ${TARGET_MAKE_ENV} CC="$(TARGET_CROSS)gcc" ${MAKE} -C ${@D} OFLAGS="${TARGET_CFLAGS}"
endef

define MINI_HTTPD_INSTALL_TARGET_CMDS
      install -d ${TARGET_DIR}/etc ${TARGET_DIR}/usr/sbin
      install -pm 0755 ${@D}/mini_httpd  ${TARGET_DIR}/usr/sbin/httpd
      install -pm 0644 ${@D}/mini_httpd-ssl.conf ${@D}/mini_httpd.pem ${TARGET_DIR}/etc
endef

$(eval $(call GENTARGETS))