#############################################################
#
# iproute2
#
#############################################################

IPROUTE2_VERSION =$(call qstrip,$(BR2_IPROUTE2_VERSION))
#IPROUTE2_SOURCE = iproute2-$(IPROUTE2_VERSION).tar.bz2
#IPROUTE2_SITE = ${DLINK_STORAGE}

ifeq (${IPROUTE2_VERSION},)
IPROUTE2_VERSION = $(call qstrip,$(BR2_DEFAULT_BRANCH))
endif

IPROUTE2_SITE = $(DLINK_GIT_STORAGE)/iproute2

IPROUTE2_TARGET_SBINS = ctstat genl ifstat ip lnstat nstat routef routel rtacct rtmon rtpr rtstat ss tc

# If both iproute2 and busybox are selected, make certain we win
# the fight over who gets to have their utils actually installed.
ifeq ($(BR2_PACKAGE_BUSYBOX),y)
IPROUTE2_DEPENDENCIES += busybox
endif

SUBDIRS=lib ip misc
ifeq ($(BR2_IPROUTE2_TC),y)
SUBDIRS += tc
endif

ifeq ($(call qstrip,$(BR2_IPROUTE2_VERSION)),2.6.22)
IPROUTE2_OPTS = INSTALL_DIR=${TARGET_DIR}

ifeq ($(BR2_INET_IPV6),y)
IPROUTE2_OPTS += CONFIG_IPV6=y
endif

define IPROUTE2_BUILD_CMDS
	$(MAKE) CC="$(TARGET_CC)" SUBDIRS="$(SUBDIRS)" -C $(@D) ${IPROUTE2_OPTS} install
endef

endif # 2.6.22

ifeq ($(call qstrip,$(BR2_IPROUTE2_VERSION)),"2.6.35")

# If we've got iptables enable xtables support for tc
ifeq ($(BR2_PACKAGE_IPTABLES),y)
IPROUTE2_DEPENDENCIES += iptables
define IPROUTE2_WITH_IPTABLES
	# Makefile is busted so it never passes IPT_LIB_DIR properly
	$(SED) "s/-DIPT/-DXT/" $(IPROUTE2_DIR)/tc/Makefile
	echo "TC_CONFIG_XT:=y" >>$(IPROUTE2_DIR)/Config
endef
endif

define IPROUTE2_CONFIGURE_CMDS
	# Use kernel headers
	rm -r $(IPROUTE2_DIR)/include/netinet
	# FIXME!!!
	cp ${STAGING_DIR}/usr/include/xtables.h $(IPROUTE2_DIR)/include/
	# arpd needs berkeleydb
	$(SED) "/^TARGETS=/s: arpd : :" $(IPROUTE2_DIR)/misc/Makefile
	$(SED) "s:-O2:$(TARGET_CFLAGS):" $(IPROUTE2_DIR)/Makefile
	echo "IPT_LIB_DIR:=/usr/lib/xtables" >>$(IPROUTE2_DIR)/Config
	$(IPROUTE2_WITH_IPTABLES)
endef

define IPROUTE2_BUILD_CMDS
	$(MAKE) SUBDIRS="$(SUBDIRS)" CC="$(TARGET_CC)" LDFLAGS="${TARGET_LDFLAGS} -L${TARGET_DIR}/usr/lib" -C $(@D)
endef

define IPROUTE2_INSTALL_TARGET_CMDS
	$(MAKE) -C $(@D)  SUBDIRS="$(SUBDIRS)" DESTDIR="$(TARGET_DIR)" SBINDIR=/sbin \
		DOCDIR=/usr/share/doc/iproute2-$(IPROUTE2_VERSION) \
		MANDIR=/usr/share/man install
	# Wants bash
	rm -f $(TARGET_DIR)/sbin/ifcfg
	cp -R $(@D)/include/libiptc ${STAGIND_DIR}/usr/include/
endef

define IPROUTE2_UNINSTALL_TARGET_CMDS
	rm -rf $(TARGET_DIR)/lib/tc
	rm -rf $(TARGET_DIR)/usr/lib/tc
	rm -rf $(TARGET_DIR)/etc/iproute2
	rm -rf $(TARGET_DIR)/var/lib/arpd
	rm -f $(addprefix $(TARGET_DIR)/sbin/, $(IPROUTE2_TARGET_SBINS))
endef
endif # 2.6.35

ifeq ($(call qstrip,$(BR2_IPROUTE2_VERSION)),3.3.0)
ifeq ($(BR2_PACKAGE_IPTABLES),y)
IPROUTE2_DEPENDENCIES += iptables
define IPROUTE2_WITH_IPTABLES
	# Makefile is busted so it never passes IPT_LIB_DIR properly
	$(SED) "s/-DIPT/-DXT/" $(IPROUTE2_DIR)/tc/Makefile
	echo "TC_CONFIG_XT:=y" >>$(IPROUTE2_DIR)/Config
endef
endif

define IPROUTE2_CONFIGURE_CMDS
	# arpd needs berkeleydb
	$(SED) "/^TARGETS=/s: arpd : :" $(IPROUTE2_DIR)/misc/Makefile
	echo "IPT_LIB_DIR:=/usr/lib/xtables" >>$(IPROUTE2_DIR)/Config
	$(IPROUTE2_WITH_IPTABLES)
endef

define IPROUTE2_BUILD_CMDS
	$(SED) 's/$$(CCOPTS)//' $(@D)/netem/Makefile
	$(MAKE) CC="$(TARGET_CC)"  SUBDIRS="$(SUBDIRS)" CCOPTS="$(TARGET_CFLAGS) -D_GNU_SOURCE" -C $(@D)
endef

define IPROUTE2_INSTALL_TARGET_CMDS
	$(MAKE) -C $(@D)  SUBDIRS="$(SUBDIRS)" DESTDIR="$(TARGET_DIR)" SBINDIR=/sbin \
		DOCDIR=/usr/share/doc/iproute2-$(IPROUTE2_VERSION) \
		MANDIR=/usr/share/man install
	# Wants bash
	rm -f $(TARGET_DIR)/sbin/ifcfg
endef

define IPROUTE2_UNINSTALL_TARGET_CMDS
	rm -rf $(TARGET_DIR)/lib/tc
	rm -rf $(TARGET_DIR)/usr/lib/tc
	rm -rf $(TARGET_DIR)/etc/iproute2
	rm -rf $(TARGET_DIR)/var/lib/arpd
	rm -f $(addprefix $(TARGET_DIR)/sbin/, $(IPROUTE2_TARGET_SBINS))
endef
endif # 3.3.0
$(eval $(call GENTARGETS))
