#############################################################
#
# inadyn
#
#############################################################
INADYN_VERSION = 1.96.2
INADYN_SOURCE = inadyn-$(INADYN_VERSION).tar.gz
INADYN_SITE = ${DLINK_STORAGE}

define INADYN_BUILD_CMDS
	$(MAKE) V=99 CC="$(TARGET_CC)" LD="$(TARGET_LD)" CFLAGS="$(TARGET_CFLAGS) -std=gnu99" LDFLAGS="$(TARGET_LDFLAGS)" -C $(@D)
endef

define INADYN_INSTALL_TARGET_CMDS
	$(INSTALL) -D -m 0755 $(@D)/bin/linux/inadyn $(TARGET_DIR)/usr/bin/
endef

$(eval $(call GENTARGETS))
