#############################################################
#
# mkimage
#
#############################################################
HOST_MKIMAGE_DIR=$(BUILD_DIR)/host-mkimage

$(HOST_MKIMAGE_DIR)/mkimage.c:
	rm -rf $(HOST_MKIMAGE_DIR)
	mkdir $(HOST_MKIMAGE_DIR)
	cp package/mkimage/mkimage.c $(HOST_MKIMAGE_DIR)
	cp package/mkimage/*.h $(HOST_MKIMAGE_DIR)
	cp package/mkimage/crc32.c $(HOST_MKIMAGE_DIR)

$(HOST_MKIMAGE_DIR)/mkimage: $(HOST_MKIMAGE_DIR)/mkimage.c
	$(CC) -I$(HOST_MKIMAGE_DIR) -Wall -O2 -c $(HOST_MKIMAGE_DIR)/mkimage.c -o $(HOST_MKIMAGE_DIR)/mkimage.o
	$(CC) -I$(HOST_MKIMAGE_DIR) -Wall -O2 -c $(HOST_MKIMAGE_DIR)/crc32.c -o $(HOST_MKIMAGE_DIR)/crc32.o
	$(CC) -I$(HOST_MKIMAGE_DIR) -Wall -O2 $(HOST_MKIMAGE_DIR)/mkimage.o $(HOST_MKIMAGE_DIR)/crc32.o -o $@

#$(HOST_DIR)/usr/bin/mkimage: $(HOST_MKIMAGE_DIR)/mkimage
#	$(INSTALL) -m 755 $^ $@

$(HOST_DIR)/usr/bin/mkimage: package/mkimage/mkimage
	$(INSTALL) -m 755 $^ $@

host-mkimage: $(HOST_DIR)/usr/bin/mkimage
host-mkimage-source:
