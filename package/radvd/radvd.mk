#############################################################
#
# radvd
#
#############################################################
RADVD_SITE = $(DLINK_GIT_STORAGE)/radvd
RADVD_VERSION = master
RADVD_AUTORECONF = NO
RADVD_INSTALL_STAGING = YES
RADVD_INSTALL_TARGET = YES

RADVD_CONF_OPT = \
	CC="$(TARGET_CC)" \
	LD="$(TARGET_LD)" \
	PREFIX="$(TARGET_DIR)" \
	STRIP=${TARGET_STRIP} \
	INSTALL_DIR=$(TARGET_DIR) 



define RADVD_BUILD_CMDS
	$(MAKE1) ${RADVD_CONF_OPT} -C $(@D) radvd
endef

define RADVD_CLEAN_CMDS
	$(MAKE1) ${RADVD_CONF_OPT} -C  clean
endef

define RADVD_INSTALL_TARGET_CMDS
	$(MAKE1) ${RADVD_CONF_OPT} -C ${@D} install
endef


$(eval $(call GENTARGETS))
