#############################################################
#
# xl2tp
#
#############################################################
ifeq (${BCM47XX},y)
RP_L2TP_SOURCE = rp-l2tp-rtn.tar.bz2
else
RP_L2TP_SOURCE = rp-l2tp.tar.bz2
endif
RP_L2TP_SITE = ${DLINK_STORAGE}
RP_L2TP_CONF_ENV = STRIP=${TARGET_STRIP}


define RP_L2TP_INSTALL_TARGET_CMDS
	install -D ${@D}/l2tpd ${TARGET_DIR}/usr/sbin/l2tpd
	install -D ${@D}/handlers/l2tp-control ${TARGET_DIR}/usr/sbin/l2tp-control
	install -d ${TARGET_DIR}/usr/lib/l2tp/plugins
	install -D ${@D}/handlers/*.so ${TARGET_DIR}/usr/lib/l2tp/plugins
	

endef
$(eval $(call AUTOTARGETS))


