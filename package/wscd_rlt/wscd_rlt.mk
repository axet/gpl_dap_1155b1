#############################################################
#
# wscd_rlt
#
#############################################################
WSCD_RLT_VERSION =$(call qstrip,$(BR2_WSCD_RLT_VERSION))
ifeq (${WSCD_RLT_VERSION},)
WSCD_RLT_VERSION = $(call qstrip,$(BR2_DEFAULT_BRANCH))
endif

WSCD_RLT_SITE = $(DLINK_GIT_STORAGE)/wscd_rlt
WSCD_RLT_DEPENDENCIES = mini_upnp_rlt jansson libshared

WSCD_RLT_MAKE_OPTS = \
	CC=$(TARGET_CC) \
	LD=$(TARGET_LD) \
	STRIP=$(TARGET_STRIP) \
	INSTALL_DIR=$(TARGET_DIR)

WSCD_RTL_LIBPATH = \
	-L ${TARGET_DIR}/usr/lib -L ${TARGET_DIR}/lib \
	-I ${STAGING_DIR}/usr/include

define WSCD_RLT_BUILD_CMDS
	$(MAKE) $(WSCD_RLT_MAKE_OPTS) WSCD_LIBPATH="$(WSCD_RTL_LIBPATH)" -C $(@D) all
endef

define WSCD_RLT_INSTALL_TARGET_CMDS
	install $(@D)/src/wscd ${TARGET_DIR}/bin
	${TARGET_STRIP} ${TARGET_DIR}/bin/wscd
endef

$(eval $(call GENTARGETS))
