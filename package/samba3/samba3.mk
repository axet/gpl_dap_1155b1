#############################################################
#
# samba3
#
#############################################################
SAMBA3_VERSION = master
SAMBA3_SITE = $(DLINK_GIT_STORAGE)/samba3
SAMBA3_AUTORECONF = NO
SAMBA3_INSTALL_STAGING = YES
SAMBA3_INSTALL_TARGET = YES
SAMBA3_DEPENDENCIES = 
SAMBA3_CONFIG_FILE = smb.default

SAMBA3_CONF_OPT = \
	CC="$(TARGET_CC)" \
	SAMBA_CFLAGS="$(TARGET_CFLAGS) -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -I$(@D)/source/include" \
	CPPFLAGS="$(TARGET_CPPFLAGS) -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE" \
	LDFLAGS="$(TARGET_LDFLAGS) -L$(@D)/source/lib" \
	AR="$(TARGET_AR)" \
	RANLIB="$(TARGET_RANLIB)" \
	TARGETFS="$(@D)/source" \

define SAMBA3_BUILD_CMDS
	mkdir -p $(@D)/source/bin
	$(MAKE1) $(SAMBA3_CONF_OPT) -C $(@D)/source \
	all bin/smbpasswd bin/libsmb.so bin/smbd.shared bin/nmbd.shared
endef

define SAMBA3_INSTALL_TARGET_CMDS
	install -pm0755 $(@D)/source/bin/smbd.shared $(TARGET_DIR)/usr/bin/smbd
	install -pm0755 $(@D)/source/bin/nmbd.shared $(TARGET_DIR)/usr/bin/nmbd
	install -pm0755 $(@D)/source/bin/smbpasswd $(TARGET_DIR)/usr/bin/smbpasswd
	install -pm0755 $(@D)/source/bin/libsmb.so $(TARGET_DIR)/usr/lib/libsmb.so
	$(TARGET_STRIP) $(TARGET_DIR)/usr/bin/smbd
	$(TARGET_STRIP) $(TARGET_DIR)/usr/bin/nmbd
	$(TARGET_STRIP) $(TARGET_DIR)/usr/bin/smbpasswd
	$(TARGET_STRIP) $(TARGET_DIR)/usr/lib/libsmb.so
	$(INSTALL) -m 0755 -D package/samba3/$(SAMBA3_CONFIG_FILE) $(TARGET_DIR)/etc/
endef

define SAMBA3_CLEAN_CMDS
	$(MAKE1) -C $(@D)/source clean
endef


$(eval $(call GENTARGETS))
