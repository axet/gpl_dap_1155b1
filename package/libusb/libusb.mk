#############################################################
#
# libusb
#
#############################################################
LIBUSB_VERSION = 1.0.8
LIBUSB_SOURCE = libusb-$(LIBUSB_VERSION).tar.bz2
LIBUSB_SITE = ${DLINK_STORAGE}
LIBUSB_DEPENDENCIES = host-pkg-config
LIBUSB_INSTALL_STAGING = YES
LIBUSB_INSTALL_TARGET = YES
LIBUSB_CONF_OPT += enable_timerfd=no
$(eval $(call AUTOTARGETS))
