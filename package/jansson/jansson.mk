JANSSON_VERSION = $(call qstrip,$(BR2_JANSSON_VERSION))
JANSSON_SOURCE = jansson-$(JANSSON_VERSION).tar.bz2
JANSSON_SITE = ${DLINK_STORAGE}

# совместимость с профилями, где версия не указана
ifeq ($(JANSSON_VERSION),)
  JANSSON_VERSION = 1.3
endif

define JANSSON_COPY_HEADERS
  $(INSTALL) -D -m 0644 $(@D)/src/jansson.h ${STAGING_DIR}/usr/include/
endef

define JANSSON_COPY_HEADERS_TEMP
  $(INSTALL) -D -m 0644 $(@D)/src/jansson_config.h ${STAGING_DIR}/usr/include/
endef

JANSSON_POST_INSTALL_TARGET_HOOKS += JANSSON_COPY_HEADERS
ifeq ($(JANSSON_VERSION), 2.3.1)
  JANSSON_POST_INSTALL_TARGET_HOOKS += JANSSON_COPY_HEADERS_TEMP
endif

$(eval $(call AUTOTARGETS))
