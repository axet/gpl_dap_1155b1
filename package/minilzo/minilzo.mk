#############################################################
#
# minilzo
#
#############################################################
MINILZO_VERSION:=2.06
MINILZO_SOURCE:=minilzo-$(MINILZO_VERSION).tar.gz
MINILZO_SITE:=${DLINK_STORAGE}

define MINILZO_BUILD_CMDS
	$(MAKE) -C $(@D) CC="$(TARGET_CC)"
endef

define MINILZO_INSTALL_TARGET_CMDS
	install $(@D)/libminilzo.so $(TARGET_DIR)/usr/lib/
	find $(@D) -name "*.h" | xargs -i cp {} ${STAGING_DIR}/usr/include/
endef

define MINILZO_CLEAN_CMDS
	$(MAKE) -C $(@D) clean
endef



$(eval $(call GENTARGETS))
