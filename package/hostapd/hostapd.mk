#############################################################
#
# hostapd
#
#############################################################

HOSTAPD_VERSION = 1.0
HOSTAPD_SOURCE = hostapd-$(HOSTAPD_VERSION).tar.gz
HOSTAPD_SITE = http://hostap.epitest.fi/releases
HOSTAPD_DEPENDENCIES = libnl-tiny

HOSTAPD_CFLAGS=$(TARGET_CFLAGS) -I$(STAGING_DIR)/usr/include/libnl-tiny
HOSTAPD_LDFLAGS=$(TARGET_LDFLAGS)

HOSTAPD_CONFIG_FILE = $(call qstrip,$(BR2_PACKAGE_HOSTAPD_CONFIG))

define HOSTAPD_CONFIGURE_CMDS
	cp $(HOSTAPD_CONFIG_FILE) $(@D)/hostapd/.config
endef

define HOSTAPD_BUILD_CMDS
	cd $(@D)/hostapd/ && $(MAKE) V=1 CC=$(TARGET_CC) LD=$(TARGET_LD) EXTRA_CFLAGS="$(HOSTAPD_CFLAGS)" EXTRA_LDFLAGS="$(HOSTAPD_LDFLAGS)"
endef

ifeq ($(BR2_PACKAGE_HOSTAPD_CLI),y)
HOSTAPD_POST_INSTALL_TARGET_HOOKS += HOSTAPD_INSTALL_CLI
endif

define HOSTAPD_INSTALL_TARGET_CMDS
	$(INSTALL) -D $(@D)/hostapd/hostapd $(TARGET_DIR)/usr/bin/
endef

define HOSTAPD_INSTALL_CLI
	$(INSTALL) -D $(@D)/hostapd/hostapd_cli $(TARGET_DIR)/usr/bin/
endef

$(eval $(call GENTARGETS))
