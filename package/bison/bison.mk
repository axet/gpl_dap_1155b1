#############################################################
#
# bison
#
#############################################################

BISON_VERSION = 2.4.3
BISON_SITE = ${DLINK_STORAGE}

define BISON_DISABLE_EXAMPLES
	echo 'all install:' > $(@D)/examples/Makefile
endef

BISON_POST_CONFIGURE_HOOKS += BISON_DISABLE_EXAMPLES

$(eval $(call AUTOTARGETS))
$(eval $(call AUTOTARGETS,host))
