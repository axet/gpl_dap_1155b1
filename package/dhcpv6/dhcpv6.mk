#############################################################
#
# dhcpv6
#
#############################################################
DHCPV6_VERSION = master
DHCPV6_SITE = $(DLINK_GIT_STORAGE)/dhcpv6
DHCPV6_INSTALL_STAGING = YES
DHCPV6_INSTALL_TARGET = YES

DHCPV6_CONF_OPT = \
	CC="$(TARGET_CC)" \
	LD="$(TARGET_LD)" \
	PREFIX="$(TARGET_DIR)" \
	STRIP=${TARGET_STRIP} \
	INSTALL_DIR=$(TARGET_DIR) \

define DHCPV6_BUILD_CMDS
	$(MAKE1) ${DHCPV6_CONF_OPT} -C $(@D) 
endef

$(eval $(call GENTARGETS))
