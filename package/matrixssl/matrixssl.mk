#############################################################
#
# MatrixSSL
#
#############################################################

MATRIXSSL_VERSION := 1.2.4
MATRIXSSL_SOURCE = matrixssl-$(MATRIXSSL_VERSION).tar.gz
MATRIXSSL_SITE = ${DLINK_STORAGE}

define MATRIXSSL_BUILD_CMDS
	${TARGET_MAKE_ENV} CC="$(TARGET_CROSS)gcc" ${MAKE} EXTRA_CFLAGS="${TARGET_CFLAGS}" -C ${@D}/src
endef

define MATRIXSSL_INSTALL_TARGET_CMDS
	install -d ${TARGET_DIR}/usr/lib
	install -pm 0644 ${@D}/src/libmatrixssl.so.${MATRIXSSL_VERSION} ${TARGET_DIR}/usr/lib/libmatrixssl.so
endef

$(eval $(call GENTARGETS))