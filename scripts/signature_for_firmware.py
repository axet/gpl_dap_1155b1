#! /usr/bin/python
# -*- coding: utf-8 -*-

import struct
import md5
import sys

#######################################################################
#####                   Подписывание прошивки                     #####
#######################################################################

#  В конец прошивки добавляем блок из 20 байт: хеш + магик = 16 + 4 = 20.
#  Порядок - big endian.
#
#  Хеш вычисляется в hash_fw_uuid(fname, uuid), 
#      где fname - прошивка, uuid - ключ в виде строки длиной 32 символа
#
#  Магик - это 4-хбайтовый бинарный кончик хвоста вида 0x00C0FFEE, 
#      где 0xC0FFEE - это собсно магик, а 0x00 это версия алгоритма подписи. 
#      Если что-то изменим, то магик будет меняться 0x01C0FFEE, 0x02C0FFEE ...
#
#  Использовать:
#      signature_for_firmware.py <путь до файла прошивки> <uuid> <magic>
#

#----------------------------------------------------------------------
def writeTailInFile(fname, salt, magic):
    """Записывает в конец прошивки хвост 20 байт"""
    try:
        fw_file = open(fname, 'ab')
    except:
        print 'Failed to open %s\n' % fname
        return False
    for i in range(0, len(salt), 8):
        fw_file.write(struct.pack('>L', int(salt[i:i+8], 16)))
    fw_file.write(struct.pack('>L', int(magic, 16)))
    fw_file.close()
    return True

#----------------------------------------------------------------------
def hash_fw_uuid(fname, uuid):
    """Возвращает хеш от uuid + прошивка без хвоста"""
    try:
        f = open(fname, 'rb')
    except:
        print 'Failed to open %s\n' % fname
        return ''
    if len(uuid) < 32: return ''
    m = md5.new()
    for i in range(0, len(uuid), 8):
        m.update(struct.pack('>L', int(uuid[i:i+8], 16)))
    while True:
        d = f.read(8096)
        if not d:
            break
        m.update(d)
    f.close()
    
    return m.hexdigest()

def main(fname, uuid, magic):
    """"""
    ret = False
    salt = hash_fw_uuid(fname, uuid)
    if salt != '':
        ret = writeTailInFile(fname, salt, magic)
    return salt, ret


if __name__ == '__main__':
    fname = sys.argv[1]
    uuid = sys.argv[2]
    #magic = 0x00c0ffee, где 0xC0FFEE - это собсно магик, а 0x00 это версия алгоритма подписи. Если что-то изменим, то магик будет меняться 0x01C0FFEE, 0x02C0FFEE ...
    magic = sys.argv[3]
    salt, ret = main(fname, uuid.replace('0x',''), magic.replace('0x',''))
    print 'Hash = 0x%32s;\nMagic = %8s;\nAdd signature in the firmware %s : %s;' % (salt, magic, fname, ret)
