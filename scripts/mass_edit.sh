#!/bin/bash


add_new_opt(){
	echo $1=y >> $2;
    
}
change_opt(){
	sed -i "s/$1/$2/g" $3;
}
disable_opt(){
	sed "/\\<$1\\>/d" $2
	echo "# $1 is not set" >> $2
}
if [ $# -lt 2 ]
then
  echo "Usage: `basename $0` new {parameter}"
  echo "Usage: `basename $0` change {parameter=old_value} {parameter=new_value}"
  echo "Usage: `basename $0` disable {parameter}"
  exit 65
fi

PROFILES=`ls profiles`
#PROFILES=DIR_632
for i in ${PROFILES}; do 
     if [ $1 == "new" ]; then add_new_opt $2 profiles/$i/$i; fi
     if [ $1 == "change" ]; then change_opt $2 $3 profiles/$i/$i; fi
     if [ $1 == "disable" ]; then disable_opt $2 profiles/$i/$i; fi
done

