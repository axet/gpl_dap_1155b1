TOOLCHAIN_VERSION = $(call qstrip,$(BR2_TOOLCHAIN_EXTERNAL_VERSION))
ifeq ($(TOOLCHAIN_VERSION),)
TOOLCHAIN_VERSION = master
endif

toolchain: $(STAMP_DIR)/ext-toolchain-installed

$(STAMP_DIR)/ext-toolchain-installed:
	@$(call MESSAGE,"Downloading toolchain")
	git clone -b $(TOOLCHAIN_VERSION) git@rd:sdk_toolchains/$(BR2_TOOLCHAIN_EXTERNAL_LOCATION) $(BASE_DIR)/toolchains

	#Ebanen liben der copiren
	cp -aR $(BASE_DIR)/toolchains/${BR2_USRINC_PATH}/* ${STAGING_DIR}/usr/include/
	cp -aR $(BASE_DIR)/toolchains/${BR2_LIB_PATH}/* ${STAGING_DIR}/lib/
ifneq (${OCTEON},y)
	cp -aR $(BASE_DIR)/toolchains/${BR2_USRLIB_PATH}/* ${STAGING_DIR}/usr/lib/
endif
	chmod -R +w ${STAGING_DIR}/usr/include
	touch $@

include toolchain/gdb/gdb.mk
include toolchain/mklibs/mklibs.mk
