######################################################################
#
# mklibs
#
######################################################################


MKLIBS_PROGRAM:=$(STAGING_DIR)/bin/mklibs.py
MK_TARGETS := $(STAGING_DIR)/uclibc/lib* 
LIB_DIR := $(TOOLCHAIN_DIR)/${BR2_LIB_PATH}
$(MKLIBS_PROGRAM): toolchain/mklibs/mklibs.py
	cp -dpf toolchain/mklibs/mklibs.py $@
	mkdir -p $(STAGING_DIR)/uclibc
ifeq (${BR2_USRBIN_HACK},y)
TOOLCHAIN_BIN=$(TOOLCHAIN_DIR)/usr/bin
ROOT_ADDON=--root ${STAGING_DIR}
else
TOOLCHAIN_BIN=$(TOOLCHAIN_DIR)/bin
endif
mklibs-clean:
	rm -f $(MKLIBS_PROGRAM)

mklibs-dirclean:
	true

#############################################################
#
# Run mklibs
#
#############################################################
MKLIBS_PYTHON:=$(shell which python)
ifeq ($(MKLIBS_PYTHON),)
    MKLIBS_PYTHON=/usr/bin/python
endif

$(STAGING_DIR)/mklibs-stamp: $(MKLIBS_PROGRAM) $(MKLIBS_PYTHON)

ifneq ($(BR2_MKLIBS),)
	@$(call MESSAGE,"CASTRATING")
	find $(TARGET_DIR) -type f -perm +100 -exec \
	    file -r -N -F '' {} + | \
	    awk ' /executable.*dynamically/ { print $$1 }' > $(STAGING_DIR)/mklibs-progs
	
	cd $(STAGING_DIR); PATH=$(PATH):$(STAGING_DIR)/bin:$(TOOLCHAIN_BIN) $(MKLIBS_PYTHON) $(MKLIBS_PROGRAM) -D ${ROOT_ADDON} \
	    -L${TARGET_DIR}/lib -L${TARGET_DIR}/usr/lib -L${TARGET_DIR}/lib/private -L${TOOLCHAIN_DIR}/usr/mips-linux-uclibc/lib ${TARGET_LDFLAGS} \
	    --target $(BR2_TOOLCHAIN_EXTERNAL_PREFIX)- -d ./uclibc \
	    `cat $(STAGING_DIR)/mklibs-progs` 
else
ifneq (${BR2_rlx},)
	@$(call MESSAGE,"Shrinking libraries...")
	cp -R $(TOOLCHAIN_DIR)/lib/*.so ${TARGET_DIR}/lib
	cp -R $(TOOLCHAIN_DIR)/lib/*.so.* ${TARGET_DIR}/lib
	$(TOOLCHAIN_BIN)/rsdk-linux-lstrip ${TARGET_DIR}
	# А хуй знает почему так надо...
ifneq (${BR2_TOOLCHAIN_EXTERNAL_LOCATION}, "rtl_5281_toolchains")
	cp -R $(TOOLCHAIN_DIR)/lib/*.so ${TARGET_DIR}/lib
	cp -R $(TOOLCHAIN_DIR)/lib/*.so.* ${TARGET_DIR}/lib
	$(TOOLCHAIN_BIN)/rsdk-linux-lstrip ${TARGET_DIR}
endif
else
	@$(call MESSAGE,"Copy libraries...")
	#cp -af $(TOOLCHAIN_DIR)/$(BR2_TOOLCHAIN_EXTERNAL_PREFIX)/sys-root/lib/* ${TARGET_DIR}/lib/
	cp -aR $(BASE_DIR)/toolchains/${BR2_LIB_PATH}/*.so ${TARGET_DIR}/lib/
endif
endif
#	touch $@


mklibs: $(STAGING_DIR)/mklibs-stamp
ifneq ($(BR2_MKLIBS),)
	-install -p ${MK_TARGETS} ${TARGET_DIR}/lib
	cp -f $(TOOLCHAIN_DIR)/${BR2_LIB_PATH}/libdl.so* ${TARGET_DIR}/lib/
	-cp -f $(TOOLCHAIN_DIR)/${BR2_LIB_PATH}/ld-uClibc.so.0 ${TARGET_DIR}/lib/
# Костыль для недостающих либ, как нормально победить пока не знаю
ifeq (${BR2_brcm},y)
	cp -f $(TOOLCHAIN_DIR)/usr/mips-linux-uclibc/lib/libgcc_s.so.1 ${TARGET_DIR}/lib/
	cp -f $(TOOLCHAIN_DIR)/lib/libm-0.9.29.so ${TARGET_DIR}/lib/
	cp -f $(TOOLCHAIN_DIR)/lib/libcrypt-0.9.29.so ${TARGET_DIR}/lib/
	cp -f $(TOOLCHAIN_DIR)/lib/libuClibc-0.9.29.so ${TARGET_DIR}/lib/
	cp -f $(TOOLCHAIN_DIR)/lib/libresolv-0.9.29.so ${TARGET_DIR}/lib/
	cd ${TARGET_DIR}/lib/ ; ln -s libm-0.9.29.so libm.so.0 || true ; ln -s libresolv-0.9.29.so libresolv.so.0 || true ; ln -s libgcc_s.so.1 libgcc_s.so || true ; ln -s libcrypt-0.9.29.so libcrypt.so.0 || true ; ln -s libuClibc-0.9.29.so libc.so.0 || true ; \
	ln -s /lib/private/libxdslctl.so libxdslctl.so || true ; ln -s /lib/private/libatmctl.so libatmctl.so || true ; ln -s /lib/private/libboard.so libboard.so || true ; 
endif
ifeq (${BR2_ralink_modem},y)
	cp -f $(TOOLCHAIN_DIR)/usr/mips-linux-uclibc/lib/libgcc_s.so.1 ${TARGET_DIR}/lib/
	cp -f $(TOOLCHAIN_DIR)/lib/libuClibc-0.9.30.so ${TARGET_DIR}/lib/
	cp -f $(TOOLCHAIN_DIR)/lib/libm-0.9.30.so ${TARGET_DIR}/lib/
	cp -f $(TOOLCHAIN_DIR)/lib/libcrypt-0.9.30.so ${TARGET_DIR}/lib/
	cp -f $(TOOLCHAIN_DIR)/lib/libresolv-0.9.30.so ${TARGET_DIR}/lib/
	cd ${TARGET_DIR}/lib/ ; ln -s libm-0.9.30.so libm.so.0 || true ; ln -s libresolv-0.9.30.so libresolv.so.0 || true ; ln -s libgcc_s.so.1 libgcc_s.so || true ; ln -s libcrypt-0.9.30.so libcrypt.so.0 || true ; ln -s libuClibc-0.9.30.so libc.so.0 || true ;
endif
endif
	
