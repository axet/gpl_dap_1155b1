ifeq (${RTL8676},y)
pack_image:
	dd if=$(BINARIES_DIR)/$(LINUX26_IMAGE_NAME) of=$(BINARIES_DIR)/fw.bin bs=64k conv=sync
	dd if=$(BINARIES_DIR)/rootfs.squashfs of=$(BINARIES_DIR)/fw.bin oflag=append conv=notrunc
	cp $(BINARIES_DIR)/fw.bin $(BINARIES_DIR)/${FW_IMAGE_NAME}
else
ifneq ($(BR2_PRIVATE_KEY),)
UUID=${BR2_FW_UUID}
FW_MAGIK_SIGN=${BR2_FW_MAGIC_VERSION}c0ffee
SIGN_PROGRAM:=scripts/signature_for_firmware.py
SIGN_PYTHON:=$(shell which python)
ifeq ($(SIGN_PYTHON),)
    SIGN_PYTHON=/usr/bin/python
endif

pack_image:
	dd if=$(BINARIES_DIR)/$(LINUX26_IMAGE_NAME) of=$(BINARIES_DIR)/fw.bin bs=64k conv=sync
	dd if=/dev/zero of=$(BINARIES_DIR)/fw.bin bs=65520 count=1 oflag=append conv=notrunc
	dd if=$(BINARIES_DIR)/rootfs.squashfs of=$(BINARIES_DIR)/fw.bin oflag=append conv=notrunc
	$(HOST_DIR)/usr/bin/cvimg linux-ro $(BINARIES_DIR)/fw.bin $(BINARIES_DIR)/${FW_IMAGE_NAME} 0x80500000 30000
	$(SIGN_PYTHON) $(SIGN_PROGRAM) $(BINARIES_DIR)/$(FW_IMAGE_NAME) ${UUID} ${FW_MAGIK_SIGN}
else

pack_image:
	dd if=$(BINARIES_DIR)/$(LINUX26_IMAGE_NAME) of=$(BINARIES_DIR)/fw.bin bs=64k conv=sync
	dd if=/dev/zero of=$(BINARIES_DIR)/fw.bin bs=65520 count=1 oflag=append conv=notrunc
	dd if=$(BINARIES_DIR)/rootfs.squashfs of=$(BINARIES_DIR)/fw.bin oflag=append conv=notrunc
	$(HOST_DIR)/usr/bin/cvimg linux-ro $(BINARIES_DIR)/fw.bin $(BINARIES_DIR)/${FW_IMAGE_NAME} 0x80500000 30000
	
endif
endif
