# When building a target filesystem, it is desirable to not have to
# become root and then run 'mknod' a thousand times.  Using a device 
# table you can create device nodes and directories "on the fly".
#
# This is a sample device table file for use with genext2fs.  You can
# do all sorts of interesting things with a device table file.  For
# example, if you want to adjust the permissions on a particular file
# you can just add an entry like:
#   /sbin/foobar        f       2755    0       0       -       -       -       -       -
# and (assuming the file /sbin/foobar exists) it will be made setuid
# root (regardless of what its permissions are on the host filesystem.
# Furthermore, you can use a single table entry to create a many device
# minors.  For example, if I wanted to create /dev/hda and /dev/hda[0-15]
# I could just use the following two table entries:
#   /dev/hda    b       640     0       0       3       0       0       0       -
#   /dev/hda    b       640     0       0       3       1       1       1       15
# 
# Device table entries take the form of:
# <name>    <type>      <mode>  <uid>   <gid>   <major> <minor> <start> <inc>   <count>
# where name is the file name,  type can be one of: 
#       f       A regular file
#       d       Directory
#       c       Character special device file
#       b       Block special device file
#       p       Fifo (named pipe)
# uid is the user id for the target file, gid is the group id for the
# target file.  The rest of the entries (major, minor, etc) apply only 
# to device special files.

# Have fun
# -Erik Andersen <andersen@codepoet.org>
#

#<name>		<type>	<mode>	<uid>	<gid>	<major>	<minor>	<start>	<inc>	<count>
/dev		d	755	0	0	-	-	-	-	-
/dev/pts	d	755	0	0	-	-	-	-	-
/dev/shm	d	755	0	0	-	-	-	-	-
/tmp		d	1777	0	0	-	-	-	-	-
/etc		d	755	0	0	-	-	-	-	-
/home/default	d	2755	1000	1000	-	-	-	-	-
#<name>					<type>	<mode>	<uid>	<gid>	<major>	<minor>	<start>	<inc>	<count>
/bin/busybox				f	4755	0	0	-	-	-	-	-
/etc/shadow				f	600	0	0	-	-	-	-	-
## /etc/passwd				f	644	0	0	-	-	-	-	-
/usr/share/udhcpc/default.script	f	755	0	0	-	-	-	-	-
# uncomment this to allow starting x as non-root
#/usr/X11R6/bin/Xfbdev		f	4755	0	0	-	-	-	-	-
# Normal system devices
/dev/mem	c	640	0	0	1	1	0	0	-
/dev/kmem	c	640	0	0	1	2	0	0	-
/dev/null	c	666	0	0	1	3	0	0	-
/dev/zero	c	666	0	0	1	5	0	0	-
/dev/random	c	666	0	0	1	8	0	0	-
/dev/urandom	c	666	0	0	1	9	0	0	-
/dev/ram	b	640	0	0	1	1	0	0	-
/dev/ram	b	640	0	0	1	0	0	1	4
/dev/loop	b	640	0	0	7	0	0	1	2
/dev/rtc	c	640	0	0	10	135	-	-	-
/dev/console	c	666	0	0	5	1	-	-	-
/dev/tty	c	666	0	0	5	0	-	-	-
/dev/tty	c	666	0	0	4	0	0	1	8
/dev/ttyp	c	666	0	0	3	0	0	1	10
/dev/ptyp	c       666     0       0       2       0       0       1       10
/dev/ptmx	c	666	0	0	5	2	-	-	-
/dev/ttyP	c	666	0	0	57	0	0	1	4
/dev/ttyS	c	666	0	0	4	64	0	1	4
/dev/fb		c	640	0	5	29	0	0	1	4
#/dev/ttySA	c	666	0	0	204	5	0	1	3
/dev/psaux	c	666	0	0	10	1	0	0	-
/dev/ppp	c	666	0	0	108	0	-	-	-
/dev/ttyUSB	c	666	0	0	188	0	0	1	8
/dev/ttyACM0	c	666	0	0	166	0	0	1	-
/dev/cdc-wdm    c       666     0       0       180     176     0       1       8

/dev/ttymxc	c	666	0	0	207	16	0	1	3
/dev/gpio	c	666	0	0	252	0	-	-	-
/dev/gpiom	c	666	0	0	111	0	-	-	-

/dev/pts/	c	666	0	0	136	0	0	1	4

# Input stuff
/dev/input		d	755	0	0	-	-	-	-	-
/dev/input/event	c	660	0	0	13	64	0	1	4
#/dev/input/js		c	660	0	0	13	0	0	1	4


# MTD stuff
/dev/mtd	c	640	0	0	90	0	0	2	10
/dev/mtdblock	b	640	0	0	31	0	0	1	8

#Tun/tap driver
/dev/net	d	755	0	0	-	-	-	-	-
/dev/net/tun	c	660	0	0	10	200	-	-	-

# SCSI Devices
/dev/sda	b	640	0	0	8	0	0	0	-
/dev/sda	b	640	0	0	8	1	1	1	15
/dev/sdb	b	640	0	0	8	16	0	0	-
/dev/sdb	b	640	0	0	8	17	1	1	15
#/dev/sdc	b	640	0	0	8	32	0	0	-
#/dev/sdc	b	640	0	0	8	33	1	1	15

# I2C device nodes
/dev/i2c-	c	666	0	0	89	0	0	1	4

# FUSE
/dev/fuse	c	777	0	0	10	229	-	-	-
/dev/misc	d	777	0	0	-	-	-	-	-
/dev/misc/fuse	c	666	0	0	10	229	-	-	-

# USB PRINTER
/dev/usblp0	c	666	0	0	180	0

#ATH ART
/dev/dk0	c	666	0	0	63	0	-	-	-
/dev/dk1	c	666	0	0	63	1	-	-	-

/dev/caldata	b	666	0	0	31	6	-	-	-
 #NETLINK MONITOR
/dev/nlmon	c	666	0	0	202	0	0	0	-
