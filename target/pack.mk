TARGETS += pack_image

MKIMAGE:=$(HOST_DIR)/usr/bin/mkimage

ENTRY=`readelf -h $(LINUX26_DIR)/vmlinux|grep "Entry"|cut -d":" -f 2`
LDADDR=`readelf -a $(LINUX26_DIR)/vmlinux|grep "\[ 1\]"|cut -d" " -f 26`

ifneq ($(subst ",,${DLINK_DEVICEID}),)
DEVICE_ID=${DLINK_DEVICEID}
else
DEVICE_ID=${PROFILE}
endif

uimage: host-mkimage host-lzma
	$(LZMA) e $(BINARIES_DIR)/$(LINUX26_IMAGE_NAME) $(BINARIES_DIR)/$(LINUX26_IMAGE_NAME).lzma
	$(MKIMAGE) -A mips -O linux -T kernel -C lzma -a 0x${LDADDR} -e ${ENTRY} -n ${DEVICE_ID}    \
	-d $(BINARIES_DIR)/$(LINUX26_IMAGE_NAME).lzma $(BINARIES_DIR)/uImage

ifneq ($(BR2_ar7240),)
include target/ar7240/pack.mk
endif

ifneq ($(BR2_octeon),)
include target/octeon/pack.mk
endif

ifneq ($(BR2_ralink),)
include target/ralink/pack.mk
endif

ifneq ($(BR2_bcm47xx),)
include target/bcm47xx/pack.mk
endif

ifneq ($(BR2_brcm),)
include target/brcm/pack.mk
endif

ifneq ($(BR2_rlx),)
include target/rlx/pack.mk
endif

ifneq ($(BR2_ralink_modem),)
include target/ralink_modem/pack.mk
endif

TARGETS += move_image

ifeq (${HOSTNAME},rd)
move_image:
	mkdir -p /var/www/${PROFILE}
	mv $(BINARIES_DIR)/${FW_IMAGE_NAME} /var/www/${PROFILE}/
else
move_image:
endif

