# D-Link DAP-1155 B1

Original GPL firmware from D-link. kernel-2.6.30.9 used.

    3745fecff85fb7d124c150943cab8eb3  gpl_DAP_1155B1.tar.gz

2.0.10 firmare do not allow to flash gpl_DAP_1155B1 firmware.
Looks like device has special UUID (private key) for signing images. It can be
recovered from original frimware image. Or device can be flashed using recovery
mode (reset+power).

D-Link GPL archive has only kernel sources and few linux components also contains
a lot of prorietary .cgi and other scripts.

# Deps

    debootstrap --arch i386 squeeze squeeze
    chroot squeeze
    mount -t proc /proc /proc; mount -t sysfs /sys /sys; mount -t devtmpfs /dev /dev
    apt install bison make build-essentials python

# Links

* http://smarpl.com/content/d-link-dap-1155-review-and-teardown
* http://forum.archive.openwrt.org/viewtopic.php?id=46606
* https://git.nprove.in/rtl819xx/
* http://archive.main.lv/writeup/building_openwrt_for_rtl8196c.html
* http://archive.main.lv/writeup/rtl8196c_support_for_openwrt.html
* http://git.advem.lv/rtl819xx
* https://github.com/srchack/rtl819xx
* https://ftp.dlink.ru/pub/Wireless/DAP-1155/Firmware/Rev.B/GPL/gpl_DAP_1155B1.tar.gz
* https://openwrt.org/docs/techref/hardware/soc/soc.realtek
* https://github.com/hackpascal/lede-rtl8196c
