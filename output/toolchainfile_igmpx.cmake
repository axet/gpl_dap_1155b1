set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_C_COMPILER /home/builder/4798_GPL/gpl_DAP_1155B1/output/toolchains/bin/rsdk-linux-gcc)
set(CMAKE_CXX_COMPILER /home/builder/4798_GPL/gpl_DAP_1155B1/output/toolchains/bin/rsdk-linux-g++)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS}  -Os  -I/home/builder/4798_GPL/gpl_DAP_1155B1/output/staging/usr/include -DTESTING -DTESTING_VERSION -DSUPPORT_TELNET -DSUPPORT_RIP -DSUPPORT_IGMP -DUPDATE_BOOTLOADER -DSUPPORT_EZ_IPUPDATE -DDAP_1155B1 -DRLX  -DSUPPORT_WIFI -DEMBED" CACHE STRING "Buildroot CFLAGS" FORCE)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}  -Os  -I/home/builder/4798_GPL/gpl_DAP_1155B1/output/staging/usr/include -DTESTING -DTESTING_VERSION -DSUPPORT_TELNET -DSUPPORT_RIP -DSUPPORT_IGMP -DUPDATE_BOOTLOADER -DSUPPORT_EZ_IPUPDATE -DDAP_1155B1 -DRLX  -DSUPPORT_WIFI -DEMBED" CACHE STRING "Buildroot CXXFLAGS" FORCE)
set(LINUXDIR /home/builder/4798_GPL/gpl_DAP_1155B1/output/build/kernel)
