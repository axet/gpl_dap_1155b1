lang = {
eng : "English",
rus : "Русский",
ukr : "Українська",
tur : "Türkçe",
fra : "Français",
ara : "العربية",
fas : "فارسی",
auth : "y",
menu_ddns : "DDNS",
macfDrop : "Refuser",
ipfltSectPort : "Ports",
rmtAccessPortS : "Ports publics",
dnsServers : "Nom du serveur",
urlfDrop : "Nier",
vserversName : "Nom",
menu_statWan : "Etat du WAN",
vserversIPDst : "Private IP",
statWanType : "Type",
menu_net : "Net",
ipfltSource : "Source",
wpsMethod : "Méthode WPS",
menu_wifi_wds : "WDS",
urlfConfigTab : "Configuration",
menu_ntp : "Client NTP",
menu_routing : "Routage",
vserversIface : "Interface",
vserversTemplate : "Modèle",
menu_wifi : "Wi-Fi ",
vserversProtocol : "Protocole",
statWanStatus : "Status",
statWanUp : "haut / Actif / Operationnel",
devmodeSelect : "Mode de travail",
clientDataUnknown : "Inconnu",
addonTxPwr : "Puissance de TX",
wdsMode : "Mode WDS",
wmmEnable : "WMM",
stalstYes : "Oui",
menu_wifi_client : "Client",
wpsSectConnect : "Connexion",
basicCountry : "Pays",
menu_wifi_wmm : "WMM",
menu_igmp : "",
clientChannel : "Canal",
slLevel3 : "message d'erreurs",
wpsPin : "Code PIN",
clientScanBtn : "Rechercher",
basicChannel : "Cannal",
menu_wifi_wps : "WPS",
addonBgProt : "Protection BG",
slRemote : "A distance",
wdsModeDisable : "Désactiver",
wifiCommonOff : "Desactivee",
menu_upnp : "",
nstMask : "Masque",
button_enter : "Entrer",
hour : "heure ",
hs_password : "Mot de passe",
wanProv : "Fournisseur",
dhcpMacAddRule : "Ajouter",
wanMetric : "Métrique",
wanMac : "MAC",
mppe_40 : "MPPE 40 bit",
mppe_128 : "MPPE 128 bit",
port : "port",
view : "Theme",
day : "Jour ",
statRouteFlags : "",
dhcpModeRelay : "Rejouer",
wanMain : "Principal",
slLogTab : "Trace",
button_conf_save : "sauvegarder",
nstMtu : "MTU",
statRouteGateway : "Passerelle",
igmpVersion : "version",
dhcpMacMac : "Adress de MAC",
vserversWrongPortDst : "Port privé incorrect!",
urlfAccept : "Autoriser",
ddnsHost : "Nom de l'hôte",
add : "Ajouter",
name : "Nom",
menu_status : "Status",
wanStatus : "Statut",
wanName : "Nom",
byte : "octet ",
minute : "min",
wanNat : "NAT",
wanMtu : "MTU",
devInfoLanIp : "LAN IP",
wanVlanId : "VLAN ID",
wanQos : "QoS",
date : "Date",
yes : "oui",
mbyte : "MO",
kbyte : "KO",
nstRxTx : "Rx/Tx",
dBm : "dBm",
wanVci : "VCI (32-65535)",
wanMiscSect : "Divers",
protocol : "protocole",
menu_system : "système",
wanCreate : "Créer",
metric : "métriques",
wanEnable : "Activer",
logout : "soti de system",
value : "valeur",
wanL2Sect : "Couche physique",
devInfoSoftRev : "revision web",
devInfoName : "Modele",
menu_stat : "Statistiques du réseau",
menu_rmtAccess : "L'accès à distance",
rpcerror : "La commande a échoué",
statWanCategory : "Categorie",
nstType : "Type",
dB : "dB",
rpcok : "OK",
nstName : "Nom",
vserversPortFw : "Ports publics",
vserversPortDst : "Port privé",
vserversPortDstB : "Port privé (début)",
adding : "rejout",
netAddrEmpty : "Remplissez le champ",
numEmpty : "Remplissez le champ",
themeNormal : "Classique",
ddnsLabel : "Configuration client DDNS",
commonEnableWiFi : "Sans Fil activee",
ddnsService : "Service DDNS",
ntpTz : "Fuseau horaire",
macfLabel : "Configuration Filtre-Mac",
ddnsUserName : "Nom d'utilisateur",
ddnsWrongUsrName : "Nom d'utilisateur incorrect!",
dnsLabel : "Paramettres DNS",
addonKeepAlive : "Station garder Actif",
slConfigTab : "configuration",
destination : "destination",
slServer : "serveur  ",
devmodeRouter : "Routeur",
menu_statLanClients : "Client LAN",
menu_dsl_advanced : "Avancée",
list : "liste",
routingViaIface : "interface cible",
addonTxPream : "Préambule de TX",
addonFrag : "Seuil Frag ",
clientScanUnknown : "Réseaus sans fil inconnue",
wanLabel : "Connexions",
wdsPhyMode : "Mode WDS Phy",
securityPreAuth : "Pre--Authentification WPA2",
wpsUnconf : "Non configuré",
routingNetDest : "Reseau destination",
wpsAuth : "Authentification Réseau",
wifiMacMode : "Restriction mode par Filtre Mac",
basicClientMaxTitle : "0- non limites",
securityAuthMode : "Authentification reseau",
enable : "permis",
mppe_40_128 : "MPPE 40/128 bit",
second : "sec",
dhcpMode : "Mode",
slLocal : "Sur place",
off : "éteint",
refresh : "rafraîchir",
wpsSectInfo : "Information",
wpsConnect : "Connecter",
wdsMac : "MAC du WDS",
wanAuto : "<i>automatique</i>",
slLogging : "Nom d'utilisateur",
wanVlanPr : "La priorité VLAN",
wanMask : "Netmask",
menu_statRoute : "Table de routage",
securityWPARen : "Renouvelement WPA",
slLevel2 : "situations critiques",
slLevel4 : "Message de notification different",
wanGenSect : "Paramètres généraux",
wanKeepAlive : "Keep Alive",
selected : "Sélection",
wanDirection : "Direction",
dhcpMacHostName : "Nom de l'hôte",
no : "non",
wanServName : "Nom du service",
dhcpMacDelRule : "Supprimer",
button_deflang_save : "sauvegarder",
dhcpEnd : "Terminer IP",
comment : "commentaires",
stalstDisas : "Deconnecter",
out : "Transmis",
button_save : "changer",
button_del : "Effacer",
menu_statDhcp : "DHCP",
button_reboot : "Redemarrage",
button_clear : "Effacer",
wanRip : "Activer RIP",
wanL3PppSect : "PPP paramètres",
wifiMacModeDen : "Refuser",
change_ip_progress : "Modification de l'adresse IP",
wanOnDemand : "Dial sur demande",
wanStatusAuthenticating : "Authentification",
statDhcpExpires : "expire",
wanGwIp : "Adresse IP de passerelle",
wanPppIpExt : "L'extension d'IP DE PPP",
lng_g3_empty_sms : "Envoyer un message SMS vide",
wanPppoePassThrough : "PPPoE passer à travers",
devInfoBugs : "Support",
msec : "msec",
devInfoSummary : "Sommaire",
devInfoVendor : "Vendeur",
nstIp : "IP",
startSepAdslStatus : "Status de DSL",
nstState : "Status",
save : "enregistrer",
slServerType : "Type d'aresse du serveur",
wanScr : "Le Taux viable de Cellule (le cellules/s",
passwConfirm : "Confirmation",
numLessThanMin : "Nombre est inférieur au minimum",
addonBand : "Bande Passante ",
menu_wifi_station_list : "Liste des stations",
resetconfig : "Les parametres usine on ete reinitialisee",
menu_wifi_addon : "Paramètres supplémentaires ",
clientWEP : "Activer cryptage WEP",
dhcpMac : "DHCP statique",
ipfltDestBeg : "Destination (première)",
devInfoBuildTime : "Temps de construction",
clientKeyPSK : "Clé de cryptage PSK",
clientSectWEP : "Paramètres de cryptage WEP",
wmmSectAP : "Paramètres de Point d'Accès",
themeCompact : "navigation rapide",
clientSignal : "Niveau du signal",
securitySectWPA : "Paramettrage de Cryptage WPA",
wpsConf : "Configuré",
clientEnable : "Activer client",
urlfConfTypeExc : "Bloquer les  URLs  énumérés",
type_start_auto : "automatique",
button_connect : "connecter",
slLevel : "",
action : "action",
wanPPTPAuto : "Connecter automatiquement",
masq : "masque",
securitySectRadius : "Paramettrage RADIUS",
menu_wifi_basic : "Pramettrages de base",
clientBSSIDWrong : "BSSID est faux!",
wanLcpFails : "ne parviennent pas à définir LCP",
upnpLabel : "",
slLevel7 : "Message de debugage",
menu_system_firmware : "Mise à jour Firmware",
wanType : "Type de connexion",
hs_login : "Connectez-vous",
ntpTzMinus7 : "(GMT -7 h.) fuseau horaire des Rocheuses (Saskatchewan, Sonora)",
bad_auth : "L'authentification a échoué!",
nstStateUp : "En haut",
dhcpModeEn : "Activer",
vserversPortDstE : "Port privé (fin)",
wanSecDns : " serveur secondaire de DNS",
dmzLabel : "Paramètres de la zone démilitarisée",
slLevel6 : "Message d'information",
menu_system_log : "Trace system",
menu_firewall : "Pare-feu",
vserversPortFwB : "Ports public (debut)",
clientAuthMode : "Authentification Réseau",
clientWEPKey : "Clé de cryptage WEP",
startSepDevInfo : "Informations du périphérique",
firmwareFileNotSelected : "Fichier non sélectionnés",
wpsSectEnable : "Activer/Desactiver WPS",
securityWEP : "Activer le Cryptage WEP",
menu_dmz : "DMZ",
wanMtuPppMoreL2 : "PPP MTU ne peut pas être plus de MTU Ethernet",
dhcpExtIp : "Serveur IP DHCP Extérieures",
clientWPAEnc : "Cryptage WPA",
dhcpMain : "DHCP serveur",
addonDtim : "Période DTIM",
wanAuth : "Algorithme d'authentification",
wanNoAuth : "Sans autorisation",
ntpAddressWrong : "Nom de domaine en ligne  \"Serveur NTP\" Erronne",
dhcpCorrectReq : "Réglez le pool de serveurs DHCP pour les paramètres IP?",
clientScanFinded : "Réseaus sans fil disponibles",
wdsEncrypt : "Cryptage WDS",
ipfltPort : "Port",
basicMode : "Mode sans fil",
ipfltAction : "Action",
wifiMacAddLabel : "Ajout de Mac adresse",
netAddrNaN : "Seuls les numéros sont valables",
ipfltSourceEnd : "Source (dernière)",
securityWEPKeyID : "Cle d'Identification par defaut",
manual : "Manuel",
wpsReset : "Remise à configuré",
ddnsCantAdd : "Vous ne pouvez pas ajouter plus d'un service DDNS!",
errorOnEdit : "Une erreur s'est produite lors de l'enregistrement de données!",
menu_ping : "Ping",
wpsEnable : "Activer WPS ",
wpsEncr : "Cryptage ",
clientSectWPA : "Paramètres de cryptage WPA",
wanVendorID : "Vendor ID",
pptp_encr : "Chiffrement",
wifiMacListForDelEmpty : "Choisissez Mac Adresse pour suppression",
routingGateway : "passerelle",
ntpTzMinus10 : "(GMT -10 h.) Hawaï",
iface : "Interface",
dhcpMaskHoleRidden : "Impossible de régler le pool de serveurs DHCP pour les paramètres IP. Incorrecte masque de sous réseau.",
addonBeacon : "Période de Balise",
ipfltSourceBeg : "Source (en premier)",
ipfltExistRuleMessage : "Tel  nom de règle existe  déjà",
wanStatusPendingDisconnect : "Dans l'attente de déconnexion",
gbyte : "GO",
config : "configuration",
dhcpLease : "Duree du bail",
ntpAddressEmpty : "Liste Vide \"Serveurs NTP\"",
menu_index : "commencer",
ntpTzMinus2 : "(GMT -2 h.) Géorgie du Sud et les îles Sandwich du Sud",
ntpTzMinus11 : "(GMT -11 h.) Midway, Samoa",
ntpTz12 : "(GMT +12 h.) Petropavlovsk-Kamchatski, Fiji, Nauru, Tuvalu",
button_config_upload : "Restauration",
button_config_download : "Sauvegarde",
ntpServers : "Serveurs NTP",
waitexceed : "Temps d'attente dépassé !",
menu_control : "controle",
slType : "Type de nom d'utilisateur",
menu_system_config : "Configuration",
comment_factory_config : "Reinitialiser aux parametrage unsine",
vserversWrongPortFw : "Port public incorrect!",
button_upload : "Mise a jour",
wanPcr : "Le Taux maximum de Cellule (le cellules/s)",
dhcpNewPool : "un pool nouveau est",
wanUseVlan : "Utilisez VLAN",
lng_g3_pin_wrong : "Vérifiez si le code PIN est correct. Il doit être composé de 4-8 chiffres!",
wanErrors : "L'action est interrompue en raison des erreurs dans la page",
wanFirewall : "pare-feu",
ddnsPeriod : "Mise à jour de la période (min.)",
nstGw : "Gateway",
urlfConfTypeInc : "Bloquer toutes les URLs sauf énumérés",
ipfltWrongRange : "Mauvaise série d'adresses",
button_refresh : "rafraîchir",
clientWMode : "Mode sans fil",
basicClientMax : "Clients maximum associes",
vserversLabel : " liste des serveurs virtuels",
vserversMenu : "Les serveurs virtuels",
urlfAddrDescText : "Vous pouvez ajouter, éditer et supprimer des adresses ici",
ipfltSectIPDesc : "Pour entrer l'adresse de type IP 32 comme masque de sous réseau (champs après \"/\")",
wanUserNameEmpty : "Entrez le nom d'utilisateur",
securityKeyPSK : "Cle PSK de cryptage",
wanIp : "Adresse IP",
ip_address : "L'adresse IP",
comment_download_config : "sauvegarder parametrage courant dans un fichier",
macfDescText : "Vous pouvez ajouter, editer, effacer des adresses ICI",
addonRts : "Seuil RTS",
dhcpBegin : "Débuter IP",
wanStatusConnecting : "se connecter ",
wanConfirmMismatch : "Confirmation et mot de passe sont différents",
dhcpModeDis : "Désactiver",
status : "statut",
lng_config_general_error : "Erreur inconnue!",
wanVlanSectDescText : "Les paramètres de réseau de secteur locaux véritables",
alert_config_upload_ok : "Les nouveaux paramettres ont ete chargees avecc succes ! Pour que la nouvelle configuration prenne place merci de redenarrer le system",
lng_g3_newpin_empty : "Vider le code PIN n'est pas autorisé. Cochez la case \"Désactiver PIN vérifier\" pour vérifier le code PIN désactiver.",
none : "aucun",
ipfltLabel : "IP-filtres de configuration",
ipfltSectPortDesc : "Vous pouvez spécifier soit  une plage de ports ou d'un port, et plusieurs",
urlfEditSect : "Edition Adresse URL",
urlfConfType : "URL de type filtre",
rebooting : "Redemarrage",
menu_options_deflang : "Langue par défaut",
save_and_reboot_success : "Sauvegarde avec succee de la configuration, Merci d'attendre le redemarage",
menu_dns : "DNS \" Serveur de nom de domaine\"",
lang : "language ",
staListForDelEmpty : "Choisissez Mac Adresse pour suppression",
ipfltIPRange : "Les plages d' adresses IP ",
menu_wifi_mac : "Filtre-MAC",
menu_macfilter : "Filtre-MAC",
macfAccept : "Autoriser",
wifiMacAddrTab : "Adresses MAC",
ipfltEditSect : "L'édition de règles de filtrage IP",
wanPrimDns : "Serveur DNS primaire",
wanPassword : " mot de passe",
urlfAddress : "Adresse URL",
menuIPFilter : "IP-filtres",
wifiMacViewLabel : "Liste d'adresse MAC",
macfEditSect : "Edition des adresses MAC",
config_aplly_ok : "La nouvelle configuration est applique avec succes",
passwPassword : "Mot de passe",
button_export : "Exporter",
wanConfirm : "Confirmation de mot de passe",
routingNetDestMask : "mask reseau destination",
wanDescText : "Vous pouvez ajouter, éditer et supprimer les connexions ici",
passwConfirmCirill : "Mot de passe ou la confirmation de l'alphabet cyrillique!",
vserversCreateWan : "S'il vous plaît, créez-WAN connexion dans un premier temps",
wanPPTPIp : "IP reçus",
wifiWEPKeyWrongHEX : "Clé de chiffrement doit contenir des numeros Hexadecimaux",
type_start : "commencer",
no_encrypt : "Aucun chiffrer",
wifiMacModeAlw : "Autoriser",
wifiMacModeDis : "desactivee",
passwErrors : "Plusieurs champs continnent d'erreurs",
ntpTzMinus12 : "(GMT -12 h.) Kwajalein, Eniwetok",
slLevel1 : "alertes",
securityWEPKeyHEX : "Cle de cryptage WEP en HEX",
file_upload_error : "Erreur lors du télécharger de fichier ",
wanPPTPExtraOptions : "Options supplémentaires",
passwDesc : "Mot de passe système mise en place",
firmwareUploading : "Chargement, patientez s'il vous plaît …",
slLevel0 : "systeme non efficace",
wanGenSectDescText : " type de connexion et  paramètres communs",
devInfoVersion : "Version du micrologiciel",
dnsDefRoute : "Chemin par defaut",
slLevel5 : "notifications importantes",
wanPPTPSect : "paramètres PPTP/L2TP",
wanVpi : "VPI (0-255)",
ntpTz9 : "(GMT +9 h.) Yakutsk, East Timor, Japan, North Korea",
ntpTz8 : "(GMT +8 h.) Irkutsk, Australia, Brunei, Hong Kong",
ntpTzMinus1 : "(GMT -1 h.)  Groenland,  Portugal, Cap-Vert",
confirm_reboot : "Le system va etre redemarer ! Toute modification non sauvegardee sera perdue ! Continuer",
wanMbs : "La Taille maximum d'Eclatement (les cellules)",
wpsStatus : "Statut de WPS",
securityRadiusKey : "Cle de cryptage WPA",
statLanClientsFlags : "parametrage graphique",
wanStatusUnconfigured : "pas configuré",
menu_wan : "Connexions",
firmwareUpload : "Sélectionnez le fichier à jour",
slBoth : "Sur place et a distance",
dnsAddressWrong : "Liste d'adresse IP en ligne du serveur DNS erronnee",
securityWEPKey : "Cle de cryptage WEP",
wmmErrorAifsn : "AIFSN hors domaine (1~15)!",
securitySectWEP : "Paramettrage de cryptage WEP",
ipfltDestEnd : "Destination (dernière)",
passwDescText : "Le mot de passe du système et le mot de passe pour l'interface de Web changeront en même temps.",
wanPPTPSectDescText : "PPTP et L2TP sont des méthodes de mise en œuvre des réseaux privés virtuels.",
uploadFileNotSelected : "Fichier non sélectionnés",
netAddrOutOfScope : "Hors de portée",
passwConfirmMismatch : "La confirmation et le mot de passe sont different",
wanNoPhyIfaceAvail : "Pas d'interfaces disponibles pour la connexion d'un type spécifique",
dhcpMacHasEmpty : "Pas tous les champs obligatoires du DHCP statique sont remplis. L'adresse IP et adresse MAC champs sont obligatoires.",
wanPppDebug : "debug PPP",
vserversEditSect : "Les paramètres du serveur virtuel",
reset_and_reboot_progress : "Configuration resetter",
wmmSectSta : "Paramètres du Station",
passwLogin : "login",
numMoreThanMax : "Nombre est plus que le maximum",
devmodeSectMode : "Parametres de l'appareil en mode travail",
devInfoBoardId : "Conseil ID",
fwupdate_progress : "Update du pilote",
ntpTz6 : "(GMT +6 h.) Dhaka, Chittagong, Novossibirsk, Khulna",
button_factory : "Usine",
clientSecureOpen : "Net ouvert",
lng_g3_invalid_number : "Numero de telephone invalide",
wanPasswordEmpty : "Entrez le mot de passe",
filter : "Filtre de données",
menu_wifi_common : "Paramettrages Commun",
ntpTzMinus6 : "(GMT -6 heures) Zone Nord Amérique Time centrale (Costa Rica, Equateur)",
ntpTz2 : "(GMT +2 h.) Le Caire, Istanbul, Johannesburg, Alexandrie",
ntpTz3 : "(GMT +3 heures) à Moscou, Bagdad, Khartoum, Saint-Pétersbourg",
ntpTz5 : "(GMT +5 h.) Ekaterinbourg, Karachi, Lahore, Faisalabad, Rawalpindi",
ntpTz0 : "(GMT) Temps universel coordonné (Dublin, Londres, Lisbonne)",
DNS : "Les noms des serveurs",
wanPppStaticIp : "Adresse IP",
kbps : "Kpbs ",
passw : "Mot de passe",
all_ : "",
ntpTzMinus4 : "(GMT -4 heures) Heure de l'Atlantique (Labrador, de la Dominique)",
ddnsCreateWan : "S'il vous plaît, créez-WAN connexion dans un premier temps",
wanPPTPSName : "Nom du service",
nstMac : "MAC",
menu_system_passw : "mot de passe administrateur",
ddnsWrongHost : "Nom d'hôte  ou adresse IP incorrect!",
vserversPortFwE : "Ports publics (fin)",
clientSectScanData : "Liste des réseaux sans fil",
wanIfaceL3 : "Interface",
securityWPAEnc : "Cryptage WPA",
wanEncap : "Mode d'encapsulation",
macfAddrTab : "Adresses MAC",
ipfltSectIP : "Adresses IP",
wanLcpInterval : "intervalle de LCP (s)",
wanVlanSect : "paramètres VLAN",
file_upload_progress : "téléchargement de fichiers",
ddnsEditSect : "Édition client DDNS",
wanIfaceL2 : "Interface physique",
wanL3IpSectDescText : "Paramètres de protocole Internet",
wanL2SectDescText : "sélection de l'interface physique et tunning",
ch_passw_warning : "Pour le moment le mot de passe par défaut est spécifiée. Pour des raisons de sécurité, il est recommandé de changer le mot de passe.",
reboot_is_complete : "equipement pret",
wanPvcBusy : "Le PVC est occupé par une autre connexion, s'il vous plaît choisir un autre VPI et/ou VCI",
ntpTzMinus3 : "(GMT -3 h.) l'Antarctique, le Brésil, la Guyane française",
urlfAddrTab : "URL-adresse",
dhcpMacIp : "Adresse IP",
statWanDown : "Bas / non operationnel",
mtu : "taille MTU",
wanIdleTimeout : "temps d'inactivité maximal (sec)",
wanL3PppSectDescText : "",
config_aplly_ok_reboot : "La nouvelle configuration a été appliquée avec succes.Pour appliquer les modifications, il est nécessaire de redémarrer l'appareil. Redémarrer maintenant?",
lng_config_other_version : "Ces paramètres sont destinés à une autre version du firmware.\n  Vous pouvez les appliquer à vos propres risques!",
comment_upload_config : "Chargement de la derniere sauvegarde des paramettres",
nstStateDown : "En bas",
lng_config_upload_failed : "Impossible d'appliquer les nouveaux parametres! Un fichier a un format incorrect ou endommagé",
dhcpCorrectImpos : "Impossible de régler le pool de serveurs DHCP pour les paramètres IP",
lng_config_unknown_device : "Vous ne trouvez pas l'information dans la version du micrologiciel configuration.\nYour transféré peut appliquer les nouveaux réglages à vos propres risques!",
clientWEPKeyID : "Clé par défaut ID",
wanDnsFromDhcp : "Obtenir adresses des serveurs DNS automatiquement",
lng_config_unknown_target_device : "Impossible de détecter la version du firmware de votre appareil. \ nVous pouvez appliquer les nouveaux réglages à vos propres risques!",
conf_change_warning : "Configuration du terminal a ete changee",
routingEditSect : "Edition du chemin",
menu_urlfilter : "URL-filtre",
lng_config_wrong_device : "Ces paramètres sont destinés à un autre appareil et n'est pas compatible avec votre appareil!",
rpcneedreboot : "Commande exécutée avec succès. \ nVeuillez redémarrer dispositif.",
confirm_reset_config : "",
comment_save_current_config : "sauvegarder paremetrage courant",
wdsEncryptKey : "Clé de cryptage ",
menu_devmode : "Mode Appareil",
vserversDescText : "Vous pouvez ajouter, éditer et supprimer des serveurs virtuels ici",
stalstAssoc : "Associee",
ntpTzMinus8 : "(GMT -8 h.) fuseau horaire du Pacifique (Mexique, Nevada)",
wanIgmp : "Activer multidiffusion IGMP",
wifiPSKKeyWrong : "WPA Cles Pre-partagee devrais etre situee entre 8 et 63 Caracteres ASCII",
devmodeAP : "Point dacces",
lng_g3_pin_mismatch : "Nouveau code PIN et le code de confirmation PIN ne correspondent pas.",
button_save_reboot : "Enregistrer et redemarer",
passwPasswordEmpty : "Entrez le mot de passe",
ntpTzMinus9 : "(GMT -9 heures)  l'Alaska",
save_config_success : "Sauvegarde avec succee de la configuration",
ddnsWrongPass : "Mot de passe incorrect!",
wanL3IpSect : "paramètres IP",
ntpTz4 : "(GMT +4 h.) Bakou, Samara, Tbilissi, Erevan",
dnsAddressEmpty : "Liste d'adresse IP du serveur DNS Vide",
wifiWEPKeyWrong : "Clé de chiffrement doit contenir 5 ou 13 caractères. Clé vide n'est pas autorisée",
ntpTz11 : "(GMT +11 h.) Magadan, New Caledonia, Solomon Islands, Vanuatu",
urlfConfEnable : "Activer / Désactiver le filtre URL",
wanExtraOptions : "Options supplémentaires",
wanStatusConnected : "connecté",
wanStatusDisconnected : "Déconnecté",
disconnect : "deconnecter",
button_disconnect : "disconnecter",
wanStatusDisconnecting : "Déconnexion",
numNaN : "Seuls les numéros sont valables",
ntpTz1 : "(GMT +1 h.): Albanie, Autriche, Cité du Vatican",
menu_wifi_security : "Paramettrage de securite",
hwaddr : "L'adresse MAC",
wanL2Param : "Interface physique",
wanUserName : "Nom d'utilisateur PPP",
ddnsUserPass : "Mot de passe de l' utilisateur",
system_in_reboot : "Le système redémarre, patientez s'il vous plaît",
wanGwIf : "passerelle par défaut",
routingDescText : "Vous pouvez ajouter, editer, effacer un routreur ICI",
ntpTzMinus5 : "(GMT -5 heures) Zone Nord Amérique heure de l'Est (Ontario, de l'Équateur)",
routingLabel : "Configuration du routage",
ntpTz10 : "(GMT +10 h.) Guam, Vladivostok, Khabarovsk Krai",
wifiMacModeTab : "Filtre Mode",
urlfAddrLabel : "URL adresses liste",
rpcstatus : "Le code de retour est",
needreset : "Il est recommandé de rétablir les parametres d'usine.",
source : "source",
wanDhcp : "Obtenir une adresse IP automatiquement",
ntpTz7 : "(GMT +7 h.) Krasnoyarsk, Cambodia, Indonesia",
rmtAccessLabel : "Configuration de l'accès à distance pour l'interface web",
urlfConfLabel : "URL de configuration du filtre",
basicHideAP : "Cacher le point d'acces",
rmtAccessEditSect : "Édition de règle d'accès à distance",
wifiWEPKeyWrongHEXSize : "Clé de chiffrement doit contenir 5 ou 13 numeraux Hexadecimaux. Clé vide n'est pas autorisée",
wanUsePppStaticIp : "Utilisez l'adresse IP statique",
igmpLabel : "Parametrage du protocol de gestion des groupe internet",
ipfltWrongPortOrRange : "Mauvais port ou plage de ports"
};
