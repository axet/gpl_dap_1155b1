/* nlmon.h */
#ifndef _NLMON_H
#define _NLMON_H
    
typedef struct nlmonIoctParms {
	int offset;
	int result;
} NLMON_IOCTL_PARMS;

#define NLMON_DRV_MAJOR 202
#define NLMON_DRV_NAME "nlmon"

#define NLMON_IOCTL_MAGIC 'B'

#define NLMON_IOCTL_SET_MONITOR_FD _IOWR (NLMON_IOCTL_MAGIC, 1, NLMON_IOCTL_PARMS)

#define NLMON_IOCTL_WAKEUP_MONITOR_TASK _IOWR (NLMON_IOCTL_MAGIC, 2, NLMON_IOCTL_PARMS)

#endif /* _NLMON_H */
