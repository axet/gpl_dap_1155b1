/* netlinkmon.h */
#ifndef _NETLINKMON_H
#define _NETLINKMON_H

#define NETLINK_MONITOR 26

/* Message types exchanged using NETLINK_MONITOR */
#ifndef MSG_NETLINK_WAKEUP_MONITOR_TASK
#define MSG_NETLINK_WAKEUP_MONITOR_TASK 0X1000
#endif

#ifndef MSG_NETLINK_LINK_STATUS_CHANGED
#define MSG_NETLINK_LINK_STATUS_CHANGED 0X2000
#endif

struct nlmon_msg_data {
	int phy;
	int status;
} __attribute__((packed));

extern void kerSysSendtoNlMonTask(int msgType, char *msgData, int msgDataLen);

#endif /*_NETLINKMON_H */
