/* nlmon.c */
#include <asm/uaccess.h>
#include <asm/delay.h>
#include <linux/module.h>
#include <linux/netlink.h>
#include <linux/skbuff.h>
#include <net/sock.h>
#include <net/nlmon/netlinkmon.h>
#include <net/nlmon/nlmon.h>

#define MAX_PAYLOAD_LEN 64

static int nlmon_major = 0;

static struct sock * g_monitor_nl_sk;
static int g_monitor_nl_pid = 0;
static void kerSysInitNlMonSocket(void);
static void kerSysCleanupNlMonSocket(void);

static int nlmon_ioctl(struct inode * inode, struct file * flip, unsigned int command, unsigned long arg);

static struct file_operations nlmon_fops =
{
	ioctl: nlmon_ioctl,
};

static void kerSysRecvFrmNlMonTask(struct sock * sk, int len) {
	struct sk_buff * skb = NULL;
	skb = skb_dequeue(&sk->sk_receive_queue);
	if(skb != NULL) {
		printk(KERN_WARNING "unexpected skb received at %s \n", __FUNCTION__);
		kfree_skb(skb);
	}
	return;
}	

void kerSysInitNlMonSocket(void) {
     g_monitor_nl_sk = netlink_kernel_create(&init_net, NETLINK_MONITOR, 0, kerSysRecvFrmNlMonTask, NULL, THIS_MODULE);
	
	if(!g_monitor_nl_sk) {
		printk(KERN_ERR "failed to create a netlink socket for monitor\n");
		return;
	}
}

void kerSysCleanupNlMonSocket(void) {
	g_monitor_nl_pid = 0;
	sock_release(g_monitor_nl_sk->sk_socket);
}

void kerSysSendtoNlMonTask(int msgType, char * msgData, int msgDataLen){
	struct sk_buff * skb =  NULL;
	struct nlmsghdr * nl_msgHdr = NULL;
	int ret;
	unsigned int payloadLen = sizeof(struct nlmsghdr);
	/* printk("status = %i, phy = %i\n", ((struct nlmon_msg_data *)msgData)->status, ((struct nlmon_msg_data *)msgData)->phy); */
	/* printk("Send to netlink socket\n"); */
	if(!g_monitor_nl_pid) {
		printk (KERN_INFO "message received before monitor task is initialized %s \n", __FUNCTION__);
		return;
	} 
	
	if(msgData && (msgDataLen > MAX_PAYLOAD_LEN)) {
		printk (KERN_ERR "invalid message len in %s", __FUNCTION__);
		return;
	} 

	payloadLen += msgDataLen;
	payloadLen = NLMSG_SPACE(payloadLen);
	
	if(in_atomic()) {
		skb = alloc_skb(payloadLen, GFP_ATOMIC);
	} else {
		skb = alloc_skb(payloadLen, GFP_KERNEL);
	}

	if(!skb) {
		printk (KERN_ERR "failed to alloc skb in %s", __FUNCTION__);
		return;
	}
	
	/* nl_msgHdr = (struct nlmsghdr*) skb->data; */
	/* nl_msgHdr->nlmsg_type = msgType; */
	/* nl_msgHdr->nlmsg_pid = 0; /\* from kernel *\/ */
	/* nl_msgHdr->nlmsg_len = payloadLen; */
	/* nl_msgHdr->nlmsg_flags = 0; */

	/* printk("nl_msgHdr->nlmsg_type = 0x%x\n", nl_msgHdr->nlmsg_type); */
	/* printk("nl_msgHdr->nlmsg_pid = 0x%x\n", nl_msgHdr->nlmsg_pid); */
	/* printk("nl_msgHdr->nlmsg_len = 0x%x\n", nl_msgHdr->nlmsg_len); */
	/* printk("nl_msgHdr->nlmsg_flags = 0x%x\n", nl_msgHdr->nlmsg_flags); */
	/* if(msgData) { */
	/* 	memcpy(NLMSG_DATA(nl_msgHdr), msgData, msgDataLen); */
	/* } */
	      
	/* NETLINK_CB(skb).pid = 0; /\* from kernel *\/ */
	
	/* skb->len = payloadLen; */ 

	nl_msgHdr = nlmsg_put(skb, 0, 0, msgType, payloadLen, 0);
	if(!nl_msgHdr){
	     printk("Error!!! Can not creating msg header\n");
	     return;
	}
	if(msgData) {
		memcpy(NLMSG_DATA(nl_msgHdr), msgData, msgDataLen);
	}
	
	/* { */
	/*      int i; */
	/*      char *buf = NULL; */

	/*      buf = (char *)nl_msgHdr; */
	/*      for(i = 0; i < sizeof(struct nlmsghdr)+msgDataLen; i++){ */
	/* 	  printk(" 0x%x", buf[i]); */
	/*      } */
	/*      printk("\n"); */
	/* } */
	/* printk("Netlink packet is out\n"); */
	ret = netlink_unicast(g_monitor_nl_sk, skb, g_monitor_nl_pid, MSG_DONTWAIT);
	//rtk_nlsendmsg(g_monitor_nl_pid, g_monitor_nl_sk, msgDataLen, msgData);
	/* printk("payload len = %i, ret = %i\n", payloadLen, ret); */
	return;
}

static int nlmon_ioctl(struct inode * inode, struct file * flip, unsigned int command, unsigned long arg) {
	int ret = 0;
	NLMON_IOCTL_PARMS ctrlParms;
	
	switch(command) {
	case NLMON_IOCTL_SET_MONITOR_FD:
		if (copy_from_user ((void*) &ctrlParms, (void*) arg, sizeof(ctrlParms)) == 0) {		
			g_monitor_nl_pid =  ctrlParms.offset;
			printk(KERN_INFO "monitor task is initialized pid= %d \n", g_monitor_nl_pid);
		}
		break;
				
	case NLMON_IOCTL_WAKEUP_MONITOR_TASK:
		kerSysSendtoNlMonTask(MSG_NETLINK_WAKEUP_MONITOR_TASK, NULL, 0);
		break;
		
	default:
		ret = -EINVAL;
		ctrlParms.result = 0;
		printk(KERN_ERR "nlmon_ioctl: invalid command %x, cmd %d .\n", command, _IOC_NR(command));
		break;
						
	}
	
    return ret;	
}

static int __init nlmon_init(void) {
	int ret;
	ret = register_chrdev(NLMON_DRV_MAJOR, NLMON_DRV_NAME, &nlmon_fops);
	if (ret < 0) {
		printk(KERN_ERR "nlmon_init (major %d): fail to register device.\n", NLMON_DRV_MAJOR);
	} else {
		printk(KERN_INFO "nlmon: nlmon_init entry\n");
		nlmon_major = NLMON_DRV_MAJOR;
		kerSysInitNlMonSocket();
	}
	return ret;
}

void __exit nlmon_cleanup(void) {
	printk(KERN_INFO "nlmon_cleanup()\n");
	if (nlmon_major != -1) {
		kerSysCleanupNlMonSocket();
		unregister_chrdev(nlmon_major, NLMON_DRV_NAME);
	}
}

module_init(nlmon_init);
module_exit(nlmon_cleanup);
EXPORT_SYMBOL(kerSysSendtoNlMonTask);
