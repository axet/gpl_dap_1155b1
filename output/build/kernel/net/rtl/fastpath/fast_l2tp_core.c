#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/kernel.h>
#include <linux/kernel_stat.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
//#include <linux/brlock.h>
#include <linux/net.h>
#include <linux/socket.h>

#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/string.h>
#include <net/ip.h>
#include <net/protocol.h>
#include <net/route.h>
#include <net/sock.h>
#include <net/arp.h>
#include <net/raw.h>
#include <net/checksum.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/netlink.h>

#include <linux/in.h>
#include <linux/udp.h>
#include <linux/if_tunnel.h>
#include <linux/if_ether.h>
#include <net/rtl/rtl_types.h>
#include <net/rtl/fastpath/fastpath_core.h>

#ifdef FAST_L2TP
#include "fast_l2tp_core.h"
#endif

/*common*/
#include <net/rtl/rtl865x_netif.h>
#include <net/rtl/rtl_nic.h>


#if defined(CONFIG_NET_SCHED)
extern int gQosEnabled;
#endif

#if defined(FAST_L2TP)

static struct l2tp_info l2tpInfo={NULL};
uint32 state;
#define L2TP_INITED 0
#define L2TP_REPLY_RECEIVED 1
#define L2TP_CONNECTED 2

#if 1 // sync from voip customer for multiple ppp
int is_l2tp_device(char *ppp_device)
{
	if (l2tpInfo.ppp0_dev && !strcmp(rtl_get_ppp_dev_name(l2tpInfo.ppp0_dev), ppp_device))
	{
		//printk("%s: l2tp device = %s\n", __FUNCTION__, ppp_device);
		return 1;
	}
	return 0;
}

void set_l2tp_device(char *ppp_device)
{
	if (fast_l2tp_fw)
	{
		//printk("%s: l2tp device = %s\n", __FUNCTION__, ppp_device);
		l2tpInfo.ppp0_dev = (void *)rtl_get_dev_by_name(ppp_device);
	}
}
#endif

void event_ppp_dev_down(const char *name)
{
	if(l2tpInfo.valid)
	{
		if(l2tpInfo.ppp0_dev && strcmp(rtl_get_ppp_dev_name(l2tpInfo.ppp0_dev),name) == 0)
		{
			memset(&l2tpInfo, 0, sizeof(l2tpInfo));
			state = 0;
		}
	}
}

#ifdef DOS_FILTER
extern void filter_addconnect(__u32 ipaddr);
#endif

void l2tp_tx_id(void *skb)
{
        __u16 *p_id;
        __u16 MessageType;
	__u16 *ppp_protocol;
	struct iphdr *iph;
	struct udphdr *hdr;
	struct l2tp_header *l2tp_ptr;
	unsigned char *data;


	data = rtl_get_skb_data(skb);
	iph = rtl_ip_hdr(skb);
       hdr = (struct udphdr *)((u_int32_t *)iph + iph->ihl);
       l2tp_ptr = (struct l2tp_header *)((u_int8_t *)hdr+8);
       if(hdr->source==1701 && hdr->dest==1701 && (l2tp_ptr->ver & 0x8000)==control_message)
	{
	  	if((l2tp_ptr->ver & 0x4000)==0)
	    		p_id=(__u16 *)((u_int8_t *)(&l2tp_ptr->ver)+2);
	  	else
	    		p_id=&l2tp_ptr->tid;

          	MessageType=*(p_id+7);
          	if(MessageType==stop_control && l2tpInfo.tid==*p_id)
	  	{
	  		memset(&l2tpInfo, '\0', sizeof(l2tpInfo));
			DEBUGP("FAST-L2TP, stop control, dev->name=%s\n", rtl_get_skb_dev_name(skb));
			#if defined(CONFIG_RTL_819X)
			rtl865x_setNetifType(RTL_PS_PPP0_DEV_NAME, IF_PPPOE);
			#endif
			state = L2TP_INITED;
	  	}

          	if(MessageType==call_reply)
	  	{
	  		/*hyking:
	  		*bug fix:l2tp wantype,l2tp pass thru
	  		*/
	  		/*log mac information when from wan interface, not from br0 or to br0*/
#if defined(CONFIG_RTL_PUBLIC_SSID)
			if(state < L2TP_REPLY_RECEIVED && memcmp(rtl_get_skb_dev_name(skb),RTL_GW_WAN_DEVICE_NAME,3) == 0)
#else
	  		if(state < L2TP_REPLY_RECEIVED && memcmp(rtl_get_skb_dev_name(skb),RTL_PS_WAN0_DEV_NAME,4) == 0)
#endif
	  		{
				l2tpInfo.wan_dev = (void*)rtl_get_skb_dev(skb);
		      		memcpy(&l2tpInfo.mac_header[0], data-ETH_HLEN+ETH_ALEN, ETH_ALEN); // da of tx pkt
				memcpy(&l2tpInfo.mac_header[ETH_ALEN], data-ETH_HLEN, ETH_ALEN); // sa of tx pkt
				memcpy(&l2tpInfo.mac_header[ETH_ALEN*2], data-2, 2); // type
				DEBUGP("FAST-L2TP: call-reply, dev->name=%s\n", rtl_get_skb_dev_name(skb));
				state = L2TP_REPLY_RECEIVED;
	  		}
	    	}

          	if(MessageType==connect_control && *(p_id+1)!= 0)
	  	{
			/*log ip/session id information when from protocal stack, not from br0*/
			if(state < L2TP_CONNECTED)
			{
		    		l2tpInfo.tid=*p_id;
		    		l2tpInfo.cid=*(p_id+1);
	      	    		l2tpInfo.saddr = iph->saddr;
	      	    		l2tpInfo.daddr = iph->daddr;
#ifdef DOS_FILTER
				 filter_addconnect(l2tpInfo.daddr);
#endif
				l2tpInfo.valid = 1;
				#if defined(CONFIG_RTL_819X)
				rtl865x_setNetifType(RTL_PS_PPP0_DEV_NAME, IF_L2TP);
				#endif
				DEBUGP("FAST-L2TP: connected\n");
				state = L2TP_CONNECTED;
			}
	    	}
	  }
	//hyking: if l2tp termination happen, reset l2tpInfo & state...
	  else if (hdr->source==1701 && hdr->dest==1701)
	  {
	  	//data message
		//length bit is set?
	  	if((l2tp_ptr->ver & 0x4000)==0)
	    		p_id=(__u16 *)((u_int8_t *)(&l2tp_ptr->ver)+2);
	  	else
	    		p_id=&l2tp_ptr->tid;

		ppp_protocol = p_id+3;
#ifdef DOS_FILTER
		if(filter_checkConnect(l2tpInfo.daddr))
			filter_addconnect(l2tpInfo.daddr);
#endif

		//link control protocol
		if(*ppp_protocol == 0xc021)
		{
			//termination ack
			if( *((u_int8_t *)(((u_int8_t *)ppp_protocol) + 2)) == 0x06)
			{
				if(l2tpInfo.tid == (*p_id) && l2tpInfo.cid == (*(p_id+1)))
				{
					memset(&l2tpInfo, 0, sizeof(l2tpInfo));
					state = 0;
				}
			}
		}
	  }
	return;
}

void fast_l2tp_rx(void *skb)
{
	struct iphdr *iph=rtl_ip_hdr(skb);
	struct udphdr *hdr= (struct udphdr *)(((unsigned char *)iph) + iph->ihl*4);
	int rightShift = 0;
	unsigned char *data;

	data = rtl_get_skb_data(skb);

	if(rtl_get_skb_len(skb) < 40)
		return;

	if (hdr->source==1701 && hdr->dest==1701) {
		if (l2tpInfo.wan_dev == NULL)
			l2tp_tx_id(skb);

		/* skb->data[28] is L2TP Flags and version.
		 * Control flags indicating data/control packet and presence of length,
		 * sequence, offset fields.
		 * Original length of header is 6.
		 * Bit 7 is length flag. If Bit 7 is on then the length of header will extend 2 bytes
		 * Bit 2 is offset flag. If Bit 2 is on then the length of header will extend 2 bytes
		 */
		if(data[28] & 0x40)
			rightShift = 2;

		if(data[28] & 0x02) // offset option is on
			rightShift += 2;

		if (l2tpInfo.tid!=0 && l2tpInfo.cid!=0 && (data[36+rightShift]==0 && data[37+rightShift]==0x21) &&
			l2tpInfo.wan_dev && l2tpInfo.saddr==iph->daddr
		#if 0 /// fixme: should check tunnel id & session id
			&& l2tpInfo.daddr== iph->saddr
		#endif				
				) {
#if 0
	      		memcpy(&l2tpInfo.mac_header[0], skb->data-ETH_HLEN+ETH_ALEN, ETH_ALEN); // da of tx pkt
			memcpy(&l2tpInfo.mac_header[ETH_ALEN], skb->data-ETH_HLEN, ETH_ALEN); // sa of tx pkt
			memcpy(&l2tpInfo.mac_header[ETH_ALEN*2], skb->data-2, 2); // type
#endif
			if (l2tpInfo.ppp0_dev == NULL)
			{
				l2tpInfo.ppp0_dev = (void*)rtl_get_dev_by_name(RTL_PS_PPP0_DEV_NAME);
			}

			if (l2tpInfo.ppp0_dev) {
				if (((struct iphdr *)(&data[38+rightShift]))->protocol == IPPROTO_TCP ||
					((struct iphdr *)(&data[38+rightShift]))->protocol == IPPROTO_UDP) {

					rtl_set_skb_dev(skb, l2tpInfo.ppp0_dev);
					skb_pull(skb, 38+rightShift);
					rtl_skb_reset_network_header(skb);
					rtl_skb_reset_transport_header(skb);
					//skb->transport_header=skb->network_header=skb->data;
					#if 1 /* update number of received l2tp packetes into ppp0 statistic */
					{
						void *ppp;
						if(l2tpInfo.ppp0_dev && rtl_get_ppp_dev_priv(l2tpInfo.ppp0_dev)){
							ppp = rtl_get_ppp_dev_priv(l2tpInfo.ppp0_dev);
							rtl_inc_ppp_stats(ppp, 0, rtl_get_skb_len(skb));
							//get_ppp_stats(ppp)->rx_packets++;
							//get_ppp_stats(ppp)->rx_bytes += rtl_get_skb_len(skb);
						}
					}
				#endif
					//skb_reset_network_header(skb);
					//skb_reset_transport_header(skb);
					DEBUGP("FAST-L2TP: delete l2tp header!\n");
				}
			}
		}
	  }
}

unsigned long get_fast_l2tp_lastxmit(void)
{
	if(l2tpInfo.valid == 1)
		return l2tpInfo.last_xmit;
	return 0;
}

// return 0: not to do fast l2tp to wan
// return 1: to do fast l2tp to wan
int check_for_fast_l2tp_to_wan(void *skb)
{
	struct iphdr *iph;

	iph = rtl_ip_hdr(skb);

	if(iph->protocol==IPPROTO_ICMP)
		return 0;

	//if (ip_hdr(skb)->frag_off & htons(IP_MF|IP_OFFSET))
		//return 0;

	// Patch for l2tp dial on-demand: pkts which will trigger l2tp dialing up should not do fast l2tp
	// dial on-demand initial ip: 10.64.64.*
	if((rtl_ip_hdr(skb)->saddr & 0xffffff00) == 0xa404000)
		return 0;

	return 1;
}

int fast_l2tp_to_wan(void *skb)
{
	int	header_len;
	struct iphdr *iph,*iph_new, iph_newone, *iph_tmp;
	struct l2tp_ext_hdr	*l2tph, l2tphone;
	unsigned char tos;
	unsigned short frag_off;

	if(!fast_l2tp_fw || l2tpInfo.tid==0 || l2tpInfo.cid==0 || !l2tpInfo.wan_dev)
		return 0;

	if(l2tpInfo.valid != 1)
		return 0;

	iph = rtl_ip_hdr(skb);
	__u16 old_len = ntohs(iph->tot_len);
	header_len = ETH_HLEN + sizeof(struct iphdr) +18 ;
	if (rtl_skb_headroom(skb) < header_len || rtl_skb_cloned(skb) || rtl_skb_shared(skb))
	{
		void *new_skb = (void*)skb_realloc_headroom(skb, header_len);
		if (!new_skb) {
			printk("%s: skb_realloc_headroom failed!\n", __FUNCTION__);
			return 0;
		}
		dev_kfree_skb(skb);
		skb = new_skb;
	}
	tos = iph->tos;
	frag_off = iph->frag_off;

	// build mac header
	memcpy(skb_push(skb, header_len), l2tpInfo.mac_header, ETH_HLEN);

	// build ip header
	iph_new = &iph_newone;
	iph_new->version	=	4;
	iph_new->ihl		=	sizeof(struct iphdr) >> 2;
	//iph_new->frag_off	=	frag_off;
	iph_new->frag_off	=	0x4000;
	iph_new->protocol	=	IPPROTO_UDP;
	iph_new->tos		=	tos;
    	iph_new->daddr	=	l2tpInfo.daddr;
    	iph_new->saddr	=	l2tpInfo.saddr;
    	iph_new->ttl 		=	IPDEFTTL;
	rtl_set_skb_ip_summed(skb, CHECKSUM_NONE);
    	iph_new->tot_len	=	htons(rtl_get_skb_len(skb) - ETH_HLEN);
    	iph_new->id		=	0;

    	iph_new->check	=	0;
    	iph_new->check	=	ip_fast_csum((unsigned char *)iph_new, iph_new->ihl);
    	memcpy(rtl_get_skb_data(skb) + ETH_HLEN, &iph_newone, sizeof(iph_newone));
    	l2tph = &l2tphone;
    	l2tph->source	=1701;
    	l2tph->dest	=1701;
	l2tph->len	=old_len+18;
	l2tph->checksum=0;
	l2tph->type	=0x0002;
	l2tph->tid	=l2tpInfo.tid;
	l2tph->cid	=l2tpInfo.cid;
	l2tph->addr_control= 0xff03;
	l2tph->protocol	=0x0021;
    	memcpy(rtl_get_skb_data(skb)+ETH_HLEN+sizeof(struct iphdr), &l2tphone, sizeof(struct l2tp_ext_hdr));

	rtl_set_skb_dev(skb, l2tpInfo.wan_dev);

	DEBUGP("FAST-L2TP: fw to WAN!\n");

	iph_tmp = rtl_get_skb_data(skb)+ETH_HLEN+sizeof(struct iphdr)+sizeof(l2tphone);

 	extern int timeoutCheck_skipp_pkt(struct iphdr *iph);
	if(timeoutCheck_skipp_pkt(iph_tmp)!=1)
		l2tpInfo.last_xmit = jiffies;


	#if 1 /* update number of transmitted l2tp packetes into ppp0 statistic */
	{
		extern struct net_device_stats *get_ppp_stats(void *ppp);
		void *ppp;
		if(l2tpInfo.ppp0_dev && rtl_get_ppp_dev_priv(l2tpInfo.ppp0_dev)){
			ppp = rtl_get_ppp_dev_priv(l2tpInfo.ppp0_dev);
			rtl_inc_ppp_stats(ppp, 1, rtl_get_skb_len(skb));
			//get_ppp_stats(ppp)->tx_packets++;
			//get_ppp_stats(ppp)->tx_bytes += rtl_get_skb_len(skb);
		}
	}
#endif

#if defined(CONFIG_NET_SCHED)
	if (gQosEnabled)
	{
		// call dev_queue_xmit() instead of hard_start_xmit(), because I want the packets be sent through Traffic Control module
		dev_queue_xmit(skb);
		return 1;
	}
	else
#endif
		//dev_hard_start_xmit(skb,skb->dev);
		//dev_queue_xmit(skb);
		rtl_call_skb_ndo_start_xmit(skb);
	return 1;
}

#if 0
static int rtl_printkl2tpcache(void)
{
	printk("cid(%d),daddr(0x%x),ppp0_dev->name(%s),l2tpInfo.saddr(0x%x),l2tpInfo.tid(%d),l2tpInfo.wan_dev->name(%s),valid(%d)\n",
		l2tpInfo.cid,l2tpInfo.daddr,l2tpInfo.ppp0_dev->name,l2tpInfo.saddr,l2tpInfo.tid,
		l2tpInfo.wan_dev->name,l2tpInfo.valid);
		return 1;
}
#endif

static int l2tp_read_proc(char *page, char **start, off_t off,
		     int count, int *eof, void *data)
{
      int len;

      len = sprintf(page, "%c\n", fast_l2tp_fw + '0');

      if (len <= off+count) *eof = 1;
      *start = page + off;
      len -= off;
      if (len>count) len = count;
      if (len<0) len = 0;
      return len;
}

static int l2tp_write_proc(struct file *file, const char *buffer,
		      unsigned long count, void *data)
{
      char l2tp_tmp;

      if (count < 2)
	    return -EFAULT;

      if (buffer && !copy_from_user(&l2tp_tmp, buffer, 1)) {
	    	fast_l2tp_fw = l2tp_tmp - '0';
      		if(fast_l2tp_fw==1) {
		      l2tp_tx_id_hook=l2tp_tx_id;
      		}
	      	else
		{
		      l2tp_tx_id_hook=NULL;
		      l2tpInfo.tid=0;
		}
		memset(&l2tpInfo, 0, sizeof(l2tpInfo));
		state = L2TP_INITED;
		return count;
      }
      return -EFAULT;
}


#if defined(CONFIG_PROC_FS)
static struct proc_dir_entry *res1=NULL;
#endif

int __init fast_l2tp_init(void)
{
#if defined(CONFIG_PROC_FS)
	res1 = create_proc_entry("fast_l2tp", 0, NULL);
	if (res1) {
		res1->read_proc = l2tp_read_proc;
		res1->write_proc = l2tp_write_proc;
	}
#endif

	l2tp_tx_id_hook = NULL;
	fast_l2tp_fw = 0;
	state = 0;
	memset(&l2tpInfo, 0, sizeof(l2tpInfo));
	return 0;
}

void __exit fast_l2tp_exit(void)
{
#if defined(CONFIG_PROC_FS)
	if (res1) {
		remove_proc_entry("fast_l2tp", res1);
		res1 = NULL;
	}
#endif
}
#endif
#if 0
#ifdef CONFIG_FAST_PATH_MODULE
EXPORT_SYMBOL(fast_l2tp_init);
EXPORT_SYMBOL(fast_l2tp_exit);
EXPORT_SYMBOL(l2tp_tx_id);
EXPORT_SYMBOL(fast_l2tp_to_wan);
EXPORT_SYMBOL(fast_l2tp_rx);
EXPORT_SYMBOL(l2tp_tx_id_hook);
EXPORT_SYMBOL(fast_l2tp_fw);
#endif
#endif
