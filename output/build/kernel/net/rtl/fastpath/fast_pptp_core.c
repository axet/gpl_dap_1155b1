#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/kernel.h>
#include <linux/kernel_stat.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
//#include <linux/brlock.h>
#include <linux/net.h>
#include <linux/socket.h>

#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/string.h>
#include <net/ip.h>
#include <net/protocol.h>
#include <net/route.h>
#include <net/sock.h>
#include <net/arp.h>
#include <net/raw.h>
#include <net/checksum.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/netlink.h>

#include <linux/in.h>
#include <linux/netfilter/nf_conntrack_proto_gre.h>
#include <linux/netfilter_ipv4/ip_conntrack_pptp.h>
#include <linux/if_tunnel.h>
#include <linux/if_ether.h>
#include <linux/tcp.h>
#include <net/rtl/rtl_types.h>
#include <net/rtl/fastpath/fastpath_core.h>
#include <net/rtl/rtl865x_netif.h>
#include <net/rtl/rtl_nic.h>


#if defined(FAST_PPTP)
#include <linux/inetdevice.h>

static struct pptp_acc_info pptpAccInfo;
int pptp_tcp_finished=0;

#ifdef CONFIG_FAST_PATH_MODULE
static int pptp_conn_check=0;
int fast_pptp_fw=0;
EXPORT_SYMBOL(fast_pptp_fw);
int Get_fast_pptp_fw(void)
{
	return fast_pptp_fw;

}
#else
static int pptp_conn_check=0;
int fast_pptp_fw=0;
#endif

#if 1	// sync from voip customer for multiple ppp
int is_pptp_device(char *ppp_device)
{
	if (pptpAccInfo.pppDev && !strcmp(rtl_get_ppp_dev_name(pptpAccInfo.pppDev), ppp_device))
	{
		FAST_PPTP_PRINT("%s: pptp device = %s\n", __FUNCTION__, ppp_device);
		return 1;
	}
	return 0;
}

void set_pptp_device(char *ppp_device)
{
	if (fast_pptp_fw)
	{
		FAST_PPTP_PRINT("%s: pptp device = %s\n", __FUNCTION__, ppp_device);
		pptpAccInfo.pppDev = (void*)rtl_get_dev_by_name(ppp_device);
	}
}
#endif

static inline void bcopy(unsigned char *dst, unsigned char *src, int len)
{	int i;
	for (i=0; i<len; i++)
		dst[i] = src[i];
}

int Check_GRE_rx_net_device(void *skb)
{
	struct iphdr *iph;
	struct pptp_gre_hdr *greh;
	iph = rtl_ip_hdr(skb);
	greh = (struct pptp_gre_hdr *)(rtl_get_skb_data(skb) + iph->ihl*4);

	if((pptpAccInfo.wanDev))
	{
		//fastpptp:add the check of callid for pptp client dial up session
		if(pptpAccInfo.wanDev == (void *)rtl_get_skb_dev(skb)&& greh->call_id == pptpAccInfo.peerCallID)
			return 1;
		else
			return 0;
	}else
		return 0;
 }

// Filter PPTP Outgoing-Call-Reply to log IP and call id
void fast_pptp_filter(void *skb)
{
   	struct pptp_pkt_hdr		*pptph;
   	struct PptpControlHeader	*ctlh;		
   	struct PptpOutCallReply	*ocack;
   	struct PptpOutCallRequest *ocreq;
   	struct PptpCallDisconnectNotify *occlrnotify;
   	struct PptpClearCallRequest *occlrreq;
   	struct iphdr *iph;
   	int i;
   	struct net_device *dstdev;
   	iph=rtl_ip_hdr(skb);

	if (rtl_get_skb_len(skb) >= (sizeof(struct iphdr)+sizeof(struct tcphdr)+sizeof(*pptph)+sizeof(*ctlh)+ sizeof(*occlrreq))) {
   		struct tcphdr *th = (struct tcphdr *)(((char *)iph) + iph->ihl*4);
		int tcplen=0;
    	
   		if (iph->protocol == IPPROTO_TCP && th->source == PPTP_CONTROL_PORT) {
	    		pptph = (struct pptp_pkt_hdr *)(((char *)th)+sizeof(struct tcphdr)+(th->doff-5)*4);
    			ctlh = (struct PptpControlHeader *)(((char *)pptph) + sizeof(struct pptp_pkt_hdr));
	    		ocack = (struct PptpOutCallReply *)(((char*)ctlh) + sizeof(struct PptpControlHeader));
    			
	   		if (ntohs(pptph->packetType) == PPTP_PACKET_CONTROL &&    				
   					ntohs(ctlh->messageType) == PPTP_OUT_CALL_REPLY &&
   					ocack->resultCode == PPTP_OUTCALL_CONNECT) {

				FAST_PPTP_PRINT("Rx PPTP Call-reply, from:%s, server callid=%d\n", skb->dev->name, ocack->callID);
    				
	    			//memset(&pptpAccInfo, 0, sizeof(pptpAccInfo));  
	    			//fastpptp:add the check of callid for pptp client dial up session
	    			
	    			if(pptpAccInfo.tx_seqno !=0 && pptpAccInfo.ourIp != 0x0 && pptpAccInfo.peerIp != 0x0){
	    				if ((pptpAccInfo.ourCallID != ocack->callID) || (pptpAccInfo.peerCallID !=ocack->peersCallID)){
	    					return;
	    				}
	    			}
#if 1
	    			//fastpptp:save server's callid and our call id from peersCallID field when parse out going reply packet 
    				pptp_tcp_finished=1;
				pptpAccInfo.ourCallID= ocack->callID;
    				pptpAccInfo.peerCallID = ocack->peersCallID;
//				printk("%s(%d): pptpAccInfo.ourCallID=%d, pptpAccInfo.peerCallID=%d\n",__FUNCTION__,__LINE__,
//					pptpAccInfo.ourCallID,pptpAccInfo.peerCallID);//Added for test

	    			pptpAccInfo.ourIp= iph->daddr;
	    			pptpAccInfo.peerIp= iph->saddr;
    				pptpAccInfo.tx_seqno = 1;
    				pptpAccInfo.wanDev= (void*)rtl_get_skb_dev(skb);
//				pptpAccInfo.lan_dev =__dev_get_by_name("br0");

	    			memcpy(&pptpAccInfo.peerMac[0], rtl_get_skb_data(skb) -ETH_HLEN+ETH_ALEN, ETH_ALEN); // da of tx pkt
				memcpy(&pptpAccInfo.ourMac[0], rtl_get_skb_data(skb)-ETH_HLEN, ETH_ALEN); // sa of tx pkt
/*
				FAST_PPTP_PRINT("********************************************************************\n");
				FAST_PPTP_PRINT("ourCallID %d, peer callID %d.\n", pptpAccInfo.ourCallID, 
					pptpAccInfo.peerCallID);
				FAST_PPTP_PRINT("peerMac %x:%x:%x:%x:%x:%x.\n", pptpAccInfo.peerMac[0]
					, pptpAccInfo.peerMac[1], pptpAccInfo.peerMac[2]
					, pptpAccInfo.peerMac[3], pptpAccInfo.peerMac[4]
					, pptpAccInfo.peerMac[5]);
				FAST_PPTP_PRINT("ourMac %x:%x:%x:%x:%x:%x.\n", pptpAccInfo.ourMac[0], 
					pptpAccInfo.ourMac[1],pptpAccInfo.ourMac[2],
					pptpAccInfo.ourMac[3], pptpAccInfo.ourMac[4],
					pptpAccInfo.ourMac[5]);
				FAST_PPTP_PRINT("Dev %s %s.\n", pptpAccInfo.wanDev->name,pptpAccInfo.pppDev->name);
				FAST_PPTP_PRINT("Sip %4u.%4u.%4u.%4u  dip %4u.%4u.%4u.%4u.\n", 
					NIPQUAD(pptpAccInfo.ourIp), 
					NIPQUAD(pptpAccInfo.peerIp));
				FAST_PPTP_PRINT("********************************************************************\n");
*/
				pptpAccInfo.valid = 1;

//    			memcpy(&pptpInfo.mac_header[0], skb->data-ETH_HLEN+ETH_ALEN, ETH_ALEN); // da of tx pkt
//				memcpy(&pptpInfo.mac_header[ETH_ALEN], skb->data-ETH_HLEN, ETH_ALEN); // sa of tx pkt   									
// 				memcpy(&pptpInfo.mac_header[ETH_ALEN*2], skb->data-2, 2); // type    				    										
#endif
    			}		
			else if (ntohs(pptph->packetType) == PPTP_PACKET_CONTROL &&    				
   					ntohs(ctlh->messageType) == PPTP_CALL_DISCONNECT_NOTIFY) {
   						occlrnotify = (struct PptpCallDisconnectNotify *)(((char*)ctlh) + sizeof(struct PptpControlHeader));
   						FAST_PPTP_PRINT("Rx PPTP Call-Disconnect notify, from:%s, callid=%d\n", skb->dev->name, occlrnotify->callID);
   						
   						//fastpptp:add the check of callid for pptp client dial up session
   						if(pptpAccInfo.tx_seqno !=0 && pptpAccInfo.ourIp != 0x0 && pptpAccInfo.peerIp != 0x0){
	    						if ((pptpAccInfo.ourCallID != occlrnotify->callID)){
	    							return;
	    						}
	    					}
	    					
   						pptp_tcp_finished=0;
						pptpAccInfo.valid = 0;
    			}		
	    	}
		else if (iph->protocol == IPPROTO_TCP && th->dest == PPTP_CONTROL_PORT) {
    			pptph = (struct pptp_pkt_hdr *)(((char *)th)+sizeof(struct tcphdr)+(th->doff-5)*4);
    			ctlh = (struct PptpControlHeader *)(((char *)pptph) + sizeof(struct pptp_pkt_hdr));
    			if (ntohs(pptph->packetType) == PPTP_PACKET_CONTROL &&    				
   					ntohs(ctlh->messageType) == PPTP_OUT_CALL_REQUEST) {
   						ocreq = (struct PptpOutCallRequest *)(((char*)ctlh) + sizeof(struct PptpControlHeader));
   						FAST_PPTP_PRINT("Rx PPTP Out-Call-request, from:%s, callid=%d\n", rtl_get_skb_dev_name(skb), ocreq->callID);
/*   						if(pptpAccInfo.tx_seqno !=0 && pptpAccInfo.ourIp != 0x0 && pptpAccInfo.peerIp != 0x0){
   							FAST_PPTP_PRINT("Rx PPTP Out Call-request dst is :%u.%u.%u.%u, PPTP server IP=%u.%u.%u.%u\n", NIPQUAD(iph->daddr), NIPQUAD(pptpAccInfo.peerIp));							
   							if((fastpptp_br0_ip !=0x0) && ((fastpptp_br0_ip & fastpptp_br0_mask) == (iph->saddr & fastpptp_br0_mask)) && (iph->daddr==pptpAccInfo.peerIp) && !strcmp(skb->dev->name, "br0")){
	   							if(ocreq->callID == pptpAccInfo.peerCallID){//conflict PPTP client call id from LAN host
	   								for(i=0;i<MAX_CONFLICT;i++){
	   									if(pptp_passthru_entry[i].isValid==0 && pptp_passthru_entry[i].LANHost==0){
	   										pptp_passthru_entry[i].greCallID_ToLAN=ocreq->callID;
	   										ocreq->callID = pptpAccInfo.peerCallID+i+1;
	   										pptp_passthru_entry[i].greCallID_ToWAN =ocreq->callID;
	   										pptp_passthru_entry[i].isValid=1;
	   										pptp_passthru_entry[i].LANHost= iph->saddr;
	   										FAST_PPTP_PRINT("Rx PPTP out Call-request,, from:%s, callid=0x%x, replace to CallID=%d, LANHost=%u.%u.%u.%u\n", skb->dev->name, pptp_passthru_entry[i].greCallID_ToLAN,pptp_passthru_entry[i].greCallID_ToWAN, NIPQUAD(pptp_passthru_entry[i].LANHost));
	   										tcplen = iph->tot_len-(iph->ihl*4);
	     										th->check = 0;
		    									th->check=csum_tcpudp_magic(skb->nh.iph->saddr, skb->nh.iph->daddr, tcplen, IPPROTO_TCP, csum_partial((char *)th, tcplen, 0));
		    									break;
	   									}else{
	   										if(pptp_passthru_entry[i].isValid==1 && pptp_passthru_entry[i].LANHost==iph->saddr){
	   											FAST_PPTP_PRINT("Rx PPTP out Call-request,, from:%s, callid=0x%x, replace to CallID=%d, LANHost=%u.%u.%u.%u already exist\n", skb->dev->name, pptp_passthru_entry[i].greCallID_ToLAN,pptp_passthru_entry[i].greCallID_ToWAN, NIPQUAD(pptp_passthru_entry[i].LANHost));
	   											break;
	   										}
	    									}
									}			
								}
   							}	
   						}
  */
	    			}else if (ntohs(pptph->packetType) == PPTP_PACKET_CONTROL &&    				
   						ntohs(ctlh->messageType) == PPTP_CALL_CLEAR_REQUEST) {
   						occlrreq = (struct PptpClearCallRequest *)(((char*)ctlh) + sizeof(struct PptpControlHeader));
   						FAST_PPTP_PRINT("Rx PPTP Call-clear-request, from:%s, callid=0x%x\n", rtl_get_skb_dev_name(skb), occlrreq->callID);
/*   						if(pptpAccInfo.tx_seqno !=0 && pptpAccInfo.ourIp != 0x0 && pptpAccInfo.peerIp != 0x0){
   							if((fastpptp_br0_ip !=0x0) && ((fastpptp_br0_ip & fastpptp_br0_mask) == (iph->saddr & fastpptp_br0_mask)) && (iph->daddr==pptpAccInfo.peerIp) && !strcmp(skb->dev->name, "br0")){
   								if(occlrreq->callID == pptpAccInfo.peerCallID){//conflict PPTP client call id from LAN host
	   								if(Last_pptp_passthru_entry.isValid==1 && Last_pptp_passthru_entry.LANHost==iph->saddr){
			   								if(Last_pptp_passthru_entry.greCallID_ToLAN==occlrreq->callID){
			   									FAST_PPTP_PRINT("Rx PPTP Call-clear-request, from:%s, callid=0x%x, replace to CallID=%d, LANHost=%u.%u.%u.%u\n", skb->dev->name, occlrreq->callID,Last_pptp_passthru_entry.greCallID_ToWAN, NIPQUAD(Last_pptp_passthru_entry.LANHost));
			   									occlrreq->callID = Last_pptp_passthru_entry.greCallID_ToWAN;
				   								tcplen = iph->tot_len-(iph->ihl*4);
				     								th->check = 0;
					    							th->check=csum_tcpudp_magic(skb->nh.iph->saddr, skb->nh.iph->daddr, tcplen, IPPROTO_TCP, csum_partial((char *)th, tcplen, 0));
					    							
					    							Last_pptp_passthru_entry.isValid=0; 
												Last_pptp_passthru_entry.LANHost=0;
												Last_pptp_passthru_entry.greCallID_ToLAN=0;
												Last_pptp_passthru_entry.greCallID_ToWAN=0;
				    							}
		   							}
	   							}
	   						}	
   						}	
 */
	    				}else if (ntohs(pptph->packetType) == PPTP_PACKET_CONTROL &&    				
   						ntohs(ctlh->messageType) == PPTP_CALL_DISCONNECT_NOTIFY) {
   						occlrnotify = (struct PptpCallDisconnectNotify *)(((char*)ctlh) + sizeof(struct PptpControlHeader));
   						FAST_PPTP_PRINT("Rx PPTP Call-Disconnect notify, from:%s, callid=0x%x\n", rtl_get_skb_dev_name(skb), occlrnotify->callID);
/*   						if(pptpAccInfo.tx_seqno !=0 && pptpAccInfo.ourIp != 0x0 && pptpAccInfo.peerIp != 0x0){
   							if((fastpptp_br0_ip !=0x0) && ((fastpptp_br0_ip & fastpptp_br0_mask) == (iph->saddr & fastpptp_br0_mask)) && (iph->daddr==pptpAccInfo.peerIp) && !strcmp(skb->dev->name, "br0")){
   								if(occlrnotify->callID == pptpAccInfo.peerCallID){//conflict PPTP client call id from LAN host
		   							if(Last_pptp_passthru_entry.isValid==1 && Last_pptp_passthru_entry.LANHost==iph->saddr){
			   								if(Last_pptp_passthru_entry.greCallID_ToLAN==occlrnotify->callID){
			   									FAST_PPTP_PRINT("Rx PPTP Call-Disconnect notify, from:%s, callid=0x%x, replace to CallID=%d, LANHost=%u.%u.%u.%u\n", skb->dev->name, occlrnotify->callID,Last_pptp_passthru_entry.greCallID_ToWAN, NIPQUAD(Last_pptp_passthru_entry.LANHost));
			   									occlrnotify->callID = Last_pptp_passthru_entry.greCallID_ToWAN;
				   								tcplen = iph->tot_len-(iph->ihl*4);
				     								th->check = 0;
					    							th->check=csum_tcpudp_magic(skb->nh.iph->saddr, skb->nh.iph->daddr, tcplen, IPPROTO_TCP, csum_partial((char *)th, tcplen, 0));
					    							
					    							Last_pptp_passthru_entry.isValid=0; 
												Last_pptp_passthru_entry.LANHost=0;
												Last_pptp_passthru_entry.greCallID_ToLAN=0;
												Last_pptp_passthru_entry.greCallID_ToWAN=0;
				    							}
		   							}
								}
							}
	    					}
*/						
    					}
    			}		
		
	}			
}
/*
void fast_pptp_map_rx_callID(struct pptp_gre_hdr *check_greh, unsigned int LANHost)
{
	int i;
	unsigned char *ppp_data;
	unsigned short ppp_protocol=0;
	int offset = sizeof(*check_greh) - 8;	// delete seq and ack no first
	
	if (!fast_pptp_fw)
		return;
		
			
		for(i=0;i<MAX_CONFLICT;i++){
			if(pptp_passthru_entry[i].isValid==1 && pptp_passthru_entry[i].LANHost==LANHost){
	   			if(pptp_passthru_entry[i].greCallID_ToWAN==check_greh->call_id){
	   				FAST_PPTP_PRINT("fast_pptp_map_rx_callID Got CallID=%d, replace to original Call ID to lan:%d, for LAN Host:%u.%u.%u.%u\n", check_greh->call_id, pptp_passthru_entry[i].greCallID_ToLAN,NIPQUAD(LANHost));
	   				check_greh->call_id=pptp_passthru_entry[i].greCallID_ToLAN;
	   				if (GRE_IS_S(check_greh->flags))	
						offset += 4;
						
					if (GRE_IS_A(check_greh->version))
						offset += 4;
						
					ppp_data = ((char *)check_greh) + offset;	
					if((ppp_data[0]==0xFF) && (ppp_data[1] == 0x03)){
						ppp_protocol=(ppp_data[2] << 8) + ppp_data[3];
						if(ppp_protocol == 0xC021){//LCP protocol
							if(ppp_data[4]==0x06 || ppp_data[4]==0x05){//LCP termination ack or termination
								FAST_PPTP_PRINT("fast_pptp_map_rx_callID Got termination, set entry %d to invalid, callid=%d\n", i, pptp_passthru_entry[i].greCallID_ToLAN);
								Last_pptp_passthru_entry.isValid=pptp_passthru_entry[i].isValid; 
								Last_pptp_passthru_entry.LANHost=pptp_passthru_entry[i].LANHost;
								Last_pptp_passthru_entry.greCallID_ToLAN=pptp_passthru_entry[i].greCallID_ToLAN;
								Last_pptp_passthru_entry.greCallID_ToWAN=pptp_passthru_entry[i].greCallID_ToWAN;
								pptp_passthru_entry[i].isValid=0;
	   							pptp_passthru_entry[i].LANHost=0;
	   							pptp_passthru_entry[i].greCallID_ToLAN=0;
	   							pptp_passthru_entry[i].greCallID_ToWAN=0;
							}
						}
					}
	    				break;
		    		}
	   		}
		}
}

void fast_pptp_map_rx_callID_pptp(const struct iphdr *iph, unsigned int LANHost)
{
	struct pptp_pkt_hdr		*pptph;
   	struct PptpControlHeader	*ctlh;		
   	struct PptpOutCallReply	*ocack;
	int i, tcplen=0;
	struct tcphdr *th = (void *)iph + iph->ihl * 4;
	
	if (!fast_pptp_fw)
		return;
		
	pptph = (struct pptp_pkt_hdr *)(((char *)th)+sizeof(struct tcphdr)+(th->doff-5)*4);
    	ctlh = (struct PptpControlHeader *)(((char *)pptph) + sizeof(struct pptp_pkt_hdr));
	ocack = (struct PptpOutCallReply *)(((char*)ctlh) + sizeof(struct PptpControlHeader));
    			
	   		if (ntohs(pptph->packetType) == PPTP_PACKET_CONTROL &&    				
   					ntohs(ctlh->messageType) == PPTP_OUT_CALL_REPLY &&
   					ocack->resultCode == PPTP_OUTCALL_CONNECT) {
		   		for(i=0;i<MAX_CONFLICT;i++){
					if(pptp_passthru_entry[i].isValid==1 && pptp_passthru_entry[i].LANHost==LANHost){
			   			if(pptp_passthru_entry[i].greCallID_ToWAN==ocack->peersCallID){
			   				FAST_PPTP_PRINT("fast_pptp_map_rx_callID_pptp Got CallID=%d, replace to original Call ID to lan:%d\n", ocack->peersCallID, pptp_passthru_entry[i].greCallID_ToLAN);
			   				ocack->peersCallID=pptp_passthru_entry[i].greCallID_ToLAN;
			   				tcplen = iph->tot_len-(iph->ihl*4);
	     						th->check = 0;
		    					th->check=csum_tcpudp_magic(iph->saddr, iph->daddr, tcplen, IPPROTO_TCP, csum_partial((char *)th, tcplen, 0));
				    			break;
				    		}
			   		}
				}
   		}		
}
*/

// Packet come from WAN, and it is GRE data
//	  delete IP+GRE+PPP header
int fast_pptp_to_lan(void **pskb)
{
	struct iphdr *iph;
	struct pptp_gre_hdr *greh;					
	unsigned char ppp_type=0;
	void *ppp;
	void *skb = *pskb;
	int pull_offset=0;
//Brad add ---------	
	int check_stats=0;
	unsigned char *data;


//Brad add end-------
//	extern struct sk_buff *ppp_receive_nonmp_frame(struct ppp *ppp, struct sk_buff *skb, int is_fast_fw);
	if ((void*)rtl_get_skb_dev(skb) != pptpAccInfo.wanDev || rtl_ip_hdr(skb)->protocol != IPPROTO_GRE || rtl_get_skb_len(skb) < sizeof(struct iphdr))
		return 0;

	iph = rtl_ip_hdr(skb);
	data = rtl_get_skb_data(skb);
	greh = (struct pptp_gre_hdr *)(data + iph->ihl*4);

	if ((greh->version&7) == GRE_VERSION_PPTP &&
						ntohs(greh->protocol) == GRE_PROTOCOL_PPTP) {
		unsigned char *ppp_data;	
		int offset = sizeof(*greh) - 8;	// delete seq and ack no
		int ppp_offset=0;

		if(greh->call_id	 != pptpAccInfo.peerCallID){
//			FAST_PPTP_PRINT("%s:NOT PPTP Dial Up session!\n", __FUNCTION__);	   
			return 0;  
		}	
				
		if (GRE_IS_S(greh->flags)) {	
			pptpAccInfo.rx_seqno= ntohl(greh->seq);						
			offset += 4;
		}	
			
		if (GRE_IS_A(greh->version))
			offset += 4;

		ppp_data = ((char *)greh) + offset;				

		ppp_offset = 0;				
		if (greh->payload_len > 0) {	
			// check PPP IP protocol
			if (*ppp_data == 0) {
				ppp_offset = 1;
				ppp_data++;
			}
			else if (*ppp_data == 0xff && *(ppp_data+1) == 0x03) {									
				ppp_offset = 2;
				ppp_data += 2;
				if (*ppp_data == 0) {
					ppp_offset++;
					ppp_data++;
				}
			}											
			if (*ppp_data == 0x21 || *ppp_data == 0xfd) {
				ppp_offset++;					
				ppp_type = *ppp_data;
			}
			else
				ppp_offset = 0;		
		}	

		if (ppp_offset ==  0) 
			return 0;

		offset = iph->ihl*4 + offset + ppp_offset;	// tunnel IP offset

		if (ppp_type != 0x21) {	// !PPP_IP
			if (!pptpAccInfo.pppDev|| !(rtl_get_ppp_dev_priv(pptpAccInfo.pppDev)))
				return 0;

			skb_pull(skb, offset-2);
			data = rtl_get_skb_data(skb);
			data[0] = 0;
			data[1] = ppp_type;
			ppp = rtl_get_ppp_dev_priv(pptpAccInfo.pppDev);

			skb = (void*)rtl_ppp_receive_nonmp_frame(ppp, skb, 1);
			if (skb == NULL) {
				FAST_PPTP_PRINT("%s: ppp_receive_nonmp_frame() return error!\n", __FUNCTION__);
				return -2;	// discard
			}			
			*pskb = skb;	
//Brad add ----------			
			check_stats = 1;				
//Brad add end-------			
		}
		else {	
			skb_pull(skb, offset);					
			pull_offset = offset;
		}
			
		FAST_PPTP_PRINT("delete GRE + PPTP header\n");

		// fix unalignment issue
		offset = ((unsigned long)rtl_get_skb_data(skb)) % 4;
		if (offset) {
			if (rtl_skb_headroom(skb) >= offset) {
				/*printk("Headroom %d, offset %d.\n", skb_headroom(skb), offset);*/
				memmove(rtl_get_skb_data(skb)-offset, rtl_get_skb_data(skb), rtl_get_skb_len(skb));
				rtl_set_skb_data(skb, offset, 1);
				rtl_set_skb_tail(skb, offset, 1);
			}
		}


		//skb->transport_header= skb->network_header= skb->data;
		rtl_skb_reset_network_header(skb);
		rtl_skb_reset_transport_header(skb);

		// sync from voip customer for multiple ppp
		// if ((pptpAccInfo.pppDev == NULL)||((pptpAccInfo.pppDev->name== NULL))||(strcmp(pptpAccInfo.pppDev->name, RTL_PS_PPP0_DEV_NAME)!=0))
		// pptpAccInfo.pppDev=__dev_get_by_name(&init_net, RTL_PS_PPP0_DEV_NAME);

		if (pptpAccInfo.pppDev) {
//Brad add --------
		if(check_stats ==0){
			if(rtl_get_ppp_dev_priv(pptpAccInfo.pppDev)){
				ppp = rtl_get_ppp_dev_priv(pptpAccInfo.pppDev);
				rtl_inc_ppp_stats(ppp, 0, rtl_get_skb_len(skb)-2);
				//++ppp->stats.rx_packets;
				//ppp->stats.rx_bytes += rtl_get_skb_len(skb) - 2;
			}//else{
			//		printk("ppp0_dev priv==NULL\n");
			//	}
		}
//Brad add end--------
			rtl_set_skb_dev(skb, pptpAccInfo.pppDev);
			return 1;
		}
#if 1 // sync from voip customer for multiple ppp
		else	
			return 0;
#endif
		
		FAST_PPTP_PRINT("pass up\n");				
		if (pull_offset) {
			skb_push(skb, pull_offset);
			//skb->transport_header= skb->network_header= skb->data;
			rtl_skb_reset_network_header(skb);
			rtl_skb_reset_transport_header(skb);
		}

	}
	return 0;		
}

unsigned long get_fastpptp_lastxmit(void)
{
	if(fast_pptp_fw && pptpAccInfo.valid == 1)
		return pptpAccInfo.fast_pptp_lastxmit;
	
	return 0;
}

// Packet come from LAN and dst dev is ppp0, 
// add IP+GRE+PPTP header
 int fast_pptp_to_wan(void *skb)
{
	int	header_len;
	struct iphdr *iph_new, iph_newone;
	struct pptp_gre_hdr	*greh, grehone;
	unsigned char tos;
	struct ethhdr *eth;
	int ppp_hdr_len=0;		
	int wanip1=0, wanip2=0,wanip3=0;

	wanip1 = (rtl_ip_hdr(skb)->saddr & 0xFF000000);
	wanip2 = (rtl_ip_hdr(skb)->saddr & 0x00FF0000);
	wanip3 = (rtl_ip_hdr(skb)->saddr & 0x0000FF00);
	if((wanip1 == 0x0A000000) && (wanip2 == 0x00400000) && (wanip3 == 0x00004000) && pptp_conn_check==3){
		//in this state, wan ip is not valid, we return the packet to kernel for trigger dial-up
		//panic_printk("wan ip is not valid, we return the packet to kernel to trigger dial-up, pptp is disconnect\n");
		return 0;
	}

	if ((pptpAccInfo.pppDev == NULL)||((rtl_get_ppp_dev_name(pptpAccInfo.pppDev) == NULL))||(strcmp(rtl_get_ppp_dev_name(pptpAccInfo.pppDev), RTL_PS_PPP0_DEV_NAME)!=0))
		return 0;			// sync from voip customer for multiple ppp

	if (pptpAccInfo.valid && pptpAccInfo.pppDev && rtl_get_ppp_dev_priv(pptpAccInfo.pppDev))
	{
		//extern int ppp_start_xmit(struct sk_buff *skb, struct net_device *dev);
		void *ppp = rtl_get_ppp_dev_priv(pptpAccInfo.pppDev);

		// if VJ compressed/uncompressed and !MPPE, not forward
		if (rtl_ppp_vj_check(ppp) == 1) {
			FAST_PPTP_PRINT("vj is used, skip fast forwarding.\n");
			return 0;			
		}

		tos = rtl_ip_hdr(skb)->tos;

		if (rtl_get_ppp_xmit_pending(ppp)) {
			printk("%s: ppp->xmit_pending\n", __FUNCTION__);
			return 0;
		}
		rtl_set_skb_cb(skb, "RTL", 3);
		rtl_ppp_start_xmit(skb, pptpAccInfo.pppDev);
		if (!(rtl_get_ppp_xmit_pending(ppp))) {
			printk("%s: ppp_start_xmit() drop skb!", __FUNCTION__);
			return 1;
		}

		skb = (void*)(rtl_get_ppp_xmit_pending(ppp));

		rtl_set_ppp_xmit_pending(ppp, NULL);

		header_len = ETH_HLEN + sizeof(*iph_new) + sizeof(*greh); // mac-header+ip+gre
		if (rtl_skb_headroom(skb) < header_len || rtl_skb_cloned(skb) || rtl_skb_shared(skb)) {
			void *new_skb = (void*)skb_realloc_headroom(skb, header_len);
			if (!new_skb) {
				printk("%s: skb_realloc_headroom failed!\n", __FUNCTION__);
				return 0;
			}									
			dev_kfree_skb(skb);
			skb = new_skb;
		}			
	
		// build mac header						
//		memcpy(skb_push(skb, header_len), pptpInfo.mac_header, ETH_HLEN);
		eth = (struct ethhdr *)skb_push(skb, header_len);
		memcpy(eth->h_dest, pptpAccInfo.peerMac, ETH_ALEN);
		memcpy(eth->h_source, pptpAccInfo.ourMac, ETH_ALEN);
		eth->h_proto = htons(0x0800);
					
		// build ip header							
		iph_new = &iph_newone;	
		iph_new->version	=	4;
		iph_new->ihl		=	sizeof(struct iphdr) >> 2;
		iph_new->frag_off	=	0x4000;		/* don't do fragement */
		iph_new->protocol	=	IPPROTO_GRE;
		iph_new->tos		=	tos;
		iph_new->daddr	=	pptpAccInfo.peerIp;
		iph_new->saddr	=	pptpAccInfo.ourIp;
		iph_new->ttl 		=	IPDEFTTL;
		rtl_set_skb_ip_summed(skb, CHECKSUM_NONE);
		iph_new->tot_len	=	htons(rtl_get_skb_len(skb) - ETH_HLEN);
		iph_new->id		=	htons(++pptpAccInfo.tx_ipID);
	    	iph_new->check	=	0;
	    	iph_new->check	=	ip_fast_csum((unsigned char *)iph_new, iph_new->ihl);	
	    	pptpAccInfo.ipID=	iph_new->id; // save id to check in sync_pptp_gre_seqno()
	    	memcpy(rtl_get_skb_data(skb) + ETH_HLEN, &iph_newone, sizeof(iph_newone));

    		// build gre header
	    	greh 			= &grehone;
	    	greh->flags		= GRE_FLAG_K | GRE_FLAG_S;
	    	greh->version	= GRE_VERSION_PPTP | GRE_FLAG_A;
	    	greh->protocol	= htons(GRE_PROTOCOL_PPTP);

	    	greh->payload_len	= htons(rtl_get_skb_len(skb) - header_len + ppp_hdr_len);
	    	greh->call_id		= pptpAccInfo.ourCallID;
	    	greh->seq 		= htonl(pptpAccInfo.tx_seqno++);
    		greh->ack		= htonl(pptpAccInfo.rx_seqno);
	    	memcpy(rtl_get_skb_data(skb)+ETH_HLEN+sizeof(struct iphdr), &grehone, sizeof(grehone));
		pptpAccInfo.fast_pptp_lastxmit = jiffies;

    		FAST_PPTP_PRINT("add GRE header, id=%d, gre-len=%d!\n", iph_new->id, rtl_get_skb_len(skb)-header_len);
		rtl_set_skb_dev(skb, pptpAccInfo.wanDev);
    		dev_queue_xmit(skb);
	    	return 1;						
	}
	else {
		if (pptpAccInfo.pppDev== NULL)
			printk("pptpInfo.ppp0_dev == NULL\n");
		else if (rtl_get_ppp_dev_priv(pptpAccInfo.pppDev) == NULL)
			printk("pptpInfo.ppp0_dev->priv == NULL\n");
	}

	return 0;	
}		

// Sync Rx GRE seq no to daemon.
//		Replace tx-seq and ack-seq.
void fast_pptp_sync_rx_seq(void *skb)
{
	struct pptp_gre_hdr *greh;
	unsigned char *iph = (unsigned char *)rtl_ip_hdr(skb);

	greh = (struct pptp_gre_hdr *)(iph + sizeof(struct iphdr));
		
	if ((greh->version&7) == GRE_VERSION_PPTP &&
						ntohs(greh->protocol) == GRE_PROTOCOL_PPTP) {			
		
		FAST_PPTP_PRINT("replace GRE seqno to daemon:");							
							
		// pass-up, replace seq and ack no                		
		if (GRE_IS_S(greh->flags)) {
			FAST_PPTP_PRINT("tx-seqno=%d ", pptpAccInfo.rx_seqno_daemon);
			greh->seq = htonl(pptpAccInfo.rx_seqno_daemon++);
		}
		if (GRE_IS_A(greh->version)) {
			FAST_PPTP_PRINT("ackno=%d ", pptpAccInfo.tx_seqno_daemon);							
			greh->ack = htonl(pptpAccInfo.tx_seqno_daemon); 
		}
		FAST_PPTP_PRINT("\n");
	}	
}


// Sync tx GRE seq no. 
//   replace gre tx seq, and ack number if packet is not sent from upper layer
void sync_tx_pptp_gre_seqno(void *skb)
{
	struct iphdr *iph = (struct iphdr *)(rtl_get_skb_data(skb)+ETH_HLEN);
	unsigned long x;

	if (!fast_pptp_fw)
		return;
	
	local_irq_save(x);

	if (iph->protocol == IPPROTO_GRE && rtl_get_skb_len(skb) > (sizeof(struct iphdr)+ETH_HLEN)) {
		struct pptp_gre_hdr *greh;
		int is_forward = 0;
		unsigned int lv;
		unsigned short protocol;

		greh = (struct pptp_gre_hdr *)(rtl_get_skb_data(skb) + ETH_HLEN + iph->ihl*4);
		bcopy((unsigned char *)&protocol, (unsigned char *)&greh->protocol, 2);
			
		if ((greh->version&7) == GRE_VERSION_PPTP &&
						ntohs(protocol) == GRE_PROTOCOL_PPTP) {					
			if (GRE_IS_S(greh->flags)) {	
				if (iph->id != pptpAccInfo.ipID) { // not fast_pptp forward
					if((greh->call_id != pptpAccInfo.ourCallID) || (pptpAccInfo.peerIp!= iph->daddr)){
						is_forward = 1;
					}else{	
						lv = ntohl(greh->seq);				
						bcopy((unsigned char *)&pptpAccInfo.tx_seqno_daemon, (unsigned char *)&lv, 4);
						lv = htonl(pptpAccInfo.tx_seqno++);
						bcopy((unsigned char *)&greh->seq,  (unsigned char *)&lv, 4);
					}
				}
				else
					is_forward = 1;					
			}			
			if (GRE_IS_A(greh->version)) {
				if((greh->call_id != pptpAccInfo.ourCallID) || (pptpAccInfo.peerIp != iph->daddr)){
					is_forward = 1;
				}else{
					lv = htonl(pptpAccInfo.rx_seqno);	
					bcopy((unsigned char *)&greh->ack, (unsigned char *)&lv, 4);	
				}
			}

			if (!is_forward) {				
				iph->id = htons(++pptpAccInfo.tx_ipID);
				iph->check = 0;
				iph->check  = ip_fast_csum((unsigned char *)iph, iph->ihl);					
			}		
			
		}
	}			

	local_irq_restore(x);
}

int ppfw_read_proc(char *page, char **start, off_t off,
		     int count, int *eof, void *data)
{
      int len;

      len = sprintf(page, "%d\n", fast_pptp_fw);
      if (len <= off+count) *eof = 1;
      *start = page + off;
      len -= off;
      if (len>count) len = count;
      if (len<0) len = 0;
      return len;
}

static unsigned long atoi_dec(char *s)
{
	unsigned long k = 0;

	k = 0;
	while (*s != '\0' && *s >= '0' && *s <= '9') {
		k = 10 * k + (*s - '0');
		s++;
	}
	return k;
}

int pptpconn_read_proc(char *page, char **start, off_t off,
		     int count, int *eof, void *data)
{
      int len;

      len = sprintf(page, "%d\n", pptp_conn_check);
      if (len <= off+count) *eof = 1;
      *start = page + off;
      len -= off;
      if (len>count) len = count;
      if (len<0) len = 0;
      return len;
}

int pptpconn_write_proc(struct file *file, const char *buffer,
		      unsigned long count, void *data)
{
	char tmpbuf[200];	

	if (count < 2) 
		return -EFAULT;

	if (buffer && !copy_from_user(tmpbuf, buffer, count)) {
		pptp_conn_check = atoi_dec(tmpbuf);
	}
	return -EFAULT;				
}

int ppfw_write_proc(struct file *file, const char *buffer,
		      unsigned long count, void *data)
{
	char *tmpbuf;

/*
	int i;
	struct in_device *in_dev;
	struct in_ifaddr **ifap = NULL;
	struct in_ifaddr *ifa = NULL;
	struct net_device *landev;
*/
	
	if (count < 2) 
		return -EFAULT;

	tmpbuf = kmalloc(count+32, GFP_ATOMIC);
	if (tmpbuf==NULL) 
		return -EFAULT;
	
	if (buffer && !copy_from_user(tmpbuf, buffer, count)) {
		fast_pptp_fw = atoi_dec(tmpbuf);

		//fastpptp:init pptpInfo struct when system re-init(init.sh) and pppd disconnect(disconnect.sh)
	    	memset((void *)&pptpAccInfo, 0, sizeof(pptpAccInfo));

/*		
		for(i=0;i<MAX_CONFLICT;i++){
              	memset(&pptp_passthru_entry[i], 0x0, sizeof(_gre_callId_map_entry)); 
              }
              memset(&Last_pptp_passthru_entry, 0x0, sizeof(_gre_callId_map_entry));

              if ((landev = __dev_get_by_name("br0")) != NULL){
			ifa =NULL;
			if ((in_dev=__in_dev_get_rtnl(landev)) != NULL) {
				for (ifap=(&(in_dev->ifa_list)); 
						(ifap!=NULL) && ((ifa=*ifap) != NULL); 
						ifap=&(ifa->ifa_next)) 
				{
					if (strcmp("br0", ifa->ifa_label) == 0){
						break; 
					}
				}
				if(ifa != NULL){
					fastpptp_br0_ip = ifa->ifa_address;
					fastpptp_br0_mask = ifa->ifa_mask;
				}
			}
		}
*/

 		if (fast_pptp_fw)
		{
			sync_tx_pptp_gre_seqno_hook = sync_tx_pptp_gre_seqno;
		}
		else
		{
			sync_tx_pptp_gre_seqno_hook = NULL; 			
		}

		kfree(tmpbuf);
		return count;
	}

	kfree(tmpbuf);
	return -EFAULT;				
}		

#if defined(CONFIG_PROC_FS)
static struct proc_dir_entry *res1=NULL;
static struct proc_dir_entry *res_check_pptp=NULL;
#endif

#ifdef CONFIG_FAST_PATH_MODULE
int  fast_pptp_init(void)
#else
int __init fast_pptp_init(void)
#endif
{
#if defined(CONFIG_PROC_FS)
	res1 = create_proc_entry("fast_pptp", 0, NULL);
	if (res1) {
		res1->read_proc = ppfw_read_proc;
		res1->write_proc = ppfw_write_proc;
	}

	res_check_pptp = create_proc_entry("pptp_conn_ck", 0, NULL);
	if (res_check_pptp) {
		res_check_pptp->read_proc = pptpconn_read_proc;
		res_check_pptp->write_proc = pptpconn_write_proc;
	}
#endif

	memset((void*)&pptpAccInfo, 0, sizeof(pptpAccInfo));
	return 0;
}

#ifdef CONFIG_FAST_PATH_MODULE
void fast_pptp_exit(void)
#else
void __exit fast_pptp_exit(void)
#endif
{
#if defined(CONFIG_PROC_FS)
	if (res1) {
		remove_proc_entry("fast_pptp", res1);		
		res1 = NULL;
	}

	if (res_check_pptp) {
		remove_proc_entry("pptp_conn_ck", res_check_pptp);		
		res_check_pptp = NULL;
	}
#endif
}

#endif
#if 0
#ifdef CONFIG_FAST_PATH_MODULE
EXPORT_SYMBOL(fast_pptp_exit);
EXPORT_SYMBOL(fast_pptp_init);
EXPORT_SYMBOL(fast_pptp_filter);
EXPORT_SYMBOL(fast_pptp_to_wan);
EXPORT_SYMBOL(Get_fast_pptp_fw);
EXPORT_SYMBOL(fast_pptp_sync_rx_seq);
EXPORT_SYMBOL(Check_GRE_rx_net_device);
EXPORT_SYMBOL(fast_pptp_to_lan);
#endif
#endif

