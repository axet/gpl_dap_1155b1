
#if defined (CONFIG_RTL_FAST_PPPOE)
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/kernel.h>
#include <linux/kernel_stat.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
//#include <linux/brlock.h>
#include <linux/net.h>
#include <linux/socket.h>

#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/string.h>
#include <net/ip.h>
#include <net/protocol.h>
#include <net/route.h>
#include <net/sock.h>
#include <net/arp.h>
#include <net/raw.h>
#include <net/checksum.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/netlink.h>

#include <linux/in.h>
#include <linux/udp.h>
#include <linux/if_tunnel.h>
#include <linux/if_ether.h>
#include <net/rtl/rtl_types.h>
#include <net/rtl/fastpath/fastpath_core.h>

#ifdef CONFIG_BRIDGE
#include <bridge/br_private.h>
#endif


#if defined(CONFIG_NET_SCHED)
extern int gQosEnabled;
#endif

#include <linux/if_pppox.h>
#include <linux/if_ether.h>

int fast_pppoe_fw=0; 
struct pppoe_info fast_pppoe_info[MAX_PPPOE_ENTRY];

#if defined(CONFIG_PROC_FS)
struct proc_dir_entry *fast_pppoe_proc=NULL;
#endif

static int fast_pppoe_read_proc(char *page, char **start, off_t off,
		     int count, int *eof, void *data)
{
	int len;
	int i;

	len = sprintf(page, "fast pppoe enable:%c\n", fast_pppoe_fw + '0');
	
	for(i=0; i<MAX_PPPOE_ENTRY; i++)
	{
		if(fast_pppoe_info[i].valid==0)
		{
			
			len += sprintf(page+len, "[%d] null,null,0\n",i);

		}
		else
		{
			
			#if defined (CONFIG_RTL_FAST_PPPOE_DEBUG)
			len += sprintf(page+len, "[%d] %s,%s,%d,0x%x:%x:%x:%x:%x:%x,%d,%d\n",i,fast_pppoe_info[i].ppp_dev, fast_pppoe_info[i].wan_dev, fast_pppoe_info[i].sid,
			fast_pppoe_info[i].peer_mac[0],fast_pppoe_info[i].peer_mac[1],fast_pppoe_info[i].peer_mac[2],fast_pppoe_info[i].peer_mac[3],fast_pppoe_info[i].peer_mac[4],fast_pppoe_info[i].peer_mac[5],fast_pppoe_info[i].total_rx,fast_pppoe_info[i].total_tx);
			#else
			len += sprintf(page+len, "[%d] %s,%s,%d,0x%x:%x:%x:%x:%x:%x\n",i,fast_pppoe_info[i].ppp_dev, fast_pppoe_info[i].wan_dev, fast_pppoe_info[i].sid,
			fast_pppoe_info[i].peer_mac[0],fast_pppoe_info[i].peer_mac[1],fast_pppoe_info[i].peer_mac[2],fast_pppoe_info[i].peer_mac[3],fast_pppoe_info[i].peer_mac[4],fast_pppoe_info[i].peer_mac[5]);
			#endif
		}
	
	}
	
	if (len <= off+count) *eof = 1;
	*start = page + off;
	len -= off;
	if (len>count) len = count;
	if (len<0) len = 0;

	return len;
}

static int fast_pppoe_write_proc(struct file *file, const char *buffer,
		      unsigned long count, void *data)
{
      char l2tp_tmp;

      if (count < 2)
	    return -EFAULT;

      if (buffer && !copy_from_user(&l2tp_tmp, buffer, 1)) 
	  {
	    	fast_pppoe_fw = l2tp_tmp - '0';

			return count;
      }
      return -EFAULT;
}


int  __init fast_pppoe_init(void)
{

#if defined(CONFIG_PROC_FS)
	fast_pppoe_proc = create_proc_entry("fast_pppoe", 0, NULL);
	if (fast_pppoe_proc) {
		fast_pppoe_proc->read_proc = fast_pppoe_read_proc;
		fast_pppoe_proc->write_proc = fast_pppoe_write_proc;
	}
#endif

	memset(fast_pppoe_info, 0 , sizeof(fast_pppoe_info));
	fast_pppoe_fw=1;
	
	return 0;
}

int __exit fast_pppoe_exit(void)
{

#if defined(CONFIG_PROC_FS)
	if (fast_pppoe_proc) {
		remove_proc_entry("fast_pppoe", fast_pppoe_proc);		
		fast_pppoe_proc = NULL;
	}
#endif

	return 0;
}

int clear_pppoe_info(char *ppp_dev, char *wan_dev, unsigned short sid,
								unsigned int our_ip,unsigned int	peer_ip,
								unsigned char * our_mac, unsigned char *peer_mac)
{
	int i;
	for(i=0; i<MAX_PPPOE_ENTRY; i++)
	{
		if(fast_pppoe_info[i].valid==0)
		{
			continue;
		}
		
		
		if((ppp_dev!=NULL) && (strcmp(fast_pppoe_info[i].ppp_dev,ppp_dev)==0))
		{
			memset(&fast_pppoe_info[i], 0 , sizeof(struct pppoe_info));
		}

		if((wan_dev!=NULL) && (strcmp(fast_pppoe_info[i].wan_dev,wan_dev)==0))
		{
			memset(&fast_pppoe_info[i], 0 , sizeof(struct pppoe_info));
		}
		
		if((peer_mac!=NULL) && (memcmp(fast_pppoe_info[i].peer_mac,peer_mac,6)==0))
		{
			memset(&fast_pppoe_info[i], 0 , sizeof(struct pppoe_info));
		}

		if((our_mac!=NULL) && (memcmp(fast_pppoe_info[i].our_mac,our_mac,6)==0))
		{
			memset(&fast_pppoe_info[i], 0 , sizeof(struct pppoe_info));
		}

		if(fast_pppoe_info[i].sid==sid)
		{
			memset(&fast_pppoe_info[i], 0 , sizeof(struct pppoe_info));
		}

		
	}
	return 0;
}

static struct pppoe_info* find_pppoe_info_fast( unsigned short sid, unsigned char *peer_mac)
{
	int i;

	
	for(i=0; i<MAX_PPPOE_ENTRY; i++)
	{
		if(fast_pppoe_info[i].valid==0)
		{
			continue;
		}
		
		if((fast_pppoe_info[i].sid==sid) && (memcmp(fast_pppoe_info[i].peer_mac,peer_mac,6)==0))
		{
			return &fast_pppoe_info[i];
		}

	}
	return NULL;
}




static struct pppoe_info* find_pppoe_info(char *ppp_dev, char *wan_dev, unsigned short sid,
												unsigned int our_ip,unsigned int	peer_ip,
												unsigned char * our_mac, unsigned char *peer_mac)
{
	int i;

	
	for(i=0; i<MAX_PPPOE_ENTRY; i++)
	{
		if(fast_pppoe_info[i].valid==0)
		{
			continue;
		}
		
		if((ppp_dev!=NULL) && (strcmp(fast_pppoe_info[i].ppp_dev,ppp_dev)==0))
		{
			return &fast_pppoe_info[i];
		}

		if((wan_dev!=NULL) && (strcmp(fast_pppoe_info[i].wan_dev,wan_dev)==0))
		{
			return &fast_pppoe_info[i];
		}
		
		
		if((peer_mac!=NULL) && (memcmp(fast_pppoe_info[i].peer_mac,peer_mac,6)==0))
		{
			return &fast_pppoe_info[i];
		}

		if((our_mac!=NULL) && (memcmp(fast_pppoe_info[i].our_mac,our_mac,6)==0))
		{
			return &fast_pppoe_info[i];
		}
		
		
		if((peer_ip!=0) && (fast_pppoe_info[i].peer_ip==peer_ip))
		{
			return &fast_pppoe_info[i];
		}
		
		if((our_ip!=0) && (fast_pppoe_info[i].our_ip==our_ip))
		{
			return &fast_pppoe_info[i];
		}
				

		if((sid!=0) && (fast_pppoe_info[i].sid==sid))
		{
			return &fast_pppoe_info[i];
		}

	}
	return NULL;
}

static struct pppoe_info* alloc_pppoe_info(void)
{
	int i;
	for(i=0; i<MAX_PPPOE_ENTRY; i++)
	{
		if(fast_pppoe_info[i].valid==0)
		{
			return &fast_pppoe_info[i];
		}

	}

	memset(fast_pppoe_info, 0 , sizeof(fast_pppoe_info));

	return &fast_pppoe_info[0];
	
}

int set_pppoe_info(char *ppp_dev, char *wan_dev, unsigned short sid,
							unsigned int our_ip,unsigned int	peer_ip,
							unsigned char * our_mac, unsigned char *peer_mac)
{
	struct pppoe_info* pppoe_info_ptr;
	
	pppoe_info_ptr=find_pppoe_info(ppp_dev, NULL, 0, our_ip, peer_ip,our_mac, peer_mac);
	
	if(pppoe_info_ptr==NULL)
	{
		pppoe_info_ptr=alloc_pppoe_info();
		if(pppoe_info_ptr == NULL)
		{
			return 0;
		}
	}

	if(sid!=0)
	{
		pppoe_info_ptr->sid=sid;
	}
	
	if(our_ip!=0)
	{
		pppoe_info_ptr->our_ip=our_ip;
	}


	if(peer_ip!=0)
	{
		pppoe_info_ptr->peer_ip=peer_ip;
	}
			
	if(ppp_dev!=NULL)
	{
		strcpy(pppoe_info_ptr->ppp_dev,ppp_dev);
	}

	if(wan_dev!=NULL)
	{
		strcpy(pppoe_info_ptr->wan_dev,wan_dev);
	}
	
	
	if(peer_mac!=NULL)
	{
		memcpy(pppoe_info_ptr->peer_mac,peer_mac,6);
	}

	if(our_mac!=NULL)
	{
		memcpy(pppoe_info_ptr->our_mac,our_mac,6);
	}
	
	pppoe_info_ptr->valid=1;
	
	return 0;
	
}

unsigned long get_pppoe_last_rx_tx(char *ppp_dev, char *wan_dev, unsigned short sid,
								unsigned int our_ip,unsigned int	peer_ip,
								unsigned char * our_mac, unsigned char *peer_mac,
								unsigned long *last_rx, unsigned long *last_tx)
{
	struct pppoe_info* pppoe_info_ptr;
	
	pppoe_info_ptr=find_pppoe_info(ppp_dev, wan_dev, sid, our_ip, peer_ip,our_mac, peer_mac);
	if(pppoe_info_ptr!=NULL)
	{
		*last_rx=pppoe_info_ptr->last_rx;
		*last_tx=pppoe_info_ptr->last_tx;
	}
	return 0;
}


int check_and_pull_pppoe_hdr(struct sk_buff *skb)
{
	unsigned char *mac_hdr=NULL;
	unsigned short sid;
	struct pppoe_info* pppoe_info_ptr=NULL;
	struct net_device	*ppp_dev=NULL;

	
	if(fast_pppoe_fw==0)
	{
		return 0;
	}

	rtl_set_skb_pppoe_flag(skb,0);
	//printk("%s:%d,%s\n",__FUNCTION__,__LINE__,skb->dev->name);
	mac_hdr=rtl_skb_mac_header(skb);
	if(mac_hdr==NULL)
	{
		return 0;
	}
	
	if( (*(unsigned short *)(&mac_hdr[12])!=ETH_P_PPP_SES) \
		||(*(unsigned short *)(&mac_hdr[20])!=0x0021))
	{
		return 0;
	}
	

	/*shouldn't use sid to find pppoe info here*/
	sid=*(unsigned short *)(&mac_hdr[16]);
	
	pppoe_info_ptr=find_pppoe_info_fast(sid,&mac_hdr[6]);
	if(pppoe_info_ptr==NULL)
	{
		return 0;
	}


	ppp_dev=rtl_get_dev_by_name(pppoe_info_ptr->ppp_dev);
	if(ppp_dev==NULL)
	{
		return 0;
	}
	

	rtl_set_skb_network_header(skb,mac_hdr+22);	
	rtl_set_skb_transport_header(skb,mac_hdr+22);
	rtl_skb_pull(skb,8);
	rtl_set_skb_rx_dev(skb,rtl_get_skb_dev(skb));
	rtl_set_skb_protocol(skb,ETH_P_IP);
	rtl_set_skb_dev(skb,ppp_dev);
	rtl_get_skb_dev(skb)->stats.rx_packets++;
	rtl_get_skb_dev(skb)->stats.rx_bytes += rtl_get_skb_len(skb);
	rtl_set_skb_pppoe_flag(skb,1);
	pppoe_info_ptr->last_rx=jiffies;
	
	
#if defined(CONFIG_RTL_FAST_PPPOE_DEBUG)
	pppoe_info_ptr->total_rx++;
#endif

	return 1;

}


void check_and_restore_pppoe_hdr(struct sk_buff *skb)
{	
#if defined(CONFIG_RTL_FAST_PPPOE_DEBUG)
	struct pppoe_info* pppoe_info_ptr=NULL;
	unsigned char *mac_hdr=NULL;
#endif

	if(fast_pppoe_fw==0)
	{
		return;
	}
	
	if(rtl_get_skb_pppoe_flag(skb))
	{
		rtl_skb_push(skb,8);
		rtl_set_skb_network_header(skb,rtl_get_skb_data(skb));
		rtl_set_skb_transport_header(skb, rtl_get_skb_data(skb));
		
		rtl_get_skb_dev(skb)->stats.rx_packets--;
		rtl_get_skb_dev(skb)->stats.rx_bytes -= rtl_get_skb_len(skb);
		rtl_set_skb_dev(skb,rtl_get_skb_rx_dev(skb));
		rtl_set_skb_protocol(skb,ETH_P_PPP_SES);
		rtl_set_skb_pppoe_flag(skb,0);
		#if defined(CONFIG_RTL_FAST_PPPOE_DEBUG)
		mac_hdr=rtl_skb_mac_header(skb);
		pppoe_info_ptr=find_pppoe_info(NULL,NULL,0,0,0,NULL,&mac_hdr[6]);
		if(pppoe_info_ptr)
		{
			pppoe_info_ptr->total_rx--;
		}
		#endif
	}
	
}

int fast_pppoe_xmit(struct sk_buff *skb)
{
	struct pppoe_info *pppoe_info_ptr;
	struct pppoe_hdr *ph;
	int data_len;
	struct net_device *ppp_dev;;
	struct net_device *wan_dev;;


	if(fast_pppoe_fw ==0) 
	{
		return 0;
	}

#if defined(CONFIG_NET_SCHED)
    if (gQosEnabled) 
    {
         return 0;
    }
#endif
	if(rtl_get_skb_dev(skb) ==NULL)
	{	
		return 0;
	}
	
	if(strncmp(rtl_get_skb_dev(skb)->name, "ppp",3)!=0)
	{
		return 0;
	}

	if((rtl_skb_headroom(skb)<22) || rtl_skb_cloned(skb) || rtl_skb_shared(skb))
	{
		return 0;
	}

	pppoe_info_ptr=find_pppoe_info(rtl_get_skb_dev(skb), NULL, 0, 0, 0,NULL, NULL);
	
	if(pppoe_info_ptr==NULL)
	{
		return 0;
	}

	if(	(pppoe_info_ptr->ppp_dev[0]==0) || 
		(pppoe_info_ptr->wan_dev[0]==0) ||
		(pppoe_info_ptr->sid==0)	)
	{
		return 0;
	}
	
	//printk("ppp_dev is %s,wan_dev is %s,sid is %d\n", pppoe_info_ptr->ppp_dev, pppoe_info_ptr->wan_dev, pppoe_info_ptr->sid);
	ppp_dev=rtl_get_skb_dev(skb);

	data_len= rtl_get_skb_len(skb);

	wan_dev=rtl_get_dev_by_name(pppoe_info_ptr->wan_dev);
	if(wan_dev==NULL)
	{
		return 0;
	}
	
	rtl_skb_push(skb, 2);
	*(unsigned short *)rtl_get_skb_data(skb)=0x0021;
	
	rtl_skb_push(skb, sizeof(struct pppoe_hdr));
	ph =(struct pppoe_hdr *)rtl_get_skb_data(skb);
	ph->ver = 1;
	ph->type = 1;
	ph->code = 0;
	ph->sid = pppoe_info_ptr->sid;
	ph->length = data_len+2;
	rtl_set_skb_dev(skb,wan_dev);


	rtl_skb_push(skb, 2);
	*(unsigned short *)(rtl_get_skb_data(skb))=0x8864;

	rtl_skb_push(skb, 12);
	
	memcpy(rtl_get_skb_data(skb),pppoe_info_ptr->peer_mac,6);
	memcpy(rtl_get_skb_data(skb)+6,wan_dev->dev_addr,6);
	
	ip_finish_output4(skb);
	pppoe_info_ptr->last_tx= jiffies;
#if defined(CONFIG_RTL_FAST_PPPOE_DEBUG)
	pppoe_info_ptr->total_tx++;
#endif

	return 1;
	
}

#endif

