/**
   @file H-файл шаблона, заменить на описание к какому девайсу
 */

/** TEMPLATE  в данном случае заменить на название модуля*/
#ifndef TEMPLATE_H
#define TEMPLATE_H


#include <jansson.h>

/** Физический wan*/
#define WAN
/** Физический lan(если есть)*/
#define LAN

/** Путь к mtd, где хранится конфиг*/
#define MTD_CONFIG   "/dev/"
/** Путь к mtd, где хранится mac*/
#define MTD_MAC      "/dev/"

/**
   @brief Возвращет базовый мак(устанавливается на LAN)
   @param Указатель на строку
 */
void get_base_mac(char *base_mac);
/**
   @brief Возвращает мак на WAN
   @param Указатель на строку
 */
void get_wan_mac(char *wan_mac);

/**
   @brief Функция выполняет переименование и поднятие интерфейсов
   @param Конфиг устройства
 */
void preinit(json_t *config);

#endif //TEMPLATE_H
