#ifndef _CONFIG_H_
#define _CONFIG_H_


/********************************************************************/
/* File name: config.h                                              */
/********************************************************************/


#define ONCE_SO_LOAD // .so подгружаются однажды, и больше не выгружаются и не подгражаются на протяжении всей работы.

#define BUFLEN 4096

// Пути к файлам
#define DEFAULT_PATH_CONF CONF_PREF "usr/etc/default"
#define FILE_ROUTING CONF_PREF "etc/default/routing"

#define FILE_FIREWALL_MASQ_SERV CONF_PREF "etc/default/nat_modules"
#define FILE_FIREWALL_MASQ_SERV_AVAIL CONF_PREF "var/www/nat_modules_avail"
#define FILE_FIREWALL_MASQ CONF_PREF "etc/default/masq"
#define FILE_FIREWALL_RULES CONF_PREF "etc/default/rules"
#define FILE_FIREWALL_VSERVERS_CONF CONF_PREF "var/www/vservers.conf"
#define FILE_FIREWALL_VSERVERS CONF_PREF "etc/default/vservers"

#define FILE_LANWANEXT "/tmp/lanwanext.conf"
#define FILE_BOARD_CONF CONF_PREF "usr/etc/board.conf"

#define FILE_DDNS_CONF CONF_PREF "etc/default/ddnsd"

#define FILE_HTTPD_ACCESS CONF_PREF "etc/default/httpd_access.conf"

#define FILE_CONFIG_WAN CONF_PREF "etc/default/wan"
#define FILE_TEMPLATE_WAN CONF_PREF "var/www/wan_template"

#define FILE_CONFIG_WIRELESS CONF_PREF "etc/default/wireless"
#define FILE_CONFIG_ADSL CONF_PREF "etc/default/adsl"
#define FILE_CONFIG_LAN CONF_PREF "etc/default/netbase"
#define FILE_CONFIG_DHCP_MOPED CONF_PREF "etc/default/dhcpd"

#define FILE_BRFILTERS_POLICY CONF_PREF "etc/default/brf_policy"
#define FILE_BRFILTERS_RULES  CONF_PREF "etc/default/brf_rules"
#define FILE_IFGROUPS_POLICY CONF_PREF "etc/default/ifgr_policy"
#define FILE_IFGROUPS_RULES  CONF_PREF "etc/default/ifgr_rules"
#define FILE_DMZ  CONF_PREF "etc/default/dmz"
#define FILE_3G  CONF_PREF "etc/default/3g"
#define FILE_CONFIG_IGMP CONF_PREF "etc/default/igmp"
#define FILE_CONFIG_UPNP CONF_PREF "etc/default/upnp"
#define FILE_G3_PPP_SETUP CONF_PREF "etc/default/ppp-peers"
#define FILE_G3_PPP_SETUP_TEMPLATE CONF_PREF "etc/default/g3_ppp_template"
#define FILE_3G_STATUS CONF_PREF "tmp/.3g_usb_status"



// ######## скрипты ##########################################
#define DSL_WIRELESS_CHANNELS CONF_PREF "bin/wlctl channels 2> /dev/null"
#define DSL_WIRELESS_INIT CONF_PREF "etc/init.d/S60wireless restart > /dev/null 2>&1"
#define DSL_WIRELESS_COUNRYLIST CONF_PREF "bin/wlctl country list 2> /dev/null"
#define DSL_WIRELESS_GET_BSSID CONF_PREF "bin/wlctl bssid 2> /dev/null"


// Системные утилиты и параметры
#define SAVECONFIG CONF_PREF "usr/sbin/saveconfig"
#define SAVE_AND_REBOOT CONF_PREF "usr/sbin/save_and_reboot"
#define RESET_AND_REBOOT CONF_PREF "usr/sbin/reset_and_reboot"
#define RESETCONFIG CONF_PREF "usr/sbin/resetconfig"
#define PPP_STATE "/var/run/pptpc-localstate"
/********************************************************************/
/* End of file                                                      */
/********************************************************************/

#endif
