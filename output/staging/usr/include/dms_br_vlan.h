#ifndef _DMS_BR_VLAN_H
#define _DMS_BR_VLAN_H

int bridge_manip(int action, char *name);
int vlan_manip(int action, char *iface, int vid, int nm_type, int skb, int qos);
int bridge_ifaces_manip(int action, char *brname, char *ifname);
int get_br_ifaces_list(const char *name, char *ifaces_list, int list_len);

#endif
