#ifndef CONF_PPTPD_H
#define CONF_PPTPD_H

#ifdef PPTPD
int apply_pptpd(json_t *config);
struct outgoin *conf_pptpd(char *buf, int id);
#endif

#endif
