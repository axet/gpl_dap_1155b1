#ifndef _BRCM_H_
#define _BRCM_H_

#include "jansson.h"

#define atmctlName         "xtmctl"
#define atmVccString       atmctlName " operate conn --"
#define atmTdteString      atmctlName " operate tdte --"

/* Values for dual latency port id */
#define PHY0_PATH0                     0
#define PHY0_PATH1                     1
#define PHY1_PATH0                     2
#define PHY1_PATH1                     3
#define PHY0_PATH0_PATH1               4
#define PHY1_PATH0_PATH1               5

#define PORTID_TO_PORTMASK(PORTID)     (UINT32)                             \
    (((PORTID) == PHY0_PATH0) ? PORT_PHY0_PATH0                           : \
     ((PORTID) == PHY0_PATH1) ? PORT_PHY0_PATH1                           : \
     ((PORTID) == PHY1_PATH0) ? PORT_PHY1_PATH0                           : \
     ((PORTID) == PHY1_PATH1) ? PORT_PHY1_PATH1                           : \
     ((PORTID) == PHY0_PATH0_PATH1) ? (PORT_PHY0_PATH0 | PORT_PHY0_PATH1) : \
     ((PORTID) == PHY1_PATH0_PATH1) ? (PORT_PHY1_PATH0 | PORT_PHY1_PATH1) : PORT_PHY0_PATH0)
     
#define NR_RX_BDS_MAX           400
#define NR_RX_BDS(x)            NR_RX_BDS_MAX
#define NR_TX_BDS               180
#define NVRAM_PSI_DEFAULT 24
void init_pre_boot(void);
void init_post_boot(void);
void deinit_adsl(void);
unsigned long int getTrffDscrIndex(json_t *iface_l2);
void ctlTrffDscrConfig(json_t *iface_l2);
unsigned int get_line_state(void);
#endif
