#ifndef _CONF_BRCMIFACE_H_
#define _CONF_BRCMIFACE_H_
#ifdef BRCM
struct outgoin*  conf_ifgroups(char* buf,int id);
struct outgoin*  conf_brcm_eth(char* buf,int id);
#endif
#endif
