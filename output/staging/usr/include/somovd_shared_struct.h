/*! \file somovd_shared_struct.h
    \brief Хедер со структурами, используемыми как в демоне, так и в морде.
       
*/

#ifndef _SOMOVD_SHARED_STRUCT_H_
#define _SOMOVD_SHARED_STRUCT_H_

#ifdef SOMOVD
#if 0
struct adsl_incoming{
	const char* gdmt; 
	const char* glite;
	const char* t1413;
	const char* adsl2;
	const char* annexl;
	const char* adsl2p;
	const char* annexm;
	const char* pair;
	const char* bitswap;
	const char* sra;
	const char* testmod;
};
#endif
struct brfiltres_incoming{
	char* protocol;
	char* src_mac;
	char* dst_mac;
	char* direction;
	char* ifaces;
};

struct ntp_incoming{
	char* enable;
	char* server_list;
	char* tz_offset; // в формате смещения времени
};

struct ddns_incoming{
	const char* service;
	const char* host_name;
	const char* usr_name;
	const char* pass;
	const char* iface;
};

struct dhcp_incoming{
	const char* dhcp_en;
	const char* dhcp_start;
	const char* dhcp_end;
	const char* dhcp_time;
	const char* dhcp_relay;
	const char* dhcp_dns1;
	const char* dhcp_dns2;
};

struct ifgroups_incoming{
	char* name;
	char* ifaces;
	char* wanifaces;
	char* dhcpid;
};

struct lan_incoming{
	const char* ip;	
	const char* mask;
	const char* alias_en;	
	const char* alias_ip;
	const char* alias_mask;	
};

struct dmz_incoming{
	const char* enabled;	
	const char* ip;
};

struct sdhcp_incoming{
	char* mac;	
	char* ip;
};

/*! \brief Структура tr69_config 
 * 
 */
struct tr69_config{
	char* tr69cDebugEnable;   /**< [yes|no] */
	char* tr69cNoneConnReqAuth;	/**< [yes|no] */
	char* tr69cInformEnable;	/**< [yes|no] */
	char* tr69cInformInterval;
	char* tr69cAcsURL;
	char* tr69cAcsUser;
	char* tr69cAcsPwd;
	char* tr69cConnReqUser;
	char* tr69cConnReqURL;
	char* tr69cConnReqPwd;
	char* tr69cBoundIfName;

};

/*! \brief Структура urlf_config 
 * 
 */
struct urlf_config{
	char* enable;   /**< [yes|no] */
	char* type;		/**< [Exclude|Include] */
};

/*! \brief Структура urllist 
 * 
 */
struct urllist{
	char* enable; 	/**< [yes|no] */
	char* address;  /**< урл */
	char* port;		/**< [1-65535] */
};


/*! \brief Структура dns_struct для работы с ДНС и доменами поиска.
 * 
 */
struct dns_struct{
	char* server;	/**< IP ДНС серверов, разделенные '\n'  */
	char* domen;	/**< Домены поиска. разделенные '\n' */
	char* iface;	/**< Задел на будущее - возможность указать интерфейс, с которого брать ДНС */
};


/*! \brief Структура vservers_incoming для работы с виртуальными серверами.
 *
 * 
 */
struct vservers_incoming{
	char* src_zone_id; 		/**< Id зоны. Используется только в расширенном фаере (DSA) */
	char* iface;			/**< Имя интерфейса в системе, на котором должны срабатывать правила. [ppp1, eth0, atm0] */
	char* protocol;			/**< Протокол. [tcp,udp,both] */
	char* fw_port;			/**<  Внешний порт [1-65535] (начальный) */
	char* fw_portE;			/**<  Внешний порт [1-65535] (конечный) */
	char* dst_port;		/**<  Внутренний порт [1-65535] (начальный) */
	char* dst_portE;		/**<  Внутренний порт [1-65535] (конечный) */
	char* dst_ip;			/**<  IP-адрес, на который все будет перенаправляться.[]*/
	char* name;				/**<  Имя всервера, произвольная строка, определяемая пользователем.*/
	char* iface_name;
};

/*! \brief Структура AvailServices.
 *
 *  Используется для работы со списком доступных сервисов (L3 интерфейсов)
 */
struct AvailServices{
	char* name; 			/**< Имя сервиса, произвольная строка, определяемая пользователем. */
	char* iface;			/**< Имя интерфейса в системе. [ppp1, eth0, atm0] */
	char* type;				/**< Тип. [ipoe,ipoa,pppoe,pppoa,mer] */
};

/*! \brief Структура SyslogCfg.
 *
 *  Используется для настройки удаленного сислога
 */

struct SyslogCfg{
	char* enable;				/**< Вкл/выкл [0-Выкл,1-Вкл.] */
	char* option;				/**< Тип логирования [1-Локальный,2-Удаленный,3-Локальный и удаленный. ]*/
	char* localLogLevel;		/**< Локальный уровень логирования [0-EMERGENCY,1-ALERT,2-CRITICAL,3-ERROR,4-WARNING,5-NOTICE,6-INFORMATIONAL,7-DEBUG] */
	char* remoteLogLevel;		/**< Удаленный уровень логирования [0-EMERGENCY,1-ALERT,2-CRITICAL,3-ERROR,4-WARNING,5-NOTICE,6-INFORMATIONAL,7-DEBUG] */
	char* ServerIp;				/**< Адрес сервера */
	char* ServerPort;			/**< Порт */
};

/*! \brief Структура SyslogDump.
 *
 *  Используется для получения сислога
 */
struct SyslogDump{
	char* localDisplayLevel; 	/**< Локальный отображаемый уровень [пока не придумал] */	
	char* logData; 				/**< Данные */
};

struct wan_incoming{
	char* name; //connection name
	char* type; //connection type
	// PVC settings
	char* vpi;
	char* vci;
	char* qos; //ATM QoS
	char* pcr; 
	char* scr;
	char* mbs;
	// Misc
	char* enable;
	char* nat; //NAT yes|no
	char* igmp; //IGMP yes|no
	char* rip; //RIP yes|no
	char* fw; //Firewall yes|no
	char* encap; //Encapsulation Mode
	char* service; //Service name (IP TV, Internet and so on...)
	// 802.1q support
	char* vid; //VLAN ID
	// Default gateway (MER, PPPoX, IPoA)
	char* gwip; //default gateway ip. only for PPPoX and MER
	char* gwif; //default gateway iface. only for PPPoX and MER
	// WAN IP address (MER, IPoA)
	char* staticIP; /**< Статический IP [yes|no] */
	char* wan_ip; //WAN IP address
	char* wan_mask; //WAN subnet mask
	// DNS servers (IPoA, MER)
	char* prim_dns; //Primary DNS IP
	char* sec_dns; //Secondary DNS IP
	// PPP settings
	char* ppp_login; /**< Имя пользователя */
	char* ppp_passw; /**< Пароль */
	char* ppp_auth; /**< Тип авторизации [0-AUTO|1-PAP|2-CHAP|3-MSCHAP] */
	char* onDemand;	/**< Соединение по требованию [yes|no] */
	char* idle_timeout; /**< Время простоя [в секундах] */
	char* mtu; //MTU Size
	char* ppp_ip_ext; //PPP IP extension
	char* keep_alive; //Keep alive
	char* ppp_static_ip; //Static IP Address
	char* bridge_frames;
	char* lcp_interval;
	char* lcp_fails;
	char* ppp_debug; //PPP debug
	char* iface; //не храниться в конфиге, а вычисляется позже;
	char* vpriority; //Приоритет Vlan (добавлено для новой морды )
	char* dhcpcOp60VID; //для петерстара
#ifdef DMP_X_BROADCOM_COM_IPV6_1 /* aka SUPPORT_IPV6 */
	char* enable6;
	char* addr6;
	char* addr6gateway;
	char* mld6;
	char* unnummod6;
	char* dns6_prim;
	char* dns6_sec;
#endif
};

struct etherwan_conf{
	char* list_ifaces; // список доступных ифейсов для разворота
	char* name_ethwan_now; // имя развернутого сейчас
	char* name_ethwan_previos; //имя предыдущего
	char* connMode;
};

struct rules_incoming{
	char* act;
	char* z_d;
	char* z_s;
	char* src_ip;
	char* dst_ip;
	char* proto;
	char* port_stc;
	char* port_dst;
	char* log;
	char* iface;
};

struct masq_incoming{
	char* iface;
	char* ip;
	char* mask;
	char* proto;
	char* port;
};

struct httpaccess_incoming{
	char* protocol;
	char* ifaces;
	char* ip;
	char* mask;
	char* act;
};

struct routing_incoming{
	char* ip;
	char* mask;
	char* gw;
	char* met;
	char* ifaces;
};

struct adsl_status_struct{
	char* adslTrainingState; //1
	//если dsl не установилось, то все остальное NULL
	char* dslDownstreamRate; //2;
	char* dslUpstreamRate; //3
	char* pwrState; //4
	char* trellisString;//7
	char* modulationStr;//8
	char* modulationStr2;//9
	char* lineStatus;//10
	char* adslAtucPhysRate;//11
	char* adslPhysRate;//12
	char* adslPhysMgn;//13
	char* adslAtucPhysMgn;//14
	char* adslPhysAtn;//15
	char* adslAtucPhysAtn;//16
	char* adslAtucPhysPwr;//17
	char* adslPhysPwr;//18
	//?
	char* IsXdsl2modType;//19
	//gdmt
	char* rK; //5
	char* xK;//6
	char* adslRxNonStdFramingAdjustK;//20
	char* rcvInfoR;//21
	char* xmtInfoR;//22
	char* rcvInfoS;//23
	char* xmtInfoS;//24
	char* rcvInfoD;//25
	char* xmtInfoD;//26
	//adsl
	char* rcv2InfoMSGc;//27
	char* xmt2InfoMSGc;//28
	char* rcv2InfoB;//29
	char* xmt2InfoB;//30
	char* rcv2InfoM;//31
	char* xmt2InfoM;//32
	char* rcv2InfoT;//33
	char* xmt2InfoT;//34
	char* rcv2InfoR;//35
	char* xmt2InfoR;//36
	char* floatXren1;//37
	char* floatXren2;//38
	char* rcv2InfoL;//39
	char* xmt2InfoL;//40
	char* rcv2InfoD;//41
	char* xmt2InfoD;//42
	//cou
	char* rcvStatcntSF;//43
	char* xmtStatcntSF;//44
	char* rcvStatcntSFErr;//45
	char* xmtStatcntSFErr;//46
	char* rcvStatcntRS;//47
	char* xmtStatcntRS;//48
	char* rcvStatcntRSCor;//49
	char* xmtStatcntRSCor;//50
	char* rcvStatcntRSUncor;//51
	char* xmtStatcntRSUncor;//52
	//spec
	//?
	char* fireStatstatus;//53
	char* reXmtRSCodewordsRcved;//54
	char* reXmtRSCodewordsRcvedUS;//55
	char* reXmtUncorrectedRSCodewords;//56
	char* reXmtUncorrectedRSCodewordsUS;//57
	//xz
	char* rcvStatcntHEC;//58
	char* xmtStatcntHEC;//59
	char* rcvStatcntOCD;//60
	char* xmtStatcntOCD;//61
	char* rcvStatcntLCD;//62
	char* xmtStatcntLCD;//63
	char* rcvStatcntCellTotal;//64
	char* xmtStatcntCellTotal;//65
	char* rcvStatcntCellData;//66
	char* xmtStatcntCellData;//67
	char* rcvStatcntCellDrop;//68
	char* rcvStatcntBitErrs;//69
	char* xmtStatcntBitErrs;//70
	char* perfTotaladslESs;//71
	char* adslESs;//72
	char* perfTotaladslSES;//73
	char* adslSES;//74
	char* perfTotaladslUAS;//75
	char* adslUAS;//76
	char* adslAS;//77
	char* floatXren3;//78
	char* floatXren4;//79
	char* floatXren5;//80
	char* floatXren6;//81
	char* floatXren7;//82
	char* floatXren8;//83
	char* floatXren9;//84
	char* floatXren10;//85
	char* bitswapStatrcvCnt;//86
	char* bitswapStatxmtCnt;//87
};

struct wan_status_struct{
	char* name;
	char* type;
	char* iface;
	char* vpi;
	char* vci;
	char* categ;
	char* en;
	char* status;
	char* ip;	
};

#ifdef CONFIG_ID_G3
#define G3_OPEN_RESULT_SUCCESS "success"
#define G3_OPEN_RESULT_UNKNOWN_DEVICE "unknown_device" //неподдерживаемое устройство
#define G3_OPEN_RESULT_INIT "init" //находится в процессе инициализациии
#define G3_OPEN_RESULT_ERROR "error" //че-то не так с девайсом
#define G3_OPEN_RESULT_SIM_PIN "sim_pin"     //устройство ожидает ввода PIN
#define G3_OPEN_RESULT_SIM_PIN2 "sim_pin2"     //устройство ожидает ввода PIN2
#define G3_OPEN_RESULT_SIM_PUK "sim_puk"     //устройство ожидает ввода PUK
#define G3_OPEN_RESULT_SIM_PUK2 "sim_puk2"     //устройство ожидает ввода PUK2
struct g3_incoming{
	char* g3_enable;    //yes/no
	char* g3_mode;      //"3G 2G"/"2G 3G"/"2G"/"3G"
	char* g3_actual_mode; // 2G/3G
	char* g3_operator;
	char* vendor;
	char* model;
	char* revision;
	char* imsi;
	char* imei;
	char* charged;       //battery
	char* raw_level;     //battery
	char* percent;       //signal level in percents (0..100) or NULL if device said "dont know"
	char* dbm;           //signal level in dBm (-113..-51) or NULL if device said "dont know"
	char* rssi;          //raw value from device (0..31) or NULL if see prev
	char* ber;           //channel bit error rate (0..7) or NULL if see prev
	char* mode;          //always present in device answer while AT_OK. What is it? A hz!!! Operartor related.
	char* format;        //filled by NULL so far. comment in 3g.h: can absent, then filled with -1. What is it? A hz!!! Operartor related.
	char* oper;          //string description of mobile operator
	char* pppup;         //ppp connection through 3g modem:  yes/connecting and NULL if down
	char* open_result;  //result of device openning: success/unknown_device/NULL
	char* pinoff;
	char* pin_left;
	char* puk_total;
	char* puk_left;
	char* puk2_total;
	char* puk2_left;
	char* cpms_used;
	char* cpms_total;
	char* new_sms;
	char* dongle_type;   // GSM/CDMA
	char* device_available; //признак присунутого донгла "yes"/"no"
};
#endif

#ifdef CONFIG_ID_G3_SMS
#define G3_SMS_STATUS_OUTGOING "outgoing"
#define G3_SMS_STATUS_DRAFT "draft"
#define G3_SMS_STATUS_INCOMING "incoming"
struct g3_sms_incoming{
	char* phone;
	char* message;
	char* status; //G3_SMS_STATUS_OUTGOING/G3_SMS_STATUS_DRAFT/G3_SMS_STATUS_INCOMING
};
#endif

#ifdef CONFIG_ID_G3_SET_PIN
struct g3_pin_incoming{
	char* pin;
	char* newpin;
	char* pinoff;
};
#endif

#if defined CONFIG_ID_G3_PHONEBOOK_DELETE || CONFIG_ID_G3_PHONEBOOK_WRITE
struct g3_phonebook_incoming{
	char* index;
	char* number;
	char* type;
	char* text;
};
#endif

#ifdef CONFIG_ID_ROUTE_TABLE
struct route_record{
	char* iface;
	char* dest;
	char* gw;
	char* flags;
	char* ref;
	char* use;
	char* mertic;
	char* mask;
	char* mtu;
	char* win;
	char* irtt;
};
#endif

struct dhcp_leases_incoming{
	char* mac;
	char* ip;
	char* exp;
};

struct wifi_incoming{
	char* en;
	char* hid;
	char* ssid;
	char* mbssid;
	char* mbssid_cur;
	char* country;
	char* bssid;
	char* bssid2;
	char* bssid3;
	char* bssid4;
	char* enc;
	char* keydef;
	char* key;
	char* key1;
	char* key2;
	char* key3;
	char* aut;
	char* chan;
	char* radip;
	char* radport;
	char* radkey;
	char* pskkey;
	char* wpapre;
	char* wpaenc;
	char* wparen;
	char* flmode;
	char* flmac;
	char* wlmode;
	char* maxsta;
	char* stkpal;
	char* beaconprd;
	char* rtsthreshld;
	char* fragthreshld;
	char* dtimprd;
	char* txpwr;
	char* bgprtct;
	char* bandw;
	char* txprmb;
	char* wps_en;
	char* wps_method;
	char* wps_dev_pin;
	char* wps_pin;
	char* wps_conf; 
	char* wps_unconf;
	char* wps_ssid;
	char* wps_auth;
	char* wps_encr;
	char* wdsmode;
	char* wdspmode;
	char* wdsmac1;
	char* wdsmac2;
	char* wdsmac3;
	char* wdsmac4;
	char* wdsenc;
	char* wdskey;
	char* clien;
	char* clissid;
	char* clibssid;
	char* cliauth;
	char* cliwep;
	char* cliencr;
	char* clidef;
	char* clikey;
	char* clikey1;
	char* clikey2;
	char* clikey3;
	char* enable2; /**< Включить гостевой ссид [1|NULL] */
	char* ssid2; /**< Название гостевого ссида [char] */
	char* cliso;
};

struct wifi_wmm{
	char* wmmen;
      char* ap_aifsn_bk;
      char* ap_aifsn_be;
      char* ap_aifsn_vi;
      char* ap_aifsn_vo;
      char* ap_cwmin_bk;
      char* ap_cwmin_be;
      char* ap_cwmin_vi;
      char* ap_cwmin_vo;
      char* ap_cwmax_bk;
      char* ap_cwmax_be;
      char* ap_cwmax_vi;
      char* ap_cwmax_vo;
      char* ap_txop_bk;
      char* ap_txop_be;
      char* ap_txop_vi;
      char* ap_txop_vo;
      char* ap_acm_bk;
      char* ap_acm_be;
      char* ap_acm_vi;
      char* ap_acm_vo;
      char* ackplc_bk;
      char* ackplc_be;
      char* ackplc_vi;
      char* ackplc_vo;
      char* bss_aifsn_bk;
      char* bss_aifsn_be;
      char* bss_aifsn_vi;
      char* bss_aifsn_vo;
      char* bss_cwmin_bk;
      char* bss_cwmin_be;
      char* bss_cwmin_vi;
      char* bss_cwmin_vo;
      char* bss_cwmax_bk;
      char* bss_cwmax_be;
      char* bss_cwmax_vi;
      char* bss_cwmax_vo;
      char* bss_txop_bk;
      char* bss_txop_be;
      char* bss_txop_vi;
      char* bss_txop_vo;
      char* bss_acm_bk;
      char* bss_acm_be;
      char* bss_acm_vi;
      char* bss_acm_vo;
};

struct wifi_station{
	char* mac;
// for BCM
	char* assoc;
	char* auth;
	char* ssid;
	char* iface;
// for DIR
	char* aid;
	char* psm;
	char* mimops;
	char* mcs;
	char* bw;
	char *sgi;
	char *stbc;
};

//**** flags for saving ppp parameters ****
struct ppp_peers{
	char* peer_name;
	char* type;
	char* auto_start;
	char* options;
};

//**** flags for saving g3 ppp options ****
#define G3_PPP_FLAG_PEER_NAME 0x0000000000000001LL
#define G3_PPP_FLAG_TYPE 0x0000000000000002LL
#define G3_PPP_FLAG_AUTO_START 0x0000000000000004LL
#define G3_PPP_FLAG_USERNAME 0x0000000000000008LL
#define G3_PPP_FLAG_PASSWORD 0x0000000000000010LL
#define G3_PPP_FLAG_DIAL_NUMBER 0x0000000000000020LL
#define G3_PPP_FLAG_AUTH 0x0000000000000040LL
#define G3_PPP_FLAG_APN 0x0000000000000080LL
#define G3_PPP_FLAG_RECONNECT_MODE 0x0000000000000100LL
#define G3_PPP_FLAG_MAX_IDLE_TIMEOUT 0x0000000000000200LL
#define G3_PPP_FLAG_MTU 0x0000000000000400LL
#define G3_PPP_FLAG_MRU 0x0000000000000800LL
#define G3_PPP_FLAG_METRIC 0x0000000000001000LL
struct g3_ppp_incoming{
	char* flags;
	char* peer_name;
	char* type;
	char* auto_start;
	char* username;
	char* password;
	char* dial_number;
	char* auth;
	char* apn;
	char* reconnect_mode;
	char* max_idle_timeout;
	char* mtu;
	char* mru;
	char* metric;
};

struct raw_data_collector{
	int clean;
	int size;
	void* buffer;
};

struct arp_record{
	char *ip; //IP address
//	char *type; //HW type
//	char *flags; //Flags
	char *mac; //MAC address
//	char *mask; //Mask
	char *name; // Name interface
};

struct device_version{
	char *name;
	char *version;
	char *buildtime;
	char *vendor;
	char *bugs;
	char *summary;
	char *boardid; // if supported
	char *lang;
};

struct wimax_record{
	char* enable_wimax;
	char* ssid;
	char* diod_off;
	char* wan_iface;
	char* info_updated_ch;
	char* proto_flags_ch;
	char* chip_ch;
	char* firmware_ch;
	char* mac_ch;
	char* link_status_ch;
	char* rssi_ch;
	char* cinr_ch;
	char* bsid_ch;
	char* txpwr_ch;
	char* freq_ch;
	char* state_ch;
};

struct radius_client_rec{
	char* serv_auth_addr;
	char* serv_auth_pass;
	char* serv_acc_addr;
	char* serv_acc_pass;
};

struct qos_incoming{
	char* enable;
	char* upload;
	char* download;
};

struct qos_ip_incoming{
	char* ip;
	char* port;
	char* upload;
	char* download;
};

struct qos_mgr{
	char* enable;
	char* defDSCPMark;
	char* defQueue; // now, must be 0
	char* qosPrioLevel; //const, only read, must be NULL
	char* qosEthPrioLevel; //const, only read, must be NULL
	char* cls_max_entry; //const, only read, must be NULL
	char* atm_max_queue; //const, only read, must be NULL
	char* eth_max_queue; //const, only read, must be NULL (for each interface)
};

struct qos_cls{
	char* ClassName; // rw , not NULL !!!!!
	char* classificationOrder; // rw , -1(Last)|1|2... !!!!!
	char* classIf; // rw ,  CONFIG_ID_GET_QOS_IFACES
	char* ethertype; // rw
	char* sourceMAC; // rw
	char* sourceMACmask; // rw
	char* destMAC; // rw
	char* destMACmask; // rw
	char* srcIP; // rw
	char* srcVendor; // rw 
	char* srcUser; // rw
	char* dstIP; // rw
	char* proto; // TCP - 6 ; UDP - 17 ; ICMP - 1 ; IGMP -2 ;
	char* srcPorts; // rw 80|80:90
	char* dstPorts; // rw 80|80:90
	char* DSCPCheck; // большая хрень...поясню при необходимости см. qoscls.html
	char* ethPriorCheck; // 802.1P Check
	char* classQueue; // queue key
	char* DSCPMark; // ещё одна большая хрень...поясню при необходимости см. qoscls.html
	char* ethPriorMark; // 802.1P Mark
	char* vlanIdTag; // rw Tag VLAN ID [0-4094]
	char* ClassRate; // ro NULL !
	char* enabled; //rw , 1 -enabled|NULL-disabled !!!!!!!!!
	char* egressInterface; // queue iface
}; //24

struct qos_que{
	char* name; // rw , not NULL !!!!!
	char* key; // ro
	char* iface; // rw , not NULL !!!!!!!! CONFIG_ID_GET_QUE_IFACES
	char* alg; // ro
	char* prec; // rw , not NULL !!!!!!!!
	char* weight;// ro
	char* latency;// ro
	char* ptmPrio;// ro
	char* enable; //rw , 1 -enabled|NULL-disabled !!!!!!!!!
	char* rm; //ro
}; //10

struct qos_bcm_special{
	char* line;
}; //для CONFIG_ID_GET_QOS_IFACES CONFIG_ID_GET_QUE_IFACES

struct urlfilter_incoming{
	char* url;
};

struct arp_ddos_incoming{
	char* enable;
};

struct apprules_incoming{
	char* name;
	char* tr_port;
	char* tr_protocol;
	char* fw_port;
	char* fw_protocol;
};

struct watchdog_incoming{
	char* enable;
	char* ip1;
	char* ip2;
	char* sleep_time;
	char* watch_route;
	char* link0;
	char* link1;
};


struct igmp_incoming{
	char* version;
	char* enable;
	char* iface;
};

struct sched_incoming{
	char* name;
	char* enable;
	char* day_month;
	char* day_week;
	char* time_of_day;
};

struct voice_incoming{
	char* ifname;
	char* locale;
	char* sipdomain;
	char* sipport;
	char* sipproxyaddr;
	char* sipproxyport;
	char* sipoutboundproxyaddr;
	char* sipoutboundproxyport;
	char* sipregistraraddr;
	char* sipregistrarport;
	char* siplineenable0;
	char* siplineenable1;
	char* phyreferencelist0;
	char* phyreferencelist1;
	char* extension0;
	char* extension1;
	char* authusername0;
	char* authusername1;
	char* authpassword0;
	char* authpassword1;
	char* packetizationperiod0;
	char* packetizationperiod1;
	char* codec0;
	char* codec1;
	char* outgoingcalldialplan;
	char* incomingcallroutingmode;
	char* incomingcallroutingdest;
	char* callwaitingenable0;
	char* callwaitingenable1;
	char* callforwardunconditionalnumber0;
	char* callforwardunconditionalnumber1;
	char* callforwardunconditionalenable0;
	char* callforwardunconditionalenable1;
	char* callforwardonbusyenable0;
	char* callforwardonbusyenable1;
	char* callforwardonnoanswerenable0;
	char* callforwardonnoanswerenable1;
	char* mwienable0;
	char* mwienable1;
	char* callbarringenable0;
	char* callbarringenable1;
	char* callbarringuserpin0;
	char* callbarringuserpin1;
	char* callbarringdigitmap0;
	char* callbarringdigitmap1;
	char* anonymouscallblockenable0;
	char* anonymouscallblockenable1;
	char* anonymouscallenable0;
	char* anonymouscallenable1;
	char* donotdisturbenable0;
	char* donotdisturbenable1;
	char* t38;
	char* v18;
	char* sipregexpires;
	char* sipregretryinterval;
	char* dtmfmethod;
	char* digitmap;
	char* hookflash;
	char* siptransport;
	char* siptotagmatching;
	char* sipmusicserver;
	char* sipmusicserverport;
	char* sipstart;
	char* sipstop;
	char* sipsave;
	char* sipdefaults;
};

struct macfilter_incoming{
	char* mac;
	char* name_of_sched;
};


struct mac_do{
	char* mac;	
};
/*! \brief Структура UpnpCfg
 *
 *  Используется для конфигурации Upnp.
 */
#ifdef BCM
struct UpnpCfg{
	char* enable; 	/**< Вкл/выкл [yes|no] */	
};
#else
struct upnp_incoming{
	char* enable;
	char* lan_iface;
	char* wan_iface;
};
#endif //BCM

struct device_params_incoming{
	char* name;
	char* mode;
	char* allow_ifaces;
};

struct ifaces_incoming {
	char* name;				/* имя интерфейса        */
	char* provider;
	char* type;				/* тип               */
	char* autor;			/* автоподнятие*/
	char* user_name; 		/* имя пользователя */
	char* password;  		/* пароль*/
	char* ifup_status;
	char* metric;			/* метрика       */
	char* mtu;				/* MTU             */
	char* ip;
	char* mask;
	char* dst_ip;
	char* bcast;
	char* mac;
	char* cname;			/* имя концентратора */
	char* sname;			/* имя сервиса */
	char* options;          /* сюда сваливать опции для неизвестного типа */
};

#endif
#endif

