#ifndef UNPACK_H
#define UNPACK_H

//int unpack_by_size(const char *path_to_flash, const char *path_to_kernel, const char *path_to_rootfs, const int kernel_size);
int unpack_from_flash(const char *path_to_flash,const  char *path_to_kernel,const char *path_to_rootfs);
int unpack_image(const char *flash, const char *kernel, const char *rootfs);
#endif
