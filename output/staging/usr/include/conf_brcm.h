#ifndef CONF_BRCM_H
#define CONF_BRCM_H

#include "cms.h"
#include "cms_dal.h"
#include "cms_log.h"
#include "cms_mdm.h"
#include "lib_config.h"
#include "cms_boardioctl.h"
#include "jansson.h"
extern void *msgH;

#define BRCM_DECLARE_STATIC(x) struct brcm x={NULL}


extern CmsRet oal_lock(const UINT32 *timeoutMs);
extern CmsRet oal_unlock(void);
   


#ifndef MLOCK


#if 0

#define MLOCK                  do {DEBUG_PRINT("-------> locking...\n"); \
                                   DEBUG_PRINT("<------- locked with %d\n",oal_lock(NULL));} while (0)
#define MLOCK_WITH_TIMEOUT(p)  do {DEBUG_PRINT("-------> locking with %d msec...\n",(p)); \
                                   DEBUG_PRINT("<------- locked with %d\n",oal_lock((p)));}  while (0)
#define MUNLOCK                do {DEBUG_PRINT("-------> unlocking...\n"); \
                                   DEBUG_PRINT("<------- unlocked with %d\n",oal_unlock());} while (0)
#else
#define MLOCK                  oal_lock(NULL)
#define MLOCK_WITH_TIMEOUT(p)  oal_lock(p)
#define MUNLOCK                oal_unlock()
#endif


#endif



extern int brcm_start(struct brcm* brcm);
extern int brcm_stop(struct brcm* brcm);
#endif

