#ifndef _SYSTEM_H_
#define _SYSTEM_H_

struct resident_threads{
	int num_threads;
	void** stacks;
};

int create_resident_thread(int(*thread_func)(void*), void* arg, size_t stack_size, bool use_clone);
void thread_notify(const char* str);
void close_all(void);
void unlink_all(void);
void create_error_file(const char* name);
void tmp_siguser_handler(int sig);
void sigterm_handler(int sig);
void sigsegv_handler(int sig);
void sigchld_handler(int sig);
void inrpc_sigsegv_handler(int sig);
void sigpipe_handler(int sig);

#ifdef ROUTERS
int passwd(const char* username, const char* new_pw);
#endif

#ifdef AUTO_RM_ALL_SOMOVD_FILES
int glob_err(const char* pathname, int globerror);
int rm_somovd_files(const char* pattern);
#endif		

#endif
