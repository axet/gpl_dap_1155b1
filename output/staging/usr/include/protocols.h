#ifndef _PROTOCOLS_H_
#define _PROTOCOLS_H_

struct outgoin* CreateAnswerWithStatus(int status);
void AddToAnswer(struct outgoin* answer,void* buf,int size_of_buf,char* separate);
#define AddToAnswerWithoutSeparator(answer,buf,size_of_buf) AddToAnswer(answer,buf,size_of_buf,NULL)
#define AddToAnswerWithSeparator(answer,buf,size_of_buf) AddToAnswer(answer,buf,size_of_buf,RPC_PROTOCOL_SEPARATOR)
#define AddStrToAnswerWithoutSeparator(answer,buf) AddToAnswer(answer,(void*)buf,0,NULL)
#define AddStrToAnswerWithSeparator(answer,buf) AddToAnswer(answer,(void*)buf,0,RPC_PROTOCOL_SEPARATOR)
#define AddStrToAnswerWithZeroSeparator(answer,buf) AddToAnswer(answer,(void*)buf,0,"\0")
#define AddStrToAnswerWithSpaceSeparator(answer,buf) AddToAnswer(answer,(void*)buf,0," ")
#define AddStrToAnswerWithNewLineSeparator(answer,buf) AddToAnswer(answer,(void*)buf,0,"\n")
void AddStructToAnswer(struct outgoin* answer,void** buf,size_t size_of_struc);
int free_all_answers();

#endif
