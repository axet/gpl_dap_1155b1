/* dms_nl_route.h */
#ifndef _DMS_NL_ROUTE_H
#define _DMS_NL_ROUTE_H

typedef struct __inet_prefix_t {
	__u8 family;
	__u8 bytelen;
	__s16 bitlen;
	__u32 flags;
	__u32 data[4];
} inet_prefix;

int addattr_l(struct nlmsghdr * n, int maxlen, int type, const void * data, int alen);
int addattr32(struct nlmsghdr *n, int maxlen, int type, __u32 data);
int get_prefix(inet_prefix * dst, char * arg, int family);

int ipaddr_modify(int family, char * iface, int cmd, int flags, inet_prefix * lcl);
int iproute_modify(int family, int cmd, unsigned flags, char * net, char * gateway, char * dev, __u32 metric, __u32 table);

#endif
