/************************************************************/
/* File name: firewall.h                                    */
/************************************************************/

#ifndef __FIREWALL_H__
#define __FIREWALL_H__

/*----------------------------------------------------------*/

#define NUM_MASQ_PROTOCOLS 4
#define NUM_RULES_PROTOCOLS 4

#define NUM_RULES_ACTIONS 2

#define NUM_RULES_LOG_LEVELS 9

#ifdef MASQ
struct objects *read_firewall_masq     (char *config_file);
void save_firewall_masq      (struct objects *objs);

void read_masq(char* strings[], struct records* dest);
bool save_masq(char* strings[], struct records* source, int pos);
void free_masq(struct records* source, int pos);

struct masq_record{
	char* iface;
	char* ip;
	char* mask;
	char* proto;
	char* port;
	bool del;
};
#endif //#ifdef MASQ

#endif /* __FIREWALL_H__ */

/************************************************************/
/* End of file                                              */
/************************************************************/
