#ifndef CONF_3G_COMMON_H
#define CONF_3G_COMMON_H

#ifdef G3

#define FILE_NEW_SMS "/tmp/new_sms"

int set_mode_3g(int mode_3g_2g);
int get_dev_type();
int get_pinstatus();
int ucs2_to_utf8(char* dst, const char* src, size_t size, size_t* dst_size, char sym);
int utf8_to_ucs2(char* dst, const char* src, size_t size, size_t* dst_size, char sym, int byte_order_le);

#endif

#endif
