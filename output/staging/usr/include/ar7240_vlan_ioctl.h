#ifndef AR7240_VLAN_H
#define AR7240_VLAN_H

#include <stdint.h>
#include <sys/types.h>

#define VLAN_CPU    (1 << 0)
#define VLAN_PORT0  (1 << 1)
#define VLAN_PORT1  (1 << 2)
#define VLAN_PORT2  (1 << 3)
#define VLAN_PORT3  (1 << 4)
#define VLAN_PORT4  (1 << 5)
#define VLAN_PORT5  (1 << 6)
#define VLAN_PORT6  (1 << 7)
#define VLAN_PORT7  (1 << 8)


void vlan_open();
void vlan_close();
void vlan_set_iface(const char *iface);
void vlan_enable();
void vlan_disable();
void vlan_addports(u_int32_t vid, u_int16_t portnum);
void vlan_delports(u_int32_t vid, u_int16_t portnum);
void vlan_setmode(u_int32_t mode, u_int16_t portnum);
void vlan_setdefault(u_int32_t vid, u_int16_t portnum);

#endif
