/* ralink.h */
#ifndef __RALINK_H_
#define __RALINK_H_

#include <jansson.h>
#include <netinet/ether.h>
#include <linux/if.h>
#include <linux/wireless.h>

#define LED_A_ON		1
#define LED_A_OFF		2
#define LED_A_RESET		3
#define LED_A_PROGRESS	4
#define LED_A_ERROR		5
#define LED_A_OVERLAP	6
#define LED_A_SUCCESS	7

#define MASK_ALL 0xff
#define WAN  "eth2.5"
#ifndef CONFIG_RALINK_MT7620
#define VLAN_MASK "0000001"
#else
#define VLAN_MASK "00000011"
#endif
#ifdef DIR_300NRUB5_6
#define NEW_KERNEL
#endif

#if defined (CONFIG_RALINK_MT7620) || defined (CONFIG_RALINK_MT7621)
#define MAX_PORT		7
#else
#define MAX_PORT		6
#endif

#ifdef DIR_300NRUB5_6
#define NEW_KERNEL
#endif

#if defined(DIR_300NRU)
static const int leds_gpio[]=
{
	9,		// зелёный power
	8,		// status (yellow power)
	12,		// зелёный wan
	14,		// желтый wan
	13,		// синий wps
	-1
};
#define LED_POWER			0
#define LED_STATUS			1
#define LED_GREEN_WAN		2
#define LED_YELLOW_WAN		3
#define LED_WPS				4
//#define LED_USB				5
// NO LEDS
#define WAN_NUM 5
#define NUM_PORTS 5
// END NO LEDS

#elif defined(DIR_620)
static const int leds_gpio[]=
{
	9,		// зелёный power
	8,		// status (yellow power)
	12,		// зелёный wan
	14,		// желтый wan
	11,		// синий wps
	13,		// желтый usb
	-1
};
#define LED_POWER			0
#define LED_STATUS			1
#define LED_GREEN_WAN		2
#define LED_YELLOW_WAN		3
#define LED_WPS				4
#define LED_USB				5
// NO LEDS
#define WAN_NUM 5
#define NUM_PORTS 5
// END NO LEDS

#elif defined(DIR_620A9)
static const int leds_gpio[]=
{
	9,		// зелёный power
	8,		// status (yellow power)
	12,		// зелёный wan
	14,		// желтый wan
	11,		// синий wps
	13,		// желтый usb
	-1
};
#define LED_POWER			0
//#define LED_STATUS			1
//#define LED_GREEN_WAN		2
//#define LED_YELLOW_WAN		3
#define LED_WPS				4
//#define LED_USB				5
// NO LEDS
#define WAN_NUM 5
#define NUM_PORTS 5
// END NO LEDS
#elif defined(DIR_620DF1A_MT7620N_ALLWINS)
static const int leds_gpio[]=
{
	9,		// зелёный power
	8,		// status (yellow power)
	12,		// зелёный wan
	14,		// желтый wan
	11,		// синий wps
	13,		// желтый usb
	-1
};
#define LED_POWER			0
#define LED_WPS				4
#define LED_USB				5
// NO LEDS
#define WAN_NUM 5
#define NUM_PORTS 5
// END NO LEDS

#elif defined(DIR_620D1)
static const int leds_gpio[]=
{
	9,		// зелёный power
	17,		// status (yellow power)
	-1
};
#define LED_POWER			0
#define LED_WLAN			1
#define WAN_NUM 5
#define NUM_PORTS 5
#elif defined(DIR_615F)
static const int leds_gpio[]=
{
	9,		// зелёный power
	17,		// status (yellow power)
	-1
};
#define LED_POWER			0
#define LED_WLAN			1
#define WAN_NUM 5
#define NUM_PORTS 5
#elif defined(DIR_456B1) || defined(DIR_456C1A)
static const int leds_gpio[]=
{
	25,		// зелёный power
	27,		// status (yellow power)
	38,		// зелёный wan
	39,		// желтый wan
	26,		// синий wps
	37,		// желтый usb
	-1
};
#define LED_POWER			0
#define LED_STATUS			1
#define LED_GREEN_WAN		2
#define LED_YELLOW_WAN		3
#define LED_WPS				4
#define LED_USB				5
// NO LEDS
#define NUM_PORTS 4
// END NO LEDS

#elif defined(DIR_320)
static const int leds_gpio[]=
{
	9,		// зелёный power
	8,		// status (yellow power)
	11,		// зелёный wan
	12,		// желтый wan
	13,		// синий wps
	14,		// желтый usb
	-1
};
#define LED_POWER			0
//#define LED_STATUS			1
//#define LED_GREEN_WAN		2
//#define LED_YELLOW_WAN		3
#define LED_WPS				4
#define LED_USB				5
// NO LEDS
#define WAN_NUM 5
#define NUM_PORTS 5
// END NO LEDS

#elif defined(DIR_412)
static const int leds_gpio[]=
{
	8,		// зелёный power
	9,		// status (yellow power)
	14,		// зелёный wan
	12,		// желтый wan
	13,		// синий wps
	-1
};
#define LED_POWER			0
#define LED_STATUS			1
#define LED_GREEN_WAN		2
#define LED_YELLOW_WAN		3
#define LED_WPS				4
//#define LED_USB				5
#define DAP_MODE
// NO LEDS
#define WAN_NUM 1
#define NUM_PORTS 1
// END NO LEDS

#elif defined(DIR_300NRUB5)
static const int leds_gpio[]=
{
	9,		// зелёный power
	8,		// status (yellow power)
	11,		// зелёный wan
	12,		// желтый wan
	13,		// синий wps
	14,		// желтый usb
	-1
};
#define LED_POWER			0
//#define LED_STATUS			1
//#define LED_GREEN_WAN		2
//#define LED_YELLOW_WAN		3
#define LED_WPS				4
//#define LED_USB				5
// NO LEDS
#define WAN_NUM 5
#define NUM_PORTS 5
// END NO LEDS

#elif defined(DIR_320NRU) || defined(DIR_320NRU_RST)
static const int leds_gpio[]=
{
	9,		// зелёный power
	8,		// status (yellow power)
	11,		// зелёный wan
	12,		// желтый wan
	13,		// синий wps
	14,		// желтый usb
	-1
};
#define LED_POWER			0
//#define LED_STATUS			1
//#define LED_GREEN_WAN		2
//#define LED_YELLOW_WAN		3
#define LED_WPS				4
#define LED_USB				5
// NO LEDS
#define WAN_NUM 5
#define NUM_PORTS 5
// END NO LEDS

#elif defined(DAP_1350) || defined(DAP_1150)
// NO LEDS
#define DAP_MODE
#define WAN_NUM 1
#define NUM_PORTS 1
// END NO LEDS

#else
static const int leds_gpio[]=
{
	9,		// зелёный power
	8,		// status (yellow power)
	12,		// зелёный wan
	14,		// желтый wan
	13,		// синий wps
	-1
};
#define LED_POWER			0
#define LED_STATUS			1
#define LED_GREEN_WAN		2
#define LED_YELLOW_WAN		3
#define LED_WPS				4
//#define LED_USB				5
#define WAN_NUM 0
#define NUM_PORTS 0 //ни черта нет портов

#endif

enum dual_wan_type	//-> p0~p4
{
	WLLLL,
	WLLLW,
	LWLLW,
	LLWLW,
	LLLWW,
	LLWWW
};

#define MAX_MBSSID_NUM 4

/* for WPS --YY  */
#define RT_OID_SYNC_RT61                            0x0D010750
#define RT_OID_WSC_QUERY_STATUS                     ((RT_OID_SYNC_RT61 + 0x01) & 0xffff)
#define RT_OID_WSC_PIN_CODE							((RT_OID_SYNC_RT61 + 0x02) & 0xffff)
#define RT_PRIV_IOCTL				(SIOCIWFIRSTPRIV + 0x0E)

#define RTPRIV_IOCTL_GET_MAC_TABLE		(SIOCIWFIRSTPRIV + 0x0F)
#define RTPRIV_IOCTL_WSC_PROFILE		(SIOCIWFIRSTPRIV + 0x12)
#define RTPRIV_IOCTL_GSITESURVEY 		(SIOCIWFIRSTPRIV + 0x0D)
#define RTPRIV_IOCTL_SET				(SIOCIWFIRSTPRIV + 0x02)

#define PACKED  __attribute__ ((packed))
#define USHORT  unsigned short
#define UCHAR   unsigned char

typedef struct PACKED _WSC_CONFIGURED_VALUE {
    USHORT WscConfigured; // 1 un-configured; 2 configured
    UCHAR   WscSsid[32 + 1];
    USHORT WscAuthMode; // mandatory, 0x01: open, 0x02: wpa-psk, 0x04: shared, 0x08:wpa, 0x10: wpa2, 0x
    USHORT  WscEncrypType;  // 0x01: none, 0x02: wep, 0x04: tkip, 0x08: aes
    UCHAR   DefaultKeyIdx;
    UCHAR   WscWPAKey[64 + 1];
} WSC_CONFIGURED_VALUE;

typedef struct _SITE_SURVEY
{
  unsigned char  channel;
  unsigned short rssi;
  unsigned char  ssid[33];
  unsigned char  bssid[6];
  unsigned char  security[9];
} SITE_SURVEY;

typedef union _MACHTTRANSMIT_SETTING {
	struct  {
		unsigned short  MCS:7;  // MCS
		unsigned short  BW:1;   //channel bandwidth 20MHz or 40 MHz
		unsigned short  ShortGI:1;
		unsigned short  STBC:2; //SPACE
		unsigned short  rsv:3;
		unsigned short  MODE:2; // Use definition MODE_xxx.
	} field;
	unsigned short      word;
} MACHTTRANSMIT_SETTING;

typedef struct _RT_802_11_MAC_ENTRY {
	unsigned char            ApIdx;
	unsigned char            Addr[6];
	unsigned char            Aid;
	unsigned char            Psm;     // 0:PWR_ACTIVE, 1:PWR_SAVE
	unsigned char            MimoPs;  // 0:MMPS_STATIC, 1:MMPS_DYNAMIC, 3:MMPS_Enabled
	char                     AvgRssi0;
	char                     AvgRssi1;
	char                     AvgRssi2;
	unsigned int             ConnectedTime;
	MACHTTRANSMIT_SETTING    TxRate;
	unsigned int			 LastRxRate;
	int						 StreamSnr[3];
	int						 SoundingRespSnr[3];
} RT_802_11_MAC_ENTRY;

typedef struct _RT_802_11_MAC_TABLE {
	unsigned long            Num;
	RT_802_11_MAC_ENTRY      Entry[32]; //MAX_LEN_OF_MAC_TABLE = 32
} RT_802_11_MAC_TABLE;



/* Vlan defines */
int set_port_pvid_dt_rem(int port, int pvid, int double_tag, int removal_tag);
int set_vlan_id_ports(int idx, int vid, char *vlan_ports);
void vlan_show(void);
void show_ports_pvid(void);

int vlans_ifaces_grouping(json_t *config);

/*reg*/
int switch_init(void);
void switch_fini(int sw_fd);
int ra3052_reg_read(int esw_fd, int offset, int *value);

/* Common defines */
int set_device_mode(int mode, json_t *glconfig);
int gen_config_new(json_t *config, int mode_bridge);
void deinit_wifi();
int start_wps(json_t *config);
void preinit(json_t *config);

/* Wi-Fi */
int getWscProfile(char *interface, WSC_CONFIGURED_VALUE *data, int len);
void getWPSAuthMode(WSC_CONFIGURED_VALUE *result, char *ret_str);
void getWPSEncrypType(WSC_CONFIGURED_VALUE *result, char *ret_str);

void init_pre_boot(void);
void init_post_boot(void);

static int gpioLedSet(int gpio, unsigned int on, unsigned int off,
		unsigned int blinks, unsigned int rests, unsigned int times);

int set_gpios(int mask, int action);
//int ralink_button_check(void* in);

void register_gpio_reset(void);

#endif
#if defined (CONFIG_RALINK_RT3052) || (CONFIG_RALINK_RT3050)
#define RALINK_GPIO_READ					RALINK_GPIO3924_READ
#define RALINK_GPIO_EXTREAD					RALINK_GPIO5140_READ
#elif defined (CONFIG_RALINK_RT3352)
#define RALINK_GPIO_READ					RALINK_GPIO3924_READ
#ifdef RALINK_OLD_KERNEL
#define RALINK_GPIO_EXTREAD					RALINK_GPIO5140_READ
#else
#define RALINK_GPIO_EXTREAD					RALINK_GPIO4540_READ
#endif
#elif defined (CONFIG_RALINK_RT5350)
#define RALINK_GPIO_READ					RALINK_GPIO2722_READ
#elif defined (CONFIG_RALINK_MT7620)
#define RALINK_GPIO_READ					RALINK_GPIO3924_READ
#define RALINK_GPIO_EXTREAD					RALINK_GPIO7140_READ
#endif
