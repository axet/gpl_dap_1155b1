/* ar7240.h */
#ifndef _AR7240_H_
#define _AR7240_H_

#include "jansson.h"

/* Copy from kernel/drivers/char/ar7240_gpio.h */
#define LED_POWER_GREEN 	0x01
#define LED_POWER_YELLOW 	0x02
#define LED_WAN_GREEN 		0x03
#define LED_WAN_YELLOW 		0x04
#define LED_WAN_ENABLE		0x05
#define LED_WPS_ON			0x06
#define LED_WPS_OFF			0x07
#define LED_USB_ON			0x08
#define LED_USB_OFF			0x09
#define REG_IRQ				0x10

#define MTD_CONFIG   "/dev/mtd1"
#define MTD_MAC "/dev/mtd4"

#ifdef DIR_632
#define LAN  "eth0"
#define WAN  "eth1"
#define WAN_NUM 9 //уточнить надо
#define NUM_PORTS 9
#else
#define LAN  "eth1"
#define WAN  "eth0"
#define WAN_NUM 0
#define NUM_PORTS 0 //ни черта нет портов
#endif

#define SHARED_WAN "wan1"

void get_base_mac(char *base_mac);
void get_wan_mac(char *wan_mac);

void preinit(json_t *config);

json_t* list_stations(const char *ifname);
json_t* list_scan(int is_client_enabled);

#define IFNAMSIZ        16

#ifdef DIR_632
#define MAC_NUM_WAN     1
#else
#define MAC_NUM_WAN     5
#endif

#endif
