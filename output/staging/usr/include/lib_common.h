#ifndef _LIB_SYS_H_
#define _LIB_SYS_H_

#include "../main.h" // только если собираем вместе с демоном, иначе надо иметь эти заголовки и поправить соответственно путь

#ifdef G3
#include "3g.h"
#include "3g_sms.h"
#include "3g_sms2.h"
#include "3g_network.h"
#include "conf_3g_info.h"
#include "conf_3g_common.h"
#ifdef G3_PHONEBOOK
#include "3g_phonebook.h"
#include "3g_charset.h"
#endif
#endif
#include "conf_arp.h"
#include "route_table.h"
#include "rpc_cmd.h"
#include "conf_g3.h"
#include "dlink_wimax.h"
#include "conf_watchdog.h"
#include "conf_usb.h"
#include "conf_fon.h"
#include "conf_web_profiles.h"
#endif
