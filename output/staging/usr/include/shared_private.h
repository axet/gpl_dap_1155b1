#ifndef _SHARED_PRIVATE_H_
#define _SHARED_PRIVATE_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/reboot.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mount.h>
#include <signal.h>
#include <netinet/ether.h>
#include <linux/if.h>
#include <jansson.h>

#ifdef DAP_MODE
#define DEF_HOSTNAME "dlinkap"
#else
#ifdef DIR_620D1PZ
#define DEF_HOSTNAME "Promzakaz-Router"
#else
#define DEF_HOSTNAME "Dlink-Router"
#endif
#endif

#ifdef BCM47XX
#include <bcmnvram.h>
#include <bcmutils.h>
#define __INCif_etherh
#include <wlioctl.h>
#ifndef DIR_320MTS
#include <bcmgpio.h>
#endif
#include <mtd/mtd-user.h>
#endif
#define IFUP (IFF_UP | IFF_RUNNING | IFF_BROADCAST | IFF_MULTICAST)
#define sin_addr(s) (((struct sockaddr_in *)(s))->sin_addr)

#ifndef MIN
#define MIN(a, b)	((a) < (b) ? (a) : (b))
#endif

#define DL_MAGIC	0x31121982		/* dlink magic number		*/
#define IH_NMLEN	(32-4)			/* Image Name Length		*/
#define ZIP_MAGIC	0x1F8B0800		/* zip magic number			*/
#define IH_MAGIC	0x27051956		/* mkimage magic number		*/
#define RLX_MAGIC	0x63723662		/* mkimage magic number		*/
#define RLX_BOOT_MAGIC	0x626F6F74	/* Магическое число uboot для RLX */

typedef struct dlink_image_header {
	uint32_t	magic;				/* magic					*/
	uint8_t		model_name[20];		/* model					*/
	uint8_t		signature[30];		/* signature				*/
	uint8_t		header_len;			/* header length			*/
	uint32_t	image_chksum;		/* image checksum			*/
	uint32_t	image_size;			/* the size of image, not include the image header	*/
	uint8_t		part;				/* partition (not used)		*/
} dlink_image_header_t;

typedef struct image_header {
	uint32_t	ih_magic;	/* Image Header Magic Number	*/
	uint32_t	ih_hcrc;	/* Image Header CRC Checksum	*/
	uint32_t	ih_time;	/* Image Creation Timestamp	*/
	uint32_t	ih_size;	/* Image Data Size		*/
	uint32_t	ih_load;	/* Data	 Load  Address		*/
	uint32_t	ih_ep;		/* Entry Point Address		*/
	uint32_t	ih_dcrc;	/* Image Data CRC Checksum	*/
	uint8_t		ih_os;		/* Operating System		*/
	uint8_t		ih_arch;	/* CPU architecture		*/
	uint8_t		ih_type;	/* Image Type			*/
	uint8_t		ih_comp;	/* Compression Type		*/
	uint8_t		ih_name[IH_NMLEN];	/* Image Name		*/
	uint32_t	ih_ksz;		/* Kernel Part Size		*/
} image_header_t;

typedef struct base_conf {
	char magic[8];
	char base_mac[6];			/* base mac        */
	char wan_mac[6];			/* wan mac        */
	char wlan_mac[6];				/* wlan mac       */
	char pin[20];
	char sn[20];
	char hwrev[20];
	char countrycode[20];
	char flashspeed[20];
	char appex[20];
} conf_f;

//int mtd_open(const char *mtd, int flags) ;

/* mtd */
int read_data_from_flash(conf_f *tconf);
int write_data_to_flash(conf_f *tconf);

ssize_t mtd_read_to_buf(const char *dev, off_t offset, size_t size, void *buf);
int mtd_write_from_buf(const char *dev, off_t offset, size_t size, const void *buf);

int dms_mtd_open(const char *mtd, int flags);
int dms_mtd_erase(const char *mtd);
int dms_mtd_flash_buffer(char *buffer, long size, const char *mtd);
int dms_mtd_flash_file(const char *path, const char *mtd);

int mtd_unlock(const char *mtd);
int mtd_open_(const char *mtd, int flags);
void mtd_close(int fd);

unsigned int mtd_size(int fd);
int mtd_seek(int fd, long seek);
int mtd_read(int fd, void *buffer, long size);
int mtd_write(int fd, void *buffer, long size);
int mtd_erase(int fd);
#ifdef USE_JFFS2_PARTITION_512K
int mtd_format_for_jffs2 (int fd);
#endif
int dconf(const char *command, const char *mtd, const long  seek, const char *config_file, const int reboot);


/* config arch */
int read_config_from_flash_arch(const char * file_name);
int write_config_to_flash_arch(const char * conf_name);
int erase_config_from_flash_arch(void);
int backup_config_arch(const char * conf_name);
int restore_config_arch(const char * conf_name);

int check_firmware_in_buffer(char *buffer, long size, char *device_name) ;

void config_loopback(void);

/* services */
int update_dns(json_t *config);
int update_ntpclient(json_t *config);

#ifdef SUPPORT_IGMP
int stop_igmpproxy(json_t *config);
int update_igmpproxy(json_t *config);
#endif
#ifdef SUPPORT_DLINK_IGMPPROXY
int stop_igmpx(json_t *config);
int update_igmpx(json_t *config);
#endif

int stop_crond(json_t *conf);
int start_httpd(json_t* conf);

/* common */
#define UUID_STRING_LENGTH 	37	///< длина UUID в строковой форме (с учётом завершающего нуля)
char *dms_uuid_get(char *uuid, int len);
int ifconfig(char *name, int flags, char *addr, char *netmask);
int ipaddr_ctrl(char *name, int family, int action, char *addr);
int show_addr(char *iface, int fl_flag);
void ip2class(char *lan_ip, char *netmask, char *buf);
int router_totalram();
void parse_l3_key(char *l3_key, char *l2_key, int *contag);

/* config */

int dms_init_config();
int msg4_handler(char *msg_buffer, json_t *iface_array);

/* route ipv4*/
int route_add(char *name, int metric, char *dst, char *gateway, char *genmask);
int route_del(char *name, int metric, char *dst, char *gateway, char *genmask);

int set_dhcp_routes(char *link, char *iface, int metric);
int apply_routing(json_t *config, const char *iface);

int route_add_ext(const char *name, int metric, const char *dst, const char *gateway, const char *genmask, int table);
int route_del_ext(const char *name, int metric, const char *dst, const char *gateway, const char *genmask, int table);

/* route ipv6 */
int route6_add(char *name, int metric, char *dst, char *gateway);
int route6_del(char *name, int metric, char *dst, char *gateway);


/* iptables */
void nat_table_set(char *lan_if, char *lan_ip, char *lan_netmask, char *logaccept, char *logdrop);
void mangle_table_set(char *lan_if, char *logaccept, char *logdrop);
void filter_table_set(char *lan_if, char *lan_ip, char *logaccept, char *logdrop);

/* brctl */
int br_add(char *name);
int br_del(char *name);
int br_addif(char *brname, char *ifname);
int br_delif(char *brname, char *ifname);
int get_br_ifaces_list(const char *name, char *ifaces_list, int list_len);
int manip_bridge_ext(char *brname, char *ifaces_list);

/* vlan */
int set_vlan_name_type(char *name_type);
int add_vlan(char *iface, int vid);
int rem_vlan(char *iface);
int set_vlan_egress_map(char *iface, int skb, int qos);
int set_vlan_ingress_map(char *iface, int skb, int qos);

/* ethernet */
struct ether_addr *check_hwaddr(const char *asc);
int set_hwaddr(const char *iface, const char *hwaddr);
int set_mtu(const char *iface, int mtu);
int set_hwaddr_mtu_ext(const char *iface, const char *hwaddr, int mtu);


int update_firmware_from_file(json_t *config, char *firmware_file);

/* policy routing */
extern int dms_policy_routing_apply(json_t *l3, const char *iface, const char *ip, const char *mask);
extern int dms_policy_routing_cancel(json_t *l3, const char *iface, const char *ip, const char *mask);

/* network */
extern int get_setting_ones(char *str);

#if defined(linux) && defined(DEBUG)
# ifdef DEBUG_TO_SYSLOG
# define cprintf cprintf_syslog
extern void cprintf_syslog( char * fmt, ... );
# else
/* Print directly to the console */
#define cprintf(fmt, args...) do { \
	FILE *fp = fopen("/dev/console", "w"); \
	if (fp) { \
		fprintf(fp, fmt, ## args); \
		fclose(fp); \
	} \
} while (0)
# endif

#else
#define cprintf(fmt, args...) do { \
} while (0)
#endif

/* Debug print */
#ifdef DEBUG_D
#define dprintf(fmt, args...) cprintf("%s: " fmt, __FUNCTION__, ## args)
#else
#define dprintf(fmt, args...)
#endif /*DEBUG*/

#endif /* _SHARED_PRIVATE_H_ */
