#ifndef _RPC_H_
#define _RPC_H_

struct supported_rpc* get_supported_rpc(void);
struct outgoin* call_requested_rpc(struct incoming_ctl* request);
struct outgoin* build_incoming_request(struct incoming_ctl* request, char* buf);

struct outgoin* call_conf_read(long id);
struct records* call_conf_read_ex(long id,size_t size);

#ifdef PARALLEL_PROCESSING
int set_shared_answer(pid_t to_pid,char* buf,int count);
//int get_shared_answer(pid_t to_pid,void** answer);
//int rm_shared_answer(pid_t from_pid,void** answer);

int free_completed_rpcs(int start, int count);
int add_rpc_in_process(struct incoming_ctl* request);
struct outgoin* check_requested_rpc(struct incoming_ctl* request);
#endif

#ifdef NON_BLOCKING_RPC_FEATURE
struct u_rpc_id* create_u_rpc_id(struct incoming_ctl* request);
struct u_rpc_id* get_u_rpc_id(struct incoming_ctl* request);
struct outgoin* get_rpc_status(struct incoming_ctl* request,struct u_rpc_id* uid);
#endif

#endif
