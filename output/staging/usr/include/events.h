#ifndef _EVENTS_H_
#define _EVENTS_H_

#define EVENT_SIGNATURE 0
#define EVENT_HANDLER  1
#define EVENT_DO_FORK   2
#define EVENT_HAVE_ARG  3

#define EV_NO_ARG    NULL
#define EV_NO_FORK   NULL
#define EV_NEED_ARG  (void*)1
#define EV_NEED_FORK (void*)1

#define EVENT_RTVALVED_START	'V'
#define EVENT_RTVALVED_STOP		'v'
#define EVENT_FON_ONLINE		'F'
#define EVENT_FON_OFLINE		'f'
#define EVENT_TEST_DFL			'T'
#define EVENT_TEST_PRN			't'
#define EVENT_TEST_FORK			'F'
#define EVENT_TEST_DATA			'D'
#define EVENT_IP_UP				'P'
#define EVENT_PPP_DOWN			'p'
#define EVENT_UDHCP_DOWN		'u'
#define EVENT_TEMP				'E'
#define EVENT_IPOE				'I'
#define EVENT_USB				'U'
#define EVENT_TR69				'R'
#define EVENT_LINK				'L'
#define EVENT_PHY_LINK			'l'
#define EVENT_SLAAC				'S'
#define EVENT_ADSL_LINK			'A'
#define EVENT_ADSL_UNLINK		'a'
#if defined(DIR_BCMIAD)
#define EVENT_WLAN_RESET		'w'
#endif
#ifdef DLINK_3G
#define EVENT_NO_DEFAULT_ON_3G	'G'
#define EVENT_DEFAULT_ON_3G		'g'
#endif
#define EVENT_DEFAULT_ROUTE		'r'


#ifdef BCM47XX
#define EVENT_WPSWIFI_RESTART		'W'
#define EVENT_BCMWPS_START			'b'
#endif

#ifdef RLX
#define EVENT_RLXWPS_START			'b'
#endif

#ifdef BRCM_CMS_BUILD
#ifndef OLD_SDK
#define EVENT_WDG_UP_DOWN_INT	'W'
#endif 
#endif
#define LENGTH_DATA_SIZE 3

int init_resident_events(void);
int check_resident_events(void);

#endif
