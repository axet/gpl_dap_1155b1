#ifndef _CONF_TELNETD_H_
#define _CONF_TELNETD_H_

#ifdef SUPPORT_TELNETD
	struct outgoin* conf_telnetd(char* buf, int id);
#endif

#endif
