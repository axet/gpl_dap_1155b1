#ifndef _F_ROUTE_
#define _F_ROUTE_

#ifndef COMMON_CONFIG
int _eval(char *const argv[], char *path, int timeout, int *ppid);
#define eval(cmd, args...) ({ \
	char *argv[] = { cmd, ## args, NULL }; \
	_eval(argv, NULL, 0, NULL); \
})
#define IFUP (IFF_UP | IFF_RUNNING | IFF_BROADCAST | IFF_MULTICAST)
#define sin_addr(s) (((struct sockaddr_in *)(s))->sin_addr)
#define schar sizeof(char)

#if 0
static int route_manip(int cmd, char *name, int metric, char *dst, char *gateway, char *genmask);
int route_add(char *name, int metric, char *dst, char *gateway, char *genmask);
int route_del(char *name, int metric, char *dst, char *gateway, char *genmask);
#endif
int ifconfig(char *name, int flags, char *addr, char *netmask);
void config_loopback(void);

#endif

json_t *get_statistic (char *name);
char *np_extract_value(const char *varlist, const char *name, char sep);

#ifdef BRCM_CMS_BUILD
#ifdef DLINK_3G
void get_ip_und_mask_bridge (char *ip, char *mask);
#define _OPT_FILE_ "/tmp/optfile3g"
#define _OBJ_FILE_ "/tmp/3gobjectjson"
//int start_3g_ppp (json_t *object, char *name);
#ifndef OLD_SDK
int start_3g_ppp (char *name);
int stop_3g_ppp (UBOOL8 sovsem);
int init_dongle(char *action, char *vendor, char *product, char *product_name, char *type_usb);
int set_mode3g();
#endif
#endif
#endif
#endif //_F_ROUTE_
