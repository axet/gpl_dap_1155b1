#ifndef _FILE_UTILS_H_
#define _FILE_UTILS_H_

int buffer_to_file(char *buffer, long size, long seek,  char *file_name);
int file_to_buffer(char *buffer, long size, char *file_name);
long get_file_size(char *file_name);


#endif
