#ifndef CONF_PPPOED_H
#define CONF_PPPOED_H

#ifdef PPPOED
int apply_pppoed(json_t *config);
struct outgoin *conf_pppoed(char *buf, int id);
#endif

#endif
