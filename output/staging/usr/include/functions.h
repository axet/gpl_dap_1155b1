/********************************************************************/
/* File name: functions.h                                           */
/********************************************************************/

#ifndef __FUNCTIONS_H__
#define __FUNCTIONS_H__

/*------------------------------------------------------------------*/
struct parameters *get_lang(const char *filename);
struct parameters *get_version(const char *filename);
void pick_special_params(char* name, char* value);
struct menu_info* find_menu_info(void);

/*------------------------------------------------------------------*/

#endif /* __FUNCTIONS_H__ */

/********************************************************************/
/* End of file                                                      */
/********************************************************************/

