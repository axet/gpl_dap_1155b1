#ifndef _AR7240_VLAN_CONF_H_
#define _AR7240_VLAN_CONF_H_

#define NEW_VLANS

/* Embedded */
#ifndef NOT_EMBEDDED
#include "ar7240.h"
#include "ar7240_vlan_ioctl.h"
#endif
#include <jansson.h>

int ar7240_vlan_configuration(json_t *vlans);
void create_ifaces_section(json_t *config);
#ifdef NOT_EMBEDDED
#include <stdio.h>
void test_is_function();
void test_nat_iface();
void test_generate_port();
void test_tag_port();
void test_configuration();
#endif

/* Заглушки */
#ifdef NOT_EMBEDDED
int br_add(char *name);
int br_addif(char *brname, char *ifname);
int add_vlan(char *iface, int vid);
void vlan_open();
void vlan_set_iface(const char *iface);
void vlan_enable();
void vlan_addports(int  vid, int portnum);
void vlan_delports(int vid, int portnum);
void vlan_setmode(int mode, int portnum);
#endif

#endif
