#ifndef CONF_IPFILTER_H
#define CONF_IPFILTER_H

#ifdef IPFILTER
int basic_filter_apply(json_t *config);
struct outgoin *conf_ipfilter(char *buf, int id);
#endif

#endif
