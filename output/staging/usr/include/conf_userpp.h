#ifndef CONF_USERPP_H
#define CONF_USERPP_H

#if (defined PPTPD) || (defined PPPOED) || (defined L2TPD)

int apply_userpp(json_t *config);
struct outgoin *conf_userpp(char *buf, int id);

#endif

#endif
