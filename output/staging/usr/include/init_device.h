#ifndef _INIT_DEVICE_H_
#define _INIT_DEVICE_H_

int init_device(void);
int init_lan(json_t* lanconf, json_t *config);
#endif
