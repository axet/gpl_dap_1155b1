#ifndef __ROUTING_H__
#define __ROUTING_H__

#define DEFAULT_METRIC "0"

#ifdef ROUTING
void read_routing(char* strings[], struct records* dest);
bool save_routing(char* strings[], struct records* source, int pos);
void free_routing(struct records* source, int pos);

struct routing_record{
	char* ip;
	char* mask;
	char* gw;
	char* met;
	char* ifaces;
	bool del;
};

bool is_unique_route(struct records* struct_rect,struct routing_incoming* rec,int pos);
#endif

#endif
