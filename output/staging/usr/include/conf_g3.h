#ifndef _CONF_G3_H_
#define _CONF_G3_H_

#define CONNECT_3G  CONF_PREF "etc/init.d/scripts/3g connect > /dev/console 2>&1"
#define DISCONNECT_3G  CONF_PREF "etc/init.d/scripts/3g disconnect > /dev/console 2>&1"
#define PROBE_3G  CONF_PREF "etc/init.d/scripts/3g probe > /dev/console 2>&1"
#define START_3G  CONF_PREF "etc/init.d/scripts/3g start > /dev/console 2>&1"
#define STOP_3G  CONF_PREF "etc/init.d/scripts/3g stop > /dev/console 2>&1"

//коды возврата try_g3_connect
#define TRY_G3_CONNECT_OK 0
#define TRY_G3_CONNECT_UNKNOWN_DEVICE 1
#define TRY_G3_CONNECT_ERROR 2
#define TRY_G3_CONNECT_SIM_PASSWORD 3
#define TRY_G3_CONNECT_INIT 4

#define G3_DEV_ORTHODOX G3_DEV_MOTOROLA_C650

#define TTY3G "/dev/AT"
#define AT_OK 0

#ifdef G3
typedef void* (*g3_connect_)(const char*, speed_t, enum g3_devtype, unsigned int);
typedef	int  (*g3_disconnect_)(void*);
typedef	int  (*g3_get_vendor_)(void*, char*, size_t);
typedef	int  (*g3_get_model_)(void*, char*, size_t);
typedef	int  (*g3_get_revision_)(void*, char*, size_t);
typedef	int  (*g3_get_imsi_)(void*, char*, size_t);
typedef	int  (*g3_get_imei_)(void*, char*, size_t);
typedef	int  (*g3_get_battery_)(void*, int*, int*);
typedef	int  (*g3_get_signal_)(void*, int*, int*, int*, int*);
//typedef	int  (*g3_get_operator_)(void*, int*, int*, char*, size_t);
typedef int  (*g3_get_operator_)(void*, enum g3_oper_mode*, enum g3_oper_format*, char*, size_t, enum g3_oper_acc_type*);
typedef	int  (*g3_set_operator_)(void*, enum g3_oper_mode, enum g3_oper_format, const char*, enum g3_oper_acc_type);
//typedef	int  (*g3_send_sms_)(void*, const char*, unsigned char*, unsigned, int*);
typedef int (*g3_send_sms2_)(void *handle, const char *number, const u8 *utf8_text, unsigned flags, sms_ref_t backrefs[256]);
//typedef	int  (*g3_sms_delete_)(void*, const enum g3_sms_storage, unsigned, enum g3_sms_delete_flag);
typedef	int  (*g3_delete_sms2_by_backrefs_)(void*, enum g3_sms_storage, sms_ref_t backrefs[256]);
typedef int  (*g3_get_pinstatus_)(void *handle, struct g3_pin_ext_status *pin_ext_status);
typedef int  (*g3_set_pin_)(void *handle, const char *pin, const char *newpin);
//typedef int  (*g3_sms_list_)(void *handle, const enum g3_sms_storage storage, const unsigned int state, unsigned int flags, struct g3_sms_list *sms_list);
typedef int  (*g3_read_sms2_)(void *handle, enum g3_sms_storage storage, enum g3_sms_state state, unsigned int flags, struct g3_sms2_list *sms2_list, size_t *num_of_unparsed, sms_ref_t *backrefs);
//typedef int  (*g3_free_sms_list_)(struct g3_sms_list *sms_list);
typedef int  (*g3_free_sms2_list_)(struct g3_sms2_list *sms2_list);
typedef int (*g3_set_clck_)(void *handle, int pin_on_boot, const char *pin);
typedef int (*g3_get_clck_)(void *handle,int *pin_on_boot);
//typedef void (*__g3_sms_free_)(struct g3_sms *sms);
#ifdef G3_PHONEBOOK
typedef int  (*g3_phonebook_entries_read_)(void*, int, int, struct g3_phonebook_entries_list *e_list);
typedef int  (*g3_phonebook_entries_write_)(void*, const struct g3_phonebook_entry *entry);
typedef	int  (*g3_phonebook_entries_delete_)(void*, int);
typedef int  (*g3_phonebook_entries_free_list_)(struct g3_phonebook_entries_list *e_list);
typedef int  (*g3_phonebook_get_storage_)(void*, struct g3_phonebook_storage *storage);
typedef int  (*g3_set_charset_)(void*, enum g3_charset);
typedef enum g3_charset  (*g3_charset_name2enum_)(const char*);
#endif
typedef int  (*g3_sms_get_counters_)(void*, struct g3_cpms cpms[3]);
struct g3_model{
	char* vendor;
	char* model;
	enum g3_devtype type;
};

struct outgoin* g3_conf(char* buf,int id);
int try_g3_connect(void* hlib, void** hdev, enum g3_devtype* devtype, int* pinstatus);
int ucs2_to_utf8(char* dst, const char* src, size_t size, size_t* dst_size, char sym);
int utf8_to_ucs2(char* dst, const char* src, size_t size, size_t* dst_size, char sym, int byte_order_le);
void read_ppp_peers(char* strings[], struct records* dest);
struct outgoin* g3_ppp_setup_conf(char* buf,int id);
void free_ppp_peers(struct records* source, int pos);
bool save_ppp_peers(char* strings[], struct records* source, int pos);
#endif //G3

#ifdef G3_SMS
enum sms_ie_types {
	IEI_SEGMENTED_MESSAGE=0
};
struct sms_udh{
	char udhl;
	char* body;
};
struct sms_udh_ie_segmented{
	char id;
	char num;
	char inx;
};
struct sms_udh_ie_common{
	enum sms_ie_types iei;
	char iedl;
	char* body;
};
#endif

#endif
