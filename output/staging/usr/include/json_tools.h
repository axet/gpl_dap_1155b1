#ifndef _JSON_TOOLS_
#define _JSON_TOOLS_
#include "jansson.h"
json_t* json_object_get_by_fullname (json_t* base, const char* path);
int json_get_child_keys_of_obj_by_path (json_t* curr_obj,const char *path,char* result);
int get_string_from_json_by_name(json_t *root, char *name, char *buffer);
int get_integer_from_json_by_name(json_t *root, char *name, int buffer);
void get_param_rec(char* src,char* param_name, char* result);
//char *np_extract_value(const char *varlist, const char *name, char sep);
int generate_default_config(const char *filename);
#endif //_JSON_TOOLS_
