#ifndef FIREWALL_RULE_H
#define FIREWALL_RULE_H

#include "lib_config.h"
//#include "jansson-util.h"

void clear_chain(const char *chain_name);
void clear_all_chain();

void set_default_rule();

void add_new_chain(const char *chain_name);
int add_rule_in_chain(json_t *firewallconfig,json_t *rule);

void delete_rule_in_chain(json_t *firewallconfig, json_t *rule);
void delete_rule_by_number(json_t *firewallconfig, const int numberchain);

void change_rule_in_chain(json_t * firewallconfig, json_t *newrule, const int numberchain);
void change_between_chain(json_t *firewallconfig, const int oldposition, const int newposition);

#endif
