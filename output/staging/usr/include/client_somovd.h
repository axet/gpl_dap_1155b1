#ifndef _CLIENT_SOMOVD_H_
#define _CLIENT_SOMOVD_H_

#ifdef IN_WINDOWS

	#ifdef BUILD_DLL
		#define DLL_EXPORT __declspec(dllexport)
	#else
		#define DLL_EXPORT __declspec(dllimport)
	#endif

#else // IN_WINDOWS

	#define DLL_EXPORT

#endif // IN_WINDOWS

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef NO_SOMOVD_C
enum client_id{
	CLIENT_UNKNOWN=0,
	CLIENT_WEBADMIN,
	CLIENT_CLI,
	CLIENT_DCC,
	CLIENT_TR
};
#endif
//!2220939
#ifdef REENTERABLE_CLIENT
struct reenterable_addition{
	char* resident_addr;
	char* resident_login;
	char* resident_pass;
	int req_rpc_version;
	int auth_ok;
	int resident_is_dead;
	int SizeOfReceiveData;
};
#else
extern char* resident_addr;
extern char* resident_pass;
extern int auth_ok;
#endif

struct request{
	int size_of_buf;
	int size_of_data;
	void* buf;
};

struct answer_struct{
	int status;
	void* buf;
};
#ifdef REENTERABLE_CLIENT
struct request* DLL_EXPORT CreateRequest(int rpc,int req_rpc_version);
#else
void DLL_EXPORT set_resident_client(enum client_id); // проверяем умер ли резидент.
int DLL_EXPORT is_resident_dead(void); // проверяем умер ли резидент.
void DLL_EXPORT set_auth_data(const char* login, const char* pass); // устанавливаем логин и пароль
int DLL_EXPORT get_auth_state(void); // проверяем успешность авторизации после выполненного запроса к резиденту.
void DLL_EXPORT set_resident_addr(const char* addr); // задаем адрес резидента ( не прокси!!!)
void DLL_EXPORT set_resident_selected(const char* addr); // задаем идентификатор девайса при работе с проксей
int DLL_EXPORT get_selected_state(void); // проверяем удалось ли выпрать девайс у прокси, выставляется после запроса
void DLL_EXPORT set_req_rpc_version(int ver); // выставляем версию запрашиваемой RPC
struct request* DLL_EXPORT CreateRequest(int rpc);
#endif
void DLL_EXPORT AddToRequest(struct request* req,const void* buf,int size_of_buf,char* separate);
#define AddStrToRequestWithoutSeparator(req,buf) AddToRequest(req,(void*)buf,0,NULL)
#define AddStrToRequestWithSeparator(req,buf) AddToRequest(req,(void*)buf,0,RPC_PROTOCOL_SEPARATOR)
#define AddStrToRequestWithZeroSeparator(req,buf) AddToRequest(req,buf,0,"\0")
#define AddToRequestWithoutSeparator(req,buf,size_of_buf) AddToRequest(req,buf,size_of_buf,NULL)
#define AddToRequestWithSeparator(req,buf,size_of_buf) AddToRequest(req,buf,size_of_buf,RPC_PROTOCOL_SEPARATOR)
void DLL_EXPORT AddStructToRequest(struct request* req,void** buf,size_t size_of_struc);
void DLL_EXPORT AddIdWithIndexToRequest(struct request* req,char* id,int index);

void DLL_EXPORT BuildIncomingStruct(void* buf,char** IncomingStruct);
struct records* DLL_EXPORT BuildIncomingStructEx(void* buf,size_t count_byte);
char* DLL_EXPORT GetParamFromReceiveBuffer(char* buf,int num,int count);

#ifdef REENTERABLE_CLIENT
char* DLL_EXPORT CallRPCNoCopy(int rpc,const void* in, size_t size, struct answer_struct* ans,struct reenterable_addition* re_ad);
void DLL_EXPORT CallRPCNonBlocking(int rpc,char* buf,struct reenterable_addition* re_ad);
char* DLL_EXPORT ReadFromDaemon(int config,struct answer_struct* ans,struct reenterable_addition* re_ad);
struct records* DLL_EXPORT ReadFromDaemonEx(int config,size_t size,struct answer_struct* ans,char** answer,struct reenterable_addition* re_ad);
int DLL_EXPORT WriteToDaemonEx(int config,int pos,void** param_struct,int size,struct answer_struct* ans,char** answer,char* addition,struct reenterable_addition* re_ad);
int DLL_EXPORT DeleteOnDaemonEx(int config,int pos,void** param_struct,int size,struct answer_struct* ans,char** answer,char* addition,struct reenterable_addition* re_ad);
int DLL_EXPORT DeleteOnDaemon(int config,int pos,struct answer_struct* ans,char** answer,struct reenterable_addition* re_ad);
#else //REENTERABLE_CLIENT
char* DLL_EXPORT CallRPCNoCopy(int rpc,const void* in, size_t size, struct answer_struct* ans);
int DLL_EXPORT CallRPC(int rpc, const void* in, size_t size, void* out);
#define CallRPCSimple(rpc) CallRPC(rpc, NULL, 0, NULL)
#define CallRPCSimpleStr(rpc, in) CallRPC(rpc, in, 0, NULL)
void DLL_EXPORT CallRPCNonBlocking(int rpc,char* buf);
#define CallRPCNonBlockingSimple(rpc) CallRPCNonBlocking(rpc,NULL)
char* DLL_EXPORT ReadFromDaemon(int config,struct answer_struct* ans);
struct records* DLL_EXPORT ReadFromDaemonEx(int config,size_t size,struct answer_struct* ans,char** answer);
#define WriteToDaemon(config,pos,param_struct,size,ans,answer) WriteToDaemonEx(config,pos,param_struct,size,ans,answer,NULL)
int DLL_EXPORT WriteToDaemonEx(int config,int pos,void** param_struct,int size,struct answer_struct* ans,char** answer,char* addition);
int DLL_EXPORT DeleteOnDaemonEx(int config,int pos,void** param_struct,int size,struct answer_struct* ans,char** answer,char* addition);
int DLL_EXPORT DeleteOnDaemon(int config,int pos,struct answer_struct* ans,char** answer);
#endif //REENTERABLE_CLIENT
void DLL_EXPORT kill_incoming_struct(void* incoming, size_t size);

#ifdef __cplusplus
}
#endif

#endif

