#ifndef _RESOLV_H_
#define _RESOLV_H_
#ifdef DNS
void read_nameval(char* strings[], struct records* dest);
void nv_kill_callback(struct records* source, int pos);
struct nameval{
	char *name;
	char *val;
};
#endif
#endif
