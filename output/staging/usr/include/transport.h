#ifndef _TRANSPORT_H_
#define _TRANSPORT_H_

int set_resident_proxy_addr(const char* addr);
int create_daemon_socket(void);
int create_local_socket(void);
int create_udp_mcast_socket(void);
int multicast_re_add(int sock);
int mcast_proc_do(int sock);
#ifdef SOMOVD_SECURE_SSL
SSL_CTX* create_daemon_ssl_ctx(void);
int init_daemon_ssl(SSL** ssl,SSL_CTX* ctx,int fd);
char* receive_data(int from,SSL* ssl_from);
void send_data(int to,SSL* ssl_to,char* buf,int count);
#else
char* receive_data(int from);
void send_data(int to,char* buf,int count);
#endif
#ifdef SECRET_HANDSHAKE
int somovd_handshake(int sock_fd);
#endif

#endif
