/********************************************************************/
/* File name: ifgroups.h                                           */
/********************************************************************/

#ifndef __IFGROUPS_H__
#define __IFGROUPS_H__

#ifdef IFGROUPS
void read_ifgroups(char* strings[], struct records* dest);
bool save_ifgroups(char* strings[], struct records* source, int pos);
void free_ifgroup(struct records* source, int pos);
int get_default_group(char** group, struct records* ifgroups);
void refresh_virtual_ports_info(void);

struct ifgroup_record{
	char* name;
	char* ifaces;
	bool del;
};

#endif

#endif // __IFGROUPS_H__ 

/********************************************************************/
/* End of file                                                      */
/********************************************************************/
