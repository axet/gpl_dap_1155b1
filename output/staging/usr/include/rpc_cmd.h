#ifndef _RPC_CMD_H_
#define _RPC_CMD_H_

#define TFTP_TIMEOUT_PERIOD 5
#define TFTP_TIMEOUT_MAX   15
#define TFTP_RETRIES_MAX    5
/*
 * Error codes for tftp. May be need in future.
 */
#define	EUNDEF		0		/* not defined */
#define	ENOTFOUND	1		/* file not found */
#define	EACCESS		2		/* access violation */
#define	ENOSPACE	3		/* disk full or allocation exceeded */
#define	EBADOP		4		/* illegal TFTP operation */
#define	EBADID		5		/* unknown transfer ID */
#define	EEXISTS		6		/* file already exists */
#define	ENOUSER		7		/* no such user */

#define RRQ 1
#define WRQ 2
#define DATA 3
#define ACK 4
#define ERR 5

#define FILE_NOT_FOUND 1

struct tftpPacket{
	short opcode;
	union {
		char bytes[514];
		struct{
			short code;
			char message[200];
			}error;
		struct{
			short block;
			char bytes[512];
			}data;
		struct{
			short block;
			}ack;
		}u;
	};
#endif
