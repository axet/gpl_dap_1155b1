#ifndef _SOMOVD_H_
#define _SOMOVD_H_

#define SOMOVD

#ifdef BRCM_CMS_BUILD
#define LIBRCM
#define BCM
#define ADSL
#define DSL_WAN
#define IFGROUPS
#else
#define ROUTERS
#define SCHEDULER
#endif

#define DEBUG_LOGGING_KOSTYL

#define SAVE_AND_REBOOT_ON
#define SAVE_CONFIG_ON
#define RESET_TO_DEFAULT_ON
#define RESET_AND_REBOOT_ON
#define RESTORE_CONFIG_ON
#define BACKUP_CONFIG_ON
#define FIRMWARE_UPLOAD_ON
#define FIRMWARE_UPDATE_ON

#ifdef SUPPORT_WIFI
#define WIFI
#define WIFI_EXT
#ifdef SUPPORT_FON
#define FON
#endif
#endif

#ifdef SUPPORT_3G
#define G3
#define G3_PIN
#define G3_PPP
#define G3_SMS
#define G3_PHONEBOOK
#ifdef BCM
#define ENABLE_USB_HOTPLUG 
#endif
#endif

#ifdef SUPPORT_WIMAX
#define WIMAX
#endif

#ifdef SUPPORT_STORAGE
#define STORAGE
#define MASS_STORAGE
#ifdef SUPPORT_TORRENT
#define TORRENT
#endif
#endif

#ifdef DLINK_WATCHDOG
#define WATCHDOG
#endif

#ifdef SUPPORT_VOIP
#define VOIP
#endif

#ifdef SUPPORT_IGMP
#define IGMP
#endif

#ifdef SUPPORT_DLINK_IGMPPROXY
#define IGMP
#endif

#define ARP_TABLE
#define HTTPACCESS
#define VSERVERS
#define MAC_FILTER
#define DMZ
#define DDNS
#define STATUS_DSL
#define STATUS_WAN
#define DSL_LAN
#define SIMPLE_CONFIG
#define BRFILTERS
#define DHCP
#define SDHCP
#define DHCP_LEASES
#define ROUTE_TABLE
#define UPNP
#define URL_FILTER
#define APPLICATION_RULES
#define IPFILTER
#define REDIRECTPORT
#define ROUTING

#ifdef SUPPORT_THEMES
#define THEMES
#endif
#ifdef SUPPORT_TR069
#define DLINK_TR069
#endif

#include "somovd_def.h"
#include "scheduler.h"
#include "somovd_shared_struct.h"
#undef SOMOVD
/*------------------------------------------------------------------*/
/* Макро-определения                                */
/*------------------------------------------------------------------*/
#define STR_SIZE 1024
#define BUFLEN 4096
#define RECEIVE_BUFLEN 2048
#define SEND_BUFERLEN 2048
#define SOMOVD_DATA_WAITING 3000 // 3 сек
#define CLIENT_DATA_WAITING 240000 // 240 сек
#define TRUE 1
#define FALSE 0

#define PARALLEL_PROCESSING y
#define INET_DAEMON y

#ifdef PARALLEL_PROCESSING
//#define NON_BLOCKING_RPC_FEATURE
#define RESIDENT_EVENTS
#define WAITING_FOR_CONNECTION 2000 // 2 сек
#else
#define RPC_EXECTION_FORK
#endif
#ifdef INET_DAEMON
#define DAEMON_PORT 8888
#define SECRET_HANDSHAKE
#endif //INET_DAEMON
#ifdef HAVE_LIBSSL
#define SOMOVD_SECURE_SSL
#endif//HAVE_LIBSSL
#define SECRET_SEQUENCE "ToP sEcReT sEqUeNcE"
#define LEN_RAND_SEQ 32
#define HANDSHAKE_RES_LEN 6
#define HANDSHAKE_OK "OK"
#define HANDSHAKE_FAIL "FAIL"
#define PASS_FILE_PATH "/var/default/somovd_passwd"
#define SOMOVD_PASS_MAX_LEN 64

#ifdef DEBUG_LOGGING
#define SOMOVD_CLI
#define AUTO_RM_ALL_SOMOVD_FILES
//#define	PARAM_VERSION	1
//#define RPC_PROTOCOL_SEPARATOR "%"
#define	PARAM_VERSION	2
#define RPC_PROTOCOL_SEPARATOR "\0"
#else
#define PARAM_VERSION   2
#define RPC_PROTOCOL_SEPARATOR "\0"
#endif

#define PROTO_VERSION (PARAM_VERSION+1)

#define PROTO_VERSION_JSON (PROTO_VERSION+10)

#define UNIX_SOCKET_PATH "/tmp/somovd.socket" // Это сокет создаваемый демоном

#define SOMOVD_FILE_PREFIX "/var/log/somovd_"

#ifdef DAP_1360D1_BEZEQ
#define ADMIN_NAME "Admin"
#elif defined(DIR_300AE_KS)
#define ADMIN_NAME "kyivstar"
#else
#define ADMIN_NAME "admin"
#endif

#ifdef NON_BLOCKING_RPC_FEATURE
struct u_rpc_id{
	int type;
	int rpc;
	int uid;
};
#endif
enum client_id{
	CLIENT_UNKNOWN=0,
	CLIENT_WEBADMIN,
	CLIENT_CLI,
	CLIENT_DCC,
	CLIENT_TR
};
#define _GNU_SOURCE
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <regex.h> 
#include <ctype.h>
#include <time.h>
#include <stdbool.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>
#include <assert.h>
#include <fnmatch.h>
#include <pwd.h>
#include <time.h>
#include <math.h>
#include <stdarg.h>
//#include <net/if.h>
#include <signal.h>
#include <dlfcn.h>
#include <glob.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/sysinfo.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/ioctl.h>
#include <sys/wait.h>
#include <sys/user.h>
#include <sys/poll.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/mman.h>
#ifdef SOMOVD_SECURE_SSL
#include <openssl/ssl.h>
#include <openssl/err.h>
#endif //SOMOVD_SECURE_SSL
#include <syslog.h>
#include <sched.h>

#endif
