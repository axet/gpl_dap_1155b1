/********************************************************************/
/* File name: main.h                                                */
/* ---------------------------------------------------------------- */
/* Данный файл является главным заголовочным файлом                 */
/********************************************************************/

#ifndef __MAIN_H__
#define __MAIN_H__

#define _GNU_SOURCE
#define STR_SIZE 1024
#define BUFLEN 4096
#define TRUE 1
#define FALSE 0

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <regex.h> 
#include <ctype.h>
#include <time.h>
#include <stdbool.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>
#include <assert.h>
#include <fnmatch.h>
#include <pwd.h>
#include <time.h>
#include <math.h>
#include <stdarg.h>
#include <net/if.h>
#include <signal.h>
#include <dlfcn.h>
#include <glob.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/sysinfo.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/ioctl.h>
#include <sys/wait.h>
#include <sys/user.h>
#include <sys/poll.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/mman.h>
#include <syslog.h>
#include <termios.h>

#define SOMOVD
#include "client_somovd.h"
#include "somovd_def.h"
#include "somovd_shared_struct.h"
#include "somovd_cli.h"

#endif /* __MAIN_H__  */

/*******************************************************************/
/* End of file                                                     */
/*******************************************************************/
