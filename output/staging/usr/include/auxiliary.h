#ifndef _AUXILIARY_H_
#define _AUXILIARY_H_

char* GetParamFromReceiveBuffer(char* buf,int num,int count);
//char* GetParamFromReceiveBufferByName(char* buf,char* name,int count);
#if PARAM_VERSION==1
int GetCountOfParametersAndSeparate(char* buf,int limit);
#define SeparateParams(buf) GetCountOfParametersAndSeparate(buf,0x7fff)
#else
int SeparateParams(char* buf);
#endif

int getIndexOfConfigString(char* buf);
int getLengthOfReceiveStr(char* buf,int count);
void BuildIncomingStruct(void* buf,void** IncomingStruct, size_t count_byte);
struct records* BuildIncomingStructEx(void* buf,size_t count_byte);
void expand_records(struct records* recs, size_t recs_chunk, size_t rec_size);
void kill_incoming_struct(void* incoming, size_t size);
struct records* init_records(void);
void kill_records(struct records* recs);
void kill_records_ex(struct records* recs, void (*callback)(struct records* recs, int pos));
#endif
