#ifndef CONF_3G_INFO
#define CONF_3G_INFO

#ifdef G3

#define STATUS_FILE "/tmp/.3g_usb_status"
#define LTE_STATUS_FILE "/tmp/.lte_usb_status"
#define TTY_3G "/dev/AT"

struct outgoin *conf_3g_info(char *buf, int id);

#endif
#endif
