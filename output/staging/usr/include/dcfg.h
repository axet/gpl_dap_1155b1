#ifndef _dcfg_h_
#define _dcfg_h_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* common */
void logmessage(char *logheader, char *fmt, ...);

#endif
