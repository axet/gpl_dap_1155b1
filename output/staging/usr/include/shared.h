#ifndef _SHARED_H_
#define _SHARED_H_

#include <string.h>
/*
#include <signal.h>
#include <netinet/ether.h>
#include <linux/if.h>
*/
#include <sys/types.h>

/**
 * @file
 * Внешний заголовочный файл библиотеки libdhal.
 */

#define CONFIG_SEM_NUM 0

#define BEELINE_FLAG				0x0001
#define PPPOA_FLAG					0x0010

#define DMS_BACKUP_FILE "/tmp/config.tar.gz"
#define IF_NAMESIZE 16

#include <jansson.h>

#if defined(RALINK)
#include "ralink.h"
#endif
#if defined(AR7240)
#include "ar7240.h"
#include "unpack.h"
#endif
#if defined(BRCM)
#include "brcm.h"
#endif
#if defined(RALINK_MODEM)
#include "ralink_modem.h"
#endif
#if defined(RLX)
#include "rlx.h"
#include "rlx_vlan.h"
#endif
#if defined(BCM47XX)
#include "bcm47xx.h"
#endif

/**
 * Коды возврата функций.
 */
enum dns_return_values {
	DMS_OK,                 ///< успешное завершение
	DMS_ERROR,              ///< ошибочное завершение
	DMS_INVALID_PARAM,      ///< неверные входные параметры
	DMS_INTERNAL_ERROR      ///< внутренняя ошибка
};

/* Network */

//#define IFUP (IFF_UP | IFF_RUNNING | IFF_BROADCAST | IFF_MULTICAST)

enum device_mode_type      /* Router, Access Point, Client, Advansed mode */
{
	RT,
	CL,
	AP,
	ADV
};

enum ip_version_type      /* IP version*/
{
	DMS_IPV4,
	DMS_IPV6,
	DMS_IPV4_6
};
enum conn_type_tun      /* Connection type L3 */
{
	DMS_TUN_PPTP,
	DMS_TUN_L2TP
};

enum conn_type_l3      /* Connection type L3 */
{
	DMS_L3_AUTO,
	DMS_L3_IPOE,
	DMS_L3_PPP,
	DMS_L3_IPOE6,
	DMS_L3_PPP6,
	DMS_L3_BRIDGE
};

enum conn_type_l2      /* Connection type L2 */
{
	DMS_L2_AUTO,
	DMS_L2_ETHERNET,
	DMS_L2_3G,
	DMS_L2_EOA,
	DMS_L2_PPPOA,
	DMS_L2_IPOA,
	DMS_L2_LTE
};

enum conn_level      /* Connection level */
{
	DMS_L2,
	DMS_L3,
	DMS_TUN
};

enum dongle_type      /* Dongle type */
{
	DMS_CDMA,
	DMS_UMTS
};
enum addr_action_type
{
	ADD_ADDR,
	DEL_ADDR,
	FSH_ADDR
};


enum iface_action_type
{
	DMS_IFACE_DOWN,
	DMS_IFACE_UP,
	DMS_IFACE_LEASE
};

enum iface_proto_type
{
	DMS_PROTO_PPP,
	DMS_PROTO_DHCP
};

typedef struct _dms_link {
	int link_level;
        int ip_ver;
	int flags;
	json_t *config;
	char l2_key[32];
	char l3_key[32];
	char tun_key[32];
} dms_link_t;

typedef struct _iface_state {
	int type;
	int action;
	char iface[16];
	char ipaddr[16];
	char netmask[16];
	char gateway[16];
	char dns1[16];
	char dns2[16];
} iface_state_t;


/* --- md5sum --- */  //переехал из резидента
typedef struct {
  int abcd[4];                                   /* state (ABCD) */
  int count[2];        /* number of bits, modulo 2^64 (lsb first) */
  unsigned char buf[64];                         /* input buffer */
} md5_state_t;
typedef unsigned char md5_byte_t;
typedef unsigned int md5_word_t;

void md5_init(md5_state_t *pms);
void md5_append(md5_state_t *pms, const md5_byte_t *data, int nbytes);
void md5_finish(md5_state_t *pms, md5_byte_t digest[]);
/* -------------- */

/* firmware */
int update_firmware(json_t *config, char *firmware_file);

#ifdef SUPPORT_AUTOUPDATE_FW
int autoupdate_start();
int autoupdate_init(json_t *config);
int autoupdate_fw_update(json_t *config);
#endif

#ifdef FIRMWARE_REMOTE_UPDATE_ON
int remote_update_firmware(json_t *config, char *firmware_file);
#endif //FIRMWARE_REMOTE_UPDATE_ON
int wget_to_file(const char *const server, const char *const filename);

/* configs */
extern void clear_ifaces_info(json_t *config);
extern json_t *dms_get_config(int force_read);
extern void dms_set_config(json_t *root_config, int check_pid);
extern int dms_check_config_by_default(void);
extern int dms_l3_lock_set(json_t *config, const char *l2_key, const char *l3_key, char *name);
extern int dms_l3_lock_clear(json_t *config, const char *l2_key, const char *l3_key, char *name);

/* service */
int update_vserver(json_t *config);
int ipaddr_ctrl(char * name, int family, int action, char * addr);
int show_addr(char *iface, int fl_flag);
json_t *find_link(json_t *ifaces, char *name, dms_link_t *link);

/* frouting */
int start_ip(dms_link_t *link);
int stop_ip(dms_link_t *link);
void ip_is_down(dms_link_t *link, int ip_type);
void ip_is_up(dms_link_t *link, int ip_type);
void ip_is_leasefail(dms_link_t *link_info, int ip_type);

int start_pppd(dms_link_t *link);
int stop_pppd(dms_link_t *link);
int dms_kill_pppd(void);
void ppp_is_up(dms_link_t *link);
void ppp_is_down(dms_link_t *link);

int start_pppd_3g (json_t* config, const char *name, const char *dongle_type);
int start_wimax(json_t *wimax);
int stop_wimax(json_t *wimax);
//int stop_pppd(json_t *obj, const char *name);
//int stop_ip(json_t *obj, const char *name, json_t *obj_l2);

int stop_wan_link_on_l2(json_t *l2_obj, const char *name);
//int start_wan_l3(dms_link_t *link);
int stop_wan_l3(dms_link_t *link);
int start_wan_l3(json_t *ifaces, const char *name);

int update_macfilter(json_t *macfilter);
int update_ddns(json_t *ddns);
int update_vserver(json_t *vserver);
int start_btpd(json_t* conf);
int stop_btpd(json_t* conf);
int update_flow_control(json_t *conf);

int init_dongle(char *action, char *vendor, char *product, char *product_name, json_t *config, char *name);

/*
void ppp_is_up(json_t *glconfig, json_t* obj,char* record, char* name);
void ppp_is_down(json_t *glconfig, json_t* obj,char* record, char* name, json_t *parent_obj, const char *parent_name);
void ip_is_up(json_t *glconfig, json_t* obj,char* record, char* name, json_t* obj_l2);
void ip_is_up(json_t *glconfig, json_t* obj, int ip_type, char* name, json_t* obj_l2);
void ip_is_up(dms_link_t *link);
void ip_is_down(json_t* glconfig, json_t* obj, char* record, char* name, json_t* obj_l2);
void ip_is_leasefail(json_t* glconfig, json_t* obj, char* record, char* name, json_t* obj_l2);
int init_dongle(char *action, char *vendor, char *product, json_t *usb_iface);

void get_base_mac(char *base_mac);
void get_wan_mac(char *wan_mac);
int get_mac_from_iface(char *name, char *macaddr);
int get_flag_from_iface(char *name);
*/

/* wifi */
//int set_wifi_settings_in_config(json_t *config);
int stop_wifi(json_t *config);
int start_wifi(json_t *config, int mode_bridge);
int get_wifi_ssid_channel(char *ssid, char *channel);
int set_wifi_ssid_channel(char *ssid, char *channel);
int stop_wps(json_t *config);

/* led */
int led_wan_status(int mode);
int led_power(int mode);
#ifdef SUPPORT_USB
int led_usb_status(int mode);
#endif

/* common set ifaces */

void preinit(json_t *config);
void init_pre_boot(void);
void init_post_boot(void);

/* Macs */
char *get_hwaddr(char *ifname);
void get_base_mac(char *base_mac);
void get_wan_mac(char *wan_mac);
int if_rename(char *ifname, char *new_ifname);

#if defined(RALINK)  //сделать под другие устройства
int get_default_pin(char *pin);
#endif

int get_device_mode(json_t *config);
json_t *get_device_info(void);

void start_upnp(json_t *config);
void stop_upnp(json_t *config);
#ifdef DNS_REDIRECT
#ifndef DNS_REDIRECT_TO_BASE_IP
#define REDIRECT_IP "192.0.2.1"
#endif
int start_httpproxy(const char *ipaddr, const char *netmask);
#endif

/*
int wget_to_file(const char *const server, const char *const filename);
int write_firmware(char *firmware_file);
*/
int check_firmware(char *firmware_file, char *device_name);

/* config */
int read_config_from_flash(const char* file_name);
int write_config_to_flash(const char* conf_name);
int erase_config_from_flash(void);
int backup_config(const char* conf_name);
int restore_config(const char* conf_name);

/* Utils */

void logmessage(char *logheader, char *fmt, ...);
void generate_ssh_key(void);
int touch_f(const char *path);
void dms_reboot(void);
int proc_list(char *process_line, int sz);
int get_string_from_json_by_name(json_t *root, char *name, char *buffer);
json_t* json_object_get_by_fullname (json_t* base, const char* path);
int killall_s(char *program, int sig);
int same_subnet(char *ip1, char *ip2, char *mask);

/* -----------------------------------------------
 * get_string_json - returns  a string from json`s tree
 * -----------------------------------------------
 * json - a json`s tree
 * count - count of node json`s tree
 * ... - names of node json`s tree
 * return - a string from the last node json`s tree
 * -----------------------------------------------
 * Example:
 *    We have following config:
 *      {"ifaces":{"br0":{"dhcp":{"enable":true} } } } 
 *    that get value node "enable" make: 
 *    char *value = get_string_json(json, 4, "ifaces","br0", "dhcp", "enable");
 * -----------------------------------------------
 * */
const char * get_string_json(const json_t *json,int count,...);
/* -----------------------------------------------
 * get_array_string_json - returns a string with the number of array json`s tree 
 * -----------------------------------------------
 * json - a json`s tree
 * index - a number of element json`s array
 * count - count of node json`s tree
 * ... - names of node json`s tree
 * return - a string from the last node with the number of json`s array
 * -----------------------------------------------
 * Example:
 *   We have following config:
 *     { "ifaces":{ "dhcp" : {"reserved" : ["192.168.1.21","192.168.1.34"] } } }
 *   that get value "192.168.1.21" make:
 *   char * value = get_array_string_json(json, 0, 3, "ifaces", "dhcp", "reserved");
 * ------------------------------------------------
 * */
const char* get_array_string_json(const json_t *json,const int index,int count,...);

/* -----------------------------------------------
 * set_string_json - set a string value  in the json`s tree
 * with cheking the last type.
 * -----------------------------------------------
 * json - a json`s tree
 * value - the string that will be set
 * count - count of node json`s tree
 * ... - names of node json`s tree
 * -----------------------------------------------
 * Example:
 *    We have following config:
 *       {"ifaces":{"br0":{"dhcp":{"enable":true} } } }
 *    that switch off dhcp  we have to do:
 *    set_string_json(json, "false", 4, "ifaces", "br0", "dhcp", "enable");
 * -----------------------------------------------
 * */
void set_string_json(const json_t *json,const char *value, int count, ...);

/* ----------------------------------------------
 * set_array_string_index - change a value of the array index.
 * Work with the string only!
 * ----------------------------------------------
 * json - a json`s tree
 * value - the string that will be set
 * index - an array index
 * count - count of node json`s tree
 * ... - names of node json`s tree
 * ---------------------------------------------
 * Example:
 *    We have followinf config:
 *    { "ifaces":{ "dhcp" : {"reserved" : ["192.168.1.21","192.168.1.34"] } } }
 *    and we want to change "192.168.1.34" on "192.168.1.30". So make:
 *    set_array_string_index(json, "192.168.1.30", 1, 3, "ifaces", "dhcp", "reserved");
 * ---------------------------------------------
 * */
void set_array_string_index(const json_t *json,const char *value, int index,int count,...);

/* --------------------------------------------
 * set_array_string_add - add a new string to the array json`s tree
 *  Work with the string only!
 * -------------------------------------------
 * json  - a json`s tree
 * value - a string that will be add
 * count - count of node json`s tree
 * ... - names of node json`s tree
 * ------------------------------------------
 * Example:
 *    We have following config:
 *    { "ifaces":{ "dhcp" : {"reserved" : ["192.168.1.21","192.168.1.34"] } } }
 *    and we want to add a new ip to the array. So  make:
 *    set_array_string_add(json,"192.168.1.100",3 "ifaces", "dhcp", "reserved")
 * ------------------------------------------
 * */
void set_array_string_add(const json_t *json,const char *value, int count,...);
int dms_json_is_array_has_string(json_t *arr, char *str, int *index);

void load_module(char *prefix, char *modules_list);
char *generate_key(int size, int type);

/*
 * Checks if file exists
 * @param	fd	file descriptor
 * @return	return st_mode field (0 if not exists)
 */
//extern mode_t exists(const char *path);

/*
 * Reads file and returns contents
 * @param	fd	file descriptor
 * @return	contents of file or NULL if an error occurred
 */
extern char * fd2str(int fd);

/*
 * Reads file and returns contents
 * @param	path	path to file
 * @return	contents of file or NULL if an error occurred
 */
extern char * file2str(const char *path);

/* 
 * Waits for a file descriptor to become available for reading or unblocked signal
 * @param	fd	file descriptor
 * @param	timeout	seconds to wait before timing out or 0 for no timeout
 * @return	1 if descriptor changed status or 0 if timed out or -1 on error
 */
extern int waitfor(int fd, int timeout);

/* 
 * Concatenates NULL-terminated list of arguments into a single
 * commmand and executes it
 * @param	argv	argument list
 * @param	path	NULL, ">output", or ">>output"
 * @param	timeout	seconds to wait before timing out or 0 for no timeout
 * @param	ppid	NULL to wait for child termination or pointer to pid
 * @return	return value of executed command or errno
 */
extern int _eval(char *const argv[], char *path, int timeout, pid_t *ppid);

/* 
 * Concatenates NULL-terminated list of arguments into a single
 * commmand and executes it
 * @param	argv	argument list
 * @return	stdout of executed command or NULL if an error occurred
 */
extern char * _backtick(char *const argv[]);

/* 
 * Kills process whose PID is stored in plaintext in pidfile
 * @param	pidfile	PID file
 * @return	0 on success and errno on failure
 */
int kill_pidfile_s(char *pidfile, int sig);

int kill_pidfile(char *pidfile);

/*
 * Gets pid of process from plaintext in pidfile
 * @param       pidfile PID file
 * @return      pid on success and 0 on failure
 */
int get_pid_from_file(char *pidfile);

/*
 * fread() with automatic retry on syscall interrupt
 * @param	ptr	location to store to
 * @param	size	size of each element of data
 * @param	nmemb	number of elements
 * @param	stream	file stream
 * @return	number of items successfully read
 */
extern int safe_fread(void *ptr, size_t size, size_t nmemb, FILE *stream);

/*
 * fwrite() with automatic retry on syscall interrupt
 * @param	ptr	location to read from
 * @param	size	size of each element of data
 * @param	nmemb	number of elements
 * @param	stream	file stream
 * @return	number of items successfully written
 */
extern int safe_fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream);

/*
 * Convert Ethernet address string representation to binary data
 * @param	a	string in xx:xx:xx:xx:xx:xx notation
 * @param	e	binary data
 * @return	TRUE if conversion was successful and FALSE otherwise
 */
extern int ether_atoe(const char *a, unsigned char *e);

/*
 * Convert Ethernet address binary data to string representation
 * @param	e	binary data
 * @param	a	string in xx:xx:xx:xx:xx:xx notation
 * @return	a
 */
extern char * ether_etoa(const unsigned char *e, char *a);

/*
 * Convert Ethernet address binary data to string representation
 * @param       e       binary data
 * @param       a       string in xxxxxxxxxxxx notation
 * @return      a
 */
extern char * ether_etoa2(const unsigned char *e, char *a);

 /* convert mac address format from XXXXXXXXXXXX to XX:XX:XX:XX:XX:XX */
extern char *mac_conv(char *mac, char *buf);

/*
 * Concatenate two strings together into a caller supplied buffer
 * @param	s1	first string
 * @param	s2	second string
 * @param	buf	buffer large enough to hold both strings
 * @return	buf
 */
static inline char * strcat_r(const char *s1, const char *s2, char *buf)
{
	strcpy(buf, s1);
	strcat(buf, s2);
	return buf;
}	



/*
 * Parse the unit and subunit from an interface string such as wlXX or wlXX.YY
 *
 * @param	ifname	interface string to parse
 * @param	unit	pointer to return the unit number, may pass NULL
 * @param	subunit	pointer to return the subunit number, may pass NULL
 * @return	Returns 0 if the string ends with digits or digits.digits, -1 otherwise.
 *		If ifname ends in digits.digits, then unit and subuint are set
 *		to the first and second values respectively. If ifname ends 
 *		in just digits, unit is set to the value, and subunit is set
 *		to -1. On error both unit and subunit are -1. NULL may be passed
 *		for unit and/or subuint to ignore the value.
 */
//extern int get_ifname_unit(const char* ifname, int *unit, int *subunit);



/* 
		remove_from_list
		Remove the specified word from the list.

		@param name word to be removed from the list
		@param list List to modify
		@param listsize Max size the list can occupy

		@return	error code
*/
extern int remove_from_list( char *name, char *list, int listsize );

/*
		add_to_list
		Add the specified interface(string) to the list as long as
		it will fit in the space left in the list.  

		@param name Name of interface to be added to the list
		@param list List to modify
		@param listsize Max size the list can occupy

		@return	error code
*/
extern int add_to_list( char *name, char *list, int listsize );

/* Strip trailing CR/NL from string <s> */
#define chomp(s) ({ \
	char *c = (s) + strlen((s)) - 1; \
	while ((c > (s)) && (*c == '\n' || *c == '\r')) \
		*c-- = '\0'; \
	s; \
})

/* Simple version of _backtick() */
#define backtick(cmd, args...) ({ \
	char *argv[] = { cmd, ## args, NULL }; \
	_backtick(argv); \
})

/* Simple version of _eval() (no timeout and wait for child termination) */
#define eval(cmd, args...) ({ \
	char *argv[] = { cmd, ## args, NULL }; \
	_eval(argv, ">/dev/null", 0, NULL); \
})

/* Copy each token in wordlist delimited by space into word */
#define foreach(word, wordlist, next) \
	for (next = &wordlist[strspn(wordlist, " ")], \
	     strncpy(word, next, sizeof(word)), \
	     word[strcspn(word, " ")] = '\0', \
	     word[sizeof(word) - 1] = '\0', \
	     next = strchr(next, ' '); \
	     strlen(word); \
	     next = next ? &next[strspn(next, " ")] : "", \
	     strncpy(word, next, sizeof(word)), \
	     word[strcspn(word, " ")] = '\0', \
	     word[sizeof(word) - 1] = '\0', \
	     next = strchr(next, ' '))

/* Return NUL instead of NULL if undefined */
#define safe_getenv(s) (getenv(s) ? : "")

/* Remove space in the end of string */
char *trim_r(char *str);

/* Samba */
#ifdef SUPPORT_SAMBA
int start_samba(json_t *conf_samba);
int stop_samba();
#endif

/* FTP */
#ifdef SUPPORT_FTP
int init_ftp(json_t *conf_ftp);
int stop_ftp();
#endif

/* Telnet */
#ifdef SUPPORT_TELNET
int start_telnetd(json_t *conf_telnetd);
int stop_telnetd(json_t *conf_telnetd);
#endif

/* Print server */
#ifdef SUPPORT_IPP
#ifdef SUPPORT_IPP_FIRMWARE
int firmware_printer_update();
#endif
int start_printserver(json_t *conf_printserver);
int stop_printserver(json_t *conf_printserver);
#endif
#ifdef SUPPORT_INETD
int start_inetd(json_t *conf_inetd);
#endif

/* TR069 client */
#ifdef	DLINK_TR069
int start_tr069(void);
int stop_tr069(void);
#endif

/* VoIP */
#ifdef SUPPORT_VOIP
extern int start_voip(json_t *conf_voip);
extern int stop_voip(json_t *conf_voip);
extern int restart_voip(json_t *conf_voip);

extern json_t *dms_voip_config_default(void);
extern int dms_voip_status_set(json_t *voip);
extern int dms_voip_config_set(json_t *config, json_t *new_voip, int *dual_restart);
extern int dms_voip_config_maintenance(void);
#endif

#ifdef SUPPORT_SENDMAIL
int send_to_email(json_t *sendmail);
#endif //SUPPORT_SENDMAIL

/* Kabinet */
#ifdef SUPPORT_KABINET
int start_kabinet(json_t *conf_kabinet);
int stop_kabinet(json_t *conf_kabinet);
#endif

#ifdef SUPPORT_GPIO_MODULE
#include <gpiom.h>
extern int dms_led_turn_on(char *name, enum gpiom_led_color color, unsigned long active_time, unsigned long disable_time);
extern int dms_led_turn_off(char *name);
extern int dms_led_turn_on_all(enum gpiom_led_color color, unsigned long active_time, unsigned long disable_time);
extern int dms_led_turn_off_all(void);
extern int dms_led_turn_on_chr(unsigned long active_time);
extern int dms_led_turn_off_chr(void);

extern int dms_button_reg(char *name, enum gpiom_button_interval bi, pid_t pid, unsigned char signal);
extern int dms_button_free(char *name, enum gpiom_button_interval bi);
#endif

/* DHCP opt */
extern void dhcp_opt_rule(char *ip, char *type);
extern int dhcp_opt_port_info_clear(json_t *config);

static inline int device_port_count(void) {
#if defined(RALINK)
#if (defined(DAP_1350) || defined(DIR_412) || defined(DAP_1150))
	return 1;
#else
	return 5;
#endif
#elif defined(BCM47XX)
	return 5;
#elif defined(AR7240)
#ifdef DIR_632
	return 9;
#else
	return 5;
#endif
#else
	return 5;
#endif
}

#endif /* _SHARED_H_ */
