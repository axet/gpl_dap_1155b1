#ifndef CONF_L2TPD_H
#define CONF_L2TPD_H

#ifdef L2TPD

int apply_l2tpd(json_t *config);
struct outgoin * conf_l2tpd(char * buf, int id);

#endif

#endif
