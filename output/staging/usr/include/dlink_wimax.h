#ifndef _DLINK_WIMAX_H
#define _DLINK_WIMAX_H

#ifdef WIMAX
/*
struct wimax_dev_status {
	unsigned int info_updated;
	unsigned char proto_flags;
	char chip[0x40];
	char firmware[0x40];
	unsigned char mac[6];
	int link_status;
	short rssi;
	float cinr;
	unsigned char bsid[6];
	short txpwr;
	unsigned int freq;
	int state;
};
*/
struct wimax_dev_status {
	unsigned int info_updated;
	unsigned char proto_flags;
	char chip[0x40];
	char firmware[0x40];
	unsigned char mac[6];
	int link_status;
	short rssi;
	float cinr;
	unsigned char bsid[6];
	short txpwr;
	unsigned int freq;
	int state;
};


struct shm_status
{
  struct timeval stamp;
  struct wimax_dev_status status;
};

int somovd_get_madwimax(struct wimax_dev_status *s);
#endif

#endif // _DLINK_WIMAX_H

