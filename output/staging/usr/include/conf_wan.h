#ifndef _CONF_WAN_H_
#define _CONF_WAN_H_
#ifdef DSL_WAN
#define TYPE_L2TP 0
#define TYPE_PPTP 1
#ifdef DMP_ETHERNETWAN_1
int getEthWanIfaceName (char*Name);
#endif
struct outgoin* wan_conf(char* buf,int id);
#endif
#endif

