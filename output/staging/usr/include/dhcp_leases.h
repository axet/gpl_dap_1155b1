#ifndef _DHCP_LEASES_H_
#define _DHCP_LEASES_H_
#ifdef DHCP_LEASES
struct outgoin* dhcp_leases(char* buf,int id);

struct lease_t {
	unsigned char chaddr[16];
	u_int32_t yiaddr;
	u_int32_t expires;
};
#endif
#endif
