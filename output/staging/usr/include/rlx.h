/* rlx.h */
#ifndef __RLX_H_
#define __RLX_H_

/* Copy from kernel/drivers/char/rtl_gpio.h */
#define LED_WAN_GREEN 		0x03
#define LED_WAN_YELLOW 		0x04
#define REG_IRQ				0x10

struct lan_port_status {
    unsigned char link;
    unsigned char speed;
    unsigned char duplex;
    unsigned char nway;    	
};

#if defined(DIR_615ABEE)
#define DIR_615A
#endif

#if defined(DIR_300ABEE)
#define DIR_300A
#endif

#ifdef SUPPORT_WIFI_5GHZ			// дефайн для 5G
#define WLAN0 "wlan1"
#define WLAN1 "wlan0"
#define VXD "wlan1-vxd"
#define VXD_5G "wlan0-vxd"
#define APCLI_IFACE "wlan1-vxd"
#else
#define WLAN0 "wlan0"
#define VXD "wlan0-vxd"
#define APCLI_IFACE "wlan0-vxd"
#endif
#define VA0 "wlan0-va0"
#define VA1 "wlan0-va1"
#define VA2 "wlan0-va2"
#define VA3 "wlan0-va3"

#if defined(DAP_1360D1) || defined(DAP_1155B1)
	#define DAP_MODE
	#define WAN "eth1"
	#define WAN_NUM 1
	#define NUM_PORTS 2
#elif defined(DIR_RTLIAD)
        #define WAN "nas0"
        #define WAN_NUM 1
        #define NUM_PORTS 5
#elif defined(DIR_300A_WITH_AP)
	#define DAP_MODE
	#define WAN "eth1"
	#define WAN_NUM 1
	#define NUM_PORTS 5
#else 
	#define WAN "eth1"
	#define WAN_NUM 1
	#define NUM_PORTS 5	
#endif 


struct rtl8196c_wifi
{
     unsigned char tx_power_cck_a[14];
     unsigned char tx_power_cck_b[14];
     unsigned char tx_power_ht40_1s_a[14];
     unsigned char tx_power_ht40_1s_b[14];
     unsigned char tx_diff_ht40_2s[14];
     unsigned char tx_diff_ht20[14];
     unsigned char tx_diff_ofdm[14];
     unsigned char reg_domain;
     unsigned char rf_type;
     unsigned char led_type;
     unsigned char xcap_11n;
     unsigned char tssi1_11n;
     unsigned char tssi2_11n;
#ifdef SUPPORT_WIFI_5GHZ
     unsigned char ther_11n_2g;
     unsigned char ther_11n_5g;
#else
     unsigned char ther_11n;
#endif
     unsigned char trswitch_11n;
     unsigned char trswpape_c9_11n;
     unsigned char trswpape_cc_11n;
     unsigned char target_pwr_11n;
     unsigned char reserver5_11n;
     unsigned char reserver6_11n;
     unsigned char reserver7_11n;
     unsigned char reserver8_11n;
     unsigned char reserver9_11n;
     unsigned char reserver10_11n;
     unsigned char tx_power_5g_ht40_1s_a[196];
     unsigned char tx_power_5g_ht40_1s_b[196];
     unsigned char tx_diff_5g_ht40_2s[196];
     unsigned char tx_power_diff_5g_ht20[196];
     unsigned char tx_power_diff_5g_ofdm[196];
} __attribute__((packed));

struct qos_cmd_info_s{
	int action;
	union{
		struct{
			unsigned char port[8];
			unsigned int portmask;
		} port_based_priority, queue_num;
		struct{
			unsigned char vlan[8];
			unsigned int vlanmask;
		}vlan_based_priority;
		struct{
			unsigned char dscp[64];
			unsigned int dscpmask1;
			unsigned int dscpmask2;
		}dscp_based_priority;
		struct{
			unsigned char queue[8][6];
			unsigned int portmask;
			unsigned int queuemask;
		}queue_type;
		struct{
			unsigned char priority[8];
			unsigned int prioritymask;
		}sys_priority;
		struct{
			unsigned char decision[5];
		}pri_decision;
		struct{
			unsigned char remark[8][8];
			unsigned int portmask;
			unsigned int prioritymask;
		}vlan_remark, dscp_remark;
		struct{
			unsigned short apr[8][6];
			unsigned char burst[8][6];
			unsigned char ppr[8][6];
			unsigned int portmask;
			unsigned int queuemask;
		}queue_rate;
	} qos_data;
};

struct nl_data_info
{
	int len;
	void *data;
};

#define FLOW_CONTROL_ENABLE 23
#define FLOW_CONTROL_DISABLE 24
#define MAX_PAYLOAD 1024
#define NETLINK_RTK_HW_QOS 25
#ifdef RLX_MODEM
//void init_dsl(json_t *config);

typedef struct OBCIF_ARG {
  int argsize;
  int arg;
}obcif_arg;

// ioctl command called by protocols (system-independent)
#define RLCM_IOC_MAGIC	(('R'+'L'+'C'+'M'+'a'+'d'+'s'+'l') << 8)  //0x2fe00
#define RLCM_PHY_ENABLE_MODEM			(RLCM_IOC_MAGIC + 1)
#define RLCM_PHY_DISABLE_MODEM			(RLCM_IOC_MAGIC + 2)
#define RLCM_GET_DRIVER_VERSION			(RLCM_IOC_MAGIC + 3)
#define RLCM_GET_DRIVER_BUILD			(RLCM_IOC_MAGIC + 4)
#define RLCM_MODEM_RETRAIN			(RLCM_IOC_MAGIC + 5)
#define RLCM_GET_REHS_COUNT			(RLCM_IOC_MAGIC + 6)
#define RLCM_GET_CHANNEL_SNR			(RLCM_IOC_MAGIC + 7)
#define RLCM_GET_AVERAGE_SNR			(RLCM_IOC_MAGIC + 8)
#define RLCM_GET_SNR_MARGIN			(RLCM_IOC_MAGIC + 9)
#define RLCM_REPORT_MODEM_STATE			(RLCM_IOC_MAGIC +10)
#define RLCM_REPORT_PM_DATA			(RLCM_IOC_MAGIC +11)
#define RLCM_MODEM_NEAR_END_ID_REQ		(RLCM_IOC_MAGIC +12)
#define RLCM_MODEM_FAR_END_ID_REQ		(RLCM_IOC_MAGIC +13)
#define RLCM_MODEM_NEAR_END_LINE_DATA_REQ	(RLCM_IOC_MAGIC +14)
#define RLCM_MODEM_FAR_END_LINE_DATA_REQ	(RLCM_IOC_MAGIC +15)
#define RLCM_MODEM_NEAR_END_FAST_CH_DATA_REQ	(RLCM_IOC_MAGIC +16)
#define RLCM_MODEM_NEAR_END_INT_CH_DATA_REQ	(RLCM_IOC_MAGIC +17)
#define RLCM_MODEM_FAR_END_FAST_CH_DATA_REQ	(RLCM_IOC_MAGIC +18)
#define RLCM_MODEM_FAR_END_INT_CH_DATA_REQ	(RLCM_IOC_MAGIC +19)
#define RLCM_SET_ADSL_MODE			(RLCM_IOC_MAGIC +20)
#define RLCM_GET_ADSL_MODE			(RLCM_IOC_MAGIC +21)
#define RLCM_GET_LOSS_DATA			(RLCM_IOC_MAGIC +22)
#define RLCM_GET_LINK_SPEED			(RLCM_IOC_MAGIC +23)
#define RLCM_GET_CHANNEL_MODE			(RLCM_IOC_MAGIC +24)
#define RLCM_GET_LOOP_ATT			(RLCM_IOC_MAGIC +25)
#define RLCM_INC_TX_POWER			(RLCM_IOC_MAGIC +26)
#define RLCM_TUNE_PERF				(RLCM_IOC_MAGIC +27)
#define RLCM_ENABLE_BIT_SWAP			(RLCM_IOC_MAGIC +28)
#define RLCM_DISABLE_BIT_SWAP			(RLCM_IOC_MAGIC +29)
#define RLCM_ENABLE_PILOT_RELOCATION		(RLCM_IOC_MAGIC +30)
#define RLCM_DISABLE_PILOT_RELOCATION		(RLCM_IOC_MAGIC +31)
#define RLCM_ENABLE_TRELLIS			(RLCM_IOC_MAGIC +32)
#define RLCM_DISABLE_TRELLIS			(RLCM_IOC_MAGIC +33)
#define RLCM_SET_VENDOR_ID			(RLCM_IOC_MAGIC +34)
#define RLCM_MODEM_READ_CONFIG                  (RLCM_IOC_MAGIC +35)
#define RLCM_MODEM_WRITE_CONFIG                 (RLCM_IOC_MAGIC +36)
#define RLCM_DEBUG_MODE				(RLCM_IOC_MAGIC +37)
#define RLCM_TEST_PSD				(RLCM_IOC_MAGIC +38)
#define RLCM_GET_ADSL_TIME			(RLCM_IOC_MAGIC +39)
#define RLCM_PHY_START_MODEM			(RLCM_IOC_MAGIC +40)
#define RLCM_ENABLE_ADSL_LOG			(RLCM_IOC_MAGIC +41)
#define RLCM_DISABLE_ADSL_LOG			(RLCM_IOC_MAGIC +42)
//added command
#define RLCM_GET_VENDOR_ID			(RLCM_IOC_MAGIC +43)
#define RLCM_GET_TX_POWER			(RLCM_IOC_MAGIC +44)
#define RLCM_GET_PERF_VALUE			(RLCM_IOC_MAGIC +45)
#define RLCM_GET_15MIN_LOSS_DATA		(RLCM_IOC_MAGIC +46)
#define RLCM_GET_1DAY_LOSS_DATA			(RLCM_IOC_MAGIC +47)
#define RLCM_GET_CHANNEL_BITLOAD		(RLCM_IOC_MAGIC +48)
//for MIB TRAP set
#define RLCM_GET_TRAP_THRESHOLD			(RLCM_IOC_MAGIC +49)
#define RLCM_SET_TRAP_THRESHOLD			(RLCM_IOC_MAGIC +50)
#define RLCM_15MIN_WAIT_TRAP			(RLCM_IOC_MAGIC +51)

// new_hibrid
#define RLCM_SET_HYBRID				(RLCM_IOC_MAGIC +75)

//yaru + for webdata
#define RLCM_SET_XDSL_MODE			(RLCM_IOC_MAGIC +79)
#define RLCM_GET_SHOWTIME_XDSL_MODE		(RLCM_IOC_MAGIC +80)
#define RLCM_GET_XDSL_MODE			(RLCM_IOC_MAGIC +81)
#define RLCM_SET_OLR_TYPE			(RLCM_IOC_MAGIC +82)
#define RLCM_GET_OLR_TYPE 			(RLCM_IOC_MAGIC +83)
#define RLCM_GET_LINE_RATE			(RLCM_IOC_MAGIC +84)
#define RLCM_GET_DS_ERROR_COUNT			(RLCM_IOC_MAGIC +85)
#define RLCM_GET_US_ERROR_COUNT			(RLCM_IOC_MAGIC +86)
#define RLCM_GET_DIAG_QLN			(RLCM_IOC_MAGIC +87)
#define RLCM_GET_DIAG_HLOG			(RLCM_IOC_MAGIC +88)
#define RLCM_GET_DIAG_SNR			(RLCM_IOC_MAGIC +89)
#define RLCM_GET_DS_PMS_PARAM1			(RLCM_IOC_MAGIC +90)
#define RLCM_GET_US_PMS_PARAM1			(RLCM_IOC_MAGIC +91)
#define RLCM_SET_ANNEX_L			(RLCM_IOC_MAGIC +92)
#define RLCM_GET_ANNEX_L			(RLCM_IOC_MAGIC +93)
#define RLCM_GET_LINK_POWER_STATE		(RLCM_IOC_MAGIC +94)
#define RLCM_GET_ATT_RATE			(RLCM_IOC_MAGIC +95)
#define RLCM_LOADCARRIERMASK			(RLCM_IOC_MAGIC +96)
#define RLCM_SET_ANNEX_M			(RLCM_IOC_MAGIC +97)
#define RLCM_GET_ANNEX_M			(RLCM_IOC_MAGIC +98)
#define RLCM_SET_8671_REV			(RLCM_IOC_MAGIC +99)
#define RLCM_GET_8671_REV			(RLCM_IOC_MAGIC +100)
#define RLCM_SET_HIGH_INP			(RLCM_IOC_MAGIC +101)
#define RLCM_GET_HIGH_INP			(RLCM_IOC_MAGIC +102)
#define RLCM_GET_LD_STATE			(RLCM_IOC_MAGIC +103)
#define RLCM_SET_ANNEX_B			(RLCM_IOC_MAGIC +104)
#define RLCM_GET_ANNEX_B			(RLCM_IOC_MAGIC +105)
//for TR069
#define RLCM_GET_DSL_STAT_SHOWTIME		(RLCM_IOC_MAGIC +106)
#define RLCM_GET_DSL_STAT_TOTAL			(RLCM_IOC_MAGIC +107)
#define RLCM_GET_DSL_PSD			(RLCM_IOC_MAGIC +108)
#define RLCM_GET_DSL_ORHERS			(RLCM_IOC_MAGIC +109)
#define RLCM_GET_DSL_GI				(RLCM_IOC_MAGIC +110)
// for Telefonica
#define RLCM_SET_ADSL_LAST_OPMode		(RLCM_IOC_MAGIC +111)
#define RLCM_SET_ADSL_PMS_CONFIG		(RLCM_IOC_MAGIC +112)
//Lupin, for TR069
#define RLCM_GET_ADSL2WAN_IFCFG			(RLCM_IOC_MAGIC +113)
#define RLCM_ENABLE_DIAGNOSTIC			(RLCM_IOC_MAGIC +114)

// ADSL MODE
#define ADSL_MODE_T1413		0x0001
#define ADSL_MODE_GDMT		0x0002
#define ADSL_MODE_GLITE		0x0008
#define ADSL_MODE_ADSL2		0x0010
#define ADSL_MODE_ANXL		0x0020
#define ADSL_MODE_ADSL2P	0x0040
#define ADSL_MODE_ANXM		0x0080
#define ADSL_MODE_ANXB		0x0100

#endif
#endif  //__RLX_H_
