#include boot/*/*.mk

include boot/cfe/cfe.mk

#BOOTLOADER_DIR:=$(BUILD_DIR)/bootloader


ifeq (${BR2_BOOTLOADER_UPDATE_FROM_FW},y)
	COPY:=cp $(PROFILE_DIR)/uboot.img $(TARGET_DIR)/bin/
endif

all: $(BOOTLOADER_DIR)/.stamp_installed

# Download
$(BOOTLOADER_DIR)/.stamp_downloaded:
	@$(call MESSAGE,"Downloading bootloader")
	git clone $(subst ",,$(BR2_BOOTLOADER_EXTERNAL_LOCATION)) $(BOOTLOADER_DIR)
	touch $@

# Configuration
$(BOOTLOADER_DIR)/.stamp_configured: $(BOOTLOADER_DIR)/.stamp_downloaded
	@$(call MESSAGE,"Configuring bootloader")
	$(MAKE) $(BR2_BOOTLOADER_CONFIG_COMMANDS) -C $(BOOTLOADER_DIR) CROSS_COMPILE=${TARGET_CROSS} DEV_ID=$(DLINK_DEVICEID)
	$(Q)touch $@
	

# Compilation. We make sure the bootloader gets rebuilt when the
# configuration has changed.
$(BOOTLOADER_DIR)/.stamp_compiled: $(BOOTLOADER_DIR)/.stamp_configured 
	@$(call MESSAGE,"Compiling bootloader")
	@echo -e ${TARGET_CROSS}
	$(MAKE) -C $(BOOTLOADER_DIR) CROSS_COMPILE=${TARGET_CROSS} DEV_ID=$(DLINK_DEVICEID)
	$(Q)touch $@

# Installation
$(BOOTLOADER_DIR)/.stamp_installed: $(BOOTLOADER_DIR)/.stamp_compiled
	@$(call MESSAGE,"Installing bootloader")
	-cp $(BOOTLOADER_DIR)/*.bin $(BOOTLOADER_DIR)/*.img $(BINARIES_DIR)
	#$(COPY)
	$(Q)touch $@

